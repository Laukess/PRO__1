import java.util.ArrayList;

/**
 * Created by Laukess on 30-05-2016.
 */
public class Gaest {

    private String navn;
    private int vaerelsesNummer;
    private ArrayList<Bestilling> bestillinger = new ArrayList<>();

    public Gaest(String navn, int vaerelsesNummer) {
        this.navn = navn;
        this.vaerelsesNummer = vaerelsesNummer;
    }

    public void setNavn(String navn) {
        this.navn = navn;
    }

    public void setVaerelsesNummer(int vaerelsesNummer) {
        this.vaerelsesNummer = vaerelsesNummer;
    }

    public double pris() {
        double total = 0;

        for (Bestilling b : bestillinger) {
            total += b.pris();
        }

        return total;
    }

    public ArrayList<String> ikkeBetalteBestillinger() {
        ArrayList<String> liste = new ArrayList<>();

        for (Bestilling b : bestillinger) {
            if (!b.isBetalt()) {
                for (BestillingsLinje bl : b.getBestillingsLinjer()) {
                    String string = "";
                    string += b.getDato() +
                            " " + bl.getAntal() +
                            " " + bl.getVare().getNavn() +
                            " " + bl.getVare().getMaerke() +
                            " " + bl.getPris();
                    liste.add(string);
                }
            }
        }
        return liste;
    }

    // TODO: Tjek lige om det er lavet med soegeskabelonen (S5)
    public boolean harBestilt(Vare vare) {
        for (Bestilling bestilling : bestillinger) {
            for (BestillingsLinje bl : bestilling.getBestillingsLinjer()) {
                if (bl.getVare()==vare) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean addBestilling(Bestilling bestilling) {
        return this.bestillinger.add(bestilling);
    }

    public boolean removeBestilling(Bestilling bestilling) {
        return this.bestillinger.remove(bestilling);
    }

    public ArrayList<Bestilling> getBestillinger() {
        return bestillinger;
    }

    public String getNavn() {
        return navn;
    }

    public int getVaerelsesNummer() {
        return vaerelsesNummer;
    }

    @Override
    public String toString() {
        return this.navn + " " + this.vaerelsesNummer;
    }
}
