/**
 * Created by Laukess on 30-05-2016.
 */

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.geometry.*;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;


public class gui extends Application{

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) {

        Service.createSomeObjects();

        stage.setTitle("");
        GridPane pane = new GridPane();
        this.initContent(pane);

        Scene scene = new Scene(pane);
        stage.setScene(scene);
        stage.show();
    }


    ListView<Gaest> lvwGaseter = new ListView();
    ListView<String> lvwIkkeBetalt = new ListView();
    TextField txfGaest = new TextField();
    TextField txfVaerelse = new TextField();

    // ------------------------------------------------------------------------

    private void initContent(GridPane pane) {
        pane.setGridLinesVisible(false);
        pane.setPadding(new Insets(20));
        pane.setHgap(10);
        pane.setVgap(10);

        Label lblGaester = new Label("Gaester:");
        pane.add(lblGaester, 0, 0);

        lvwGaseter.getItems().setAll(Service.getGaester());
        pane.add(lvwGaseter, 0, 1, 1, 5);

        pane.add(lvwIkkeBetalt, 2, 4, 2, 1);
        selectedGaestChanged();

        Label lblGaest = new Label("Gaset:");
        pane.add(lblGaest, 1, 0, 1, 1);

        txfGaest = new TextField();
        pane.add(txfGaest, 2, 0, 2, 1);

        Label lblVaerelse = new Label("Vaerelse:");
        pane.add(lblVaerelse, 1, 1, 1, 1);

        txfVaerelse = new TextField();
        pane.add(txfVaerelse, 2, 1, 2, 1);

        Button btnOpret = new Button("Opret");
        pane.add(btnOpret, 2, 2, 1, 1);

        Button btnOpdater = new Button("Opdater");
        pane.add(btnOpdater, 3, 2, 1, 1);

        Label lblIkkeBetaltBestilling = new Label("Ikke betalt bestillinger:");
        pane.add(lblIkkeBetaltBestilling, 1, 3, 2, 1);

        ChangeListener<Gaest> listener =
                (ov, oldHotel, newHotel) -> this.selectedGaestChanged();
        lvwGaseter.getSelectionModel().selectedItemProperty().addListener(listener);

        btnOpret.setOnAction(event -> createAction());
        btnOpdater.setOnAction(event -> updateAction());

    }

    private void selectedGaestChanged() {
        Gaest gaest = null;

        try {
            gaest = (Gaest) lvwGaseter.getSelectionModel().getSelectedItem();
        } catch (Exception e) {

        }
        if (gaest != null) {
            lvwIkkeBetalt.getItems().setAll(gaest.ikkeBetalteBestillinger());

            txfGaest.setText(gaest.getNavn());
            txfVaerelse.setText(gaest.getVaerelsesNummer()+"");
        }


    }

    private void createAction() {
        try {
            Service.createGaest(txfGaest.getText(), Integer.parseInt(txfVaerelse.getText()));
            update();
        } catch (Exception e) {
        }
    }

    private void updateAction() {
        try {
            Gaest gaest = lvwGaseter.getSelectionModel().getSelectedItem();
            Service.updaterGaest(gaest, txfGaest.getText(), Integer.parseInt(txfVaerelse.getText()));
            update();
        } catch (Exception e) {
            
        }
    }

    private void update() {
        lvwGaseter.getItems().setAll(Service.getGaester());
    }

}
