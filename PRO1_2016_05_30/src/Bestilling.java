import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Created by Laukess on 30-05-2016.
 */
public class Bestilling {

    private LocalDate dato;
    private boolean betalt;
    private ArrayList<BestillingsLinje> bestillingsLinjer = new ArrayList<>();

    public Bestilling(LocalDate dato) {
        this.dato = dato;
        betalt = false;
    }

    public double pris() {
        double total = 0;
        for (BestillingsLinje bl : bestillingsLinjer) {
            total += bl.getPris();
        }
        return total;
    }

    public BestillingsLinje createBestillingsLinje(Vare vare, int antal) {
        BestillingsLinje bl = new BestillingsLinje(antal, vare, this);
        return bl;
    }

    public boolean addBestillingsLinje(BestillingsLinje bestillingsLinje) {
        return this.bestillingsLinjer.add(bestillingsLinje);
    }

    public boolean removeBestillingsLinje(BestillingsLinje bestillingsLinje) {
            return this.bestillingsLinjer.remove(bestillingsLinje);
    }

    public ArrayList<BestillingsLinje> getBestillingsLinjer() {
        return this.bestillingsLinjer;
    }


    public LocalDate getDato() {
        return dato;
    }

    public boolean isBetalt() {
        return betalt;
    }

    public void setBetalt(boolean betalt) {
        this.betalt = betalt;
    }


}
