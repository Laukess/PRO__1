import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Created by Laukess on 30-05-2016.
 */
public class Service {

    public static Vare createVare(String navn, String maerke, double maengde, double pris){
        Vare vare = new Vare(navn, maerke, maengde, pris);
        Dao.addVare(vare);
        return vare;
    }

    public static ArrayList<Gaest> getGaester() {
        return Dao.getGaester();
    }

    public static Gaest createGaest(String navn, int vaerelsesnummer) {
        Gaest gaest = new Gaest(navn, vaerelsesnummer);
        Dao.addGaest(gaest);
        return gaest;
    }

    public static Bestilling createBestilling(LocalDate dato) {
        return new Bestilling(dato);
    }

    // husk den skal laves igennem Bestilling;
    public static BestillingsLinje createBestillingsLinje(Bestilling bestilling, Vare vare, int antal) {
        BestillingsLinje bl = bestilling.createBestillingsLinje(vare, antal);
        bestilling.addBestillingsLinje(bl);
        return bl;
    }

    public static void createSomeObjects() {
        Vare v1 = createVare("Cola", "CocaCola", 0.5, 28);
        Vare v2 = createVare("Cola", "Pepsi", 0.5, 26);
        Vare v3 = createVare("Oel", "Tuborg", 0.33, 33);
        Vare v4 =  createVare("Oel", "Carlberg", 0.57, 48);

        Gaest g1 = createGaest("Ib", 17);
        Gaest g2 = createGaest("Eva", 12);
        Gaest g3 = createGaest("Lene", 8);

        Bestilling b1 = createBestilling(LocalDate.of(2015, 6, 14));
        createBestillingsLinje(b1, v3, 3);
        createBestillingsLinje(b1, v1, 2);
        createBestillingsLinje(b1, v2, 2);

        Bestilling b2 = createBestilling(LocalDate.of(2016, 6, 14));
        createBestillingsLinje(b2, v3, 2);
        createBestillingsLinje(b2, v2, 3);
        createBestillingsLinje(b2, v4, 4);

        g1.addBestilling(b1);
        g1.addBestilling(b2);

    }

    public static void updaterGaest(Gaest gaest, String navn, int vaerelsesnummer) {
        gaest.setNavn(navn);
        gaest.setVaerelsesNummer(vaerelsesnummer);
    }
}
