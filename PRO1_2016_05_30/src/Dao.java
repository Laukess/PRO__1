import java.util.ArrayList;

/**
 * Created by Laukess on 30-05-2016.
 */
public class Dao {

    private static ArrayList<Vare> varer = new ArrayList<>();
    private static ArrayList<Gaest> gaester = new ArrayList<>();

    public static boolean addVare(Vare vare) {
        return varer.add(vare);
    }

    public static boolean addGaest(Gaest gaest) {
        return gaester.add(gaest);
    }

    public static boolean removeVare(Vare vare) {
        return varer.remove(vare);
    }

    public static boolean removeGaest(Gaest gaest) {
        return gaester.remove(gaest);
    }

    public static ArrayList<Vare> getVarer() {
        return varer;
    }

    public static ArrayList<Gaest> getGaester() {
        return gaester;
    }
}
