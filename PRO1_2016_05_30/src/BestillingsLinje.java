/**
 * Created by Laukess on 30-05-2016.
 */
public class BestillingsLinje {

    private int antal;
    private Vare vare;
    private Bestilling bestilling;

    public BestillingsLinje(int antal, Vare vare, Bestilling bestilling) {
        this.antal = antal;
        this.vare = vare;
        this.bestilling = bestilling;

//        // TODO: tjek lige om det her er maaden at goere det paa ????
//        vare.addBestillingsLinje(this);
    }

    public double getPris() {
        return vare.getPris() * this.antal;
    }

    public int getAntal() {
        return antal;
    }

    public Vare getVare() {
        return vare;
    }
}
