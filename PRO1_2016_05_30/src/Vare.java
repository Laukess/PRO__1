import java.util.ArrayList;

/**
 * Created by Laukess on 30-05-2016.
 */
public class Vare {

    private String navn;
    private String maerke;
    private double maengde;
    private double pris;
    private ArrayList<BestillingsLinje> bestillingsLinjer = new ArrayList<>();

    public Vare(String navn, String maerke, double maengde, double pris) {
        this.navn = navn;
        this.maerke = maerke;
        this.maengde = maengde;
        this.pris = pris;
    }

    public boolean addBestillingsLinje(BestillingsLinje bestillingsLinje) {
        return this.bestillingsLinjer.add(bestillingsLinje);
    }

    public boolean removeBestillingsLinje(BestillingsLinje bestillingsLinje) {
        return this.bestillingsLinjer.remove(bestillingsLinje);
    }


    public String getNavn() {
        return navn;
    }

    public String getMaerke() {
        return maerke;
    }

    public double getMaengde() {
        return maengde;
    }

    public double getPris() {
        return pris;
    }

    public ArrayList<BestillingsLinje> getBestillingsLinjer() {
        return bestillingsLinjer;
    }
}
