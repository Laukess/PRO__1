import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Opgave_2_b extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) {
        Pane root = this.initContent();
        Scene scene = new Scene(root);

        stage.setTitle("Shapes");
        stage.setScene(scene);
        stage.show();
    }

    private Pane initContent() {
        Pane pane = new Pane();
        pane.setPrefSize(400 + distanceShift, 400);
        this.drawShapes(pane);
        return pane;
    }

    // ------------------------------------------------------------------------

    int distanceShift = 100;
    Color color = Color.YELLOW;

    private void drawShapes(Pane pane) {

        Circle outerCircle = new Circle(60, 60, 50);
        outerCircle.setFill(Color.BLACK);
        pane.getChildren().add(outerCircle);

        Circle outerCircle2 = new Circle(60, 60, 40);
        outerCircle2.setFill(Color.WHITE);
        pane.getChildren().add(outerCircle2);

        Circle outerCircle3 = new Circle(60, 60, 30);
        outerCircle3.setFill(Color.BLACK);
        pane.getChildren().add(outerCircle3);

        Circle outerCircle4 = new Circle(60, 60, 20);
        outerCircle4.setFill(Color.WHITE);
        pane.getChildren().add(outerCircle4);

        Circle outerCircle5 = new Circle(60, 60, 10);
        outerCircle5.setFill(Color.BLACK);
        pane.getChildren().add(outerCircle5);
    }
}

