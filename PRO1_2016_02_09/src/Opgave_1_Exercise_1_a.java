import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Opgave_1_Exercise_1_a extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) {
        Pane root = this.initContent();
        Scene scene = new Scene(root);

        stage.setTitle("Shapes");
        stage.setScene(scene);
        stage.show();
    }

    private Pane initContent() {
        Pane pane = new Pane();
        pane.setPrefSize(400, 400);
        this.drawShapes(pane);
        return pane;
    }

    // ------------------------------------------------------------------------

    private void drawShapes(Pane pane) {
        Circle circle = new Circle(85, 85, 75);
        pane.getChildren().add(circle);
        circle.setFill(Color.DARKBLUE);

        Rectangle rect = new Rectangle(170, 10, 150, 150);
        pane.getChildren().add(rect);
        rect.setFill(Color.RED);

        Line line = new Line(10, 170, 330, 170);
        pane.getChildren().add(line);
        line.setStroke(Color.GREEN);
        line.setStrokeWidth(5);

    }
}

