import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Opgave_2_d extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) {
        Pane root = this.initContent();
        Scene scene = new Scene(root);

        stage.setTitle("Shapes");
        stage.setScene(scene);
        stage.show();
    }

    private Pane initContent() {
        Pane pane = new Pane();
        pane.setPrefSize(400 + distanceShift, 400);
        this.drawShapes(pane);
        return pane;
    }

    // ------------------------------------------------------------------------

    int distanceShift = 100;
    Color color = Color.YELLOW;

    private void drawShapes(Pane pane) {

        Circle blueCircle = new Circle(35, 35, 25);
        blueCircle.setStroke(Color.BLUE);
        blueCircle.setStrokeWidth(5);
        blueCircle.setFill(Color.WHITE);
        pane.getChildren().add(blueCircle);

        Circle yellowCircle = new Circle(65, 60, 25);
        yellowCircle.setStroke(Color.YELLOW);
        yellowCircle.setStrokeWidth(5);
        yellowCircle.setFill(Color.TRANSPARENT);
        pane.getChildren().add(yellowCircle);

        Circle blackCircle = new Circle(95, 35, 25);
        blackCircle.setStroke(Color.BLACK);
        blackCircle.setStrokeWidth(5);
        blackCircle.setFill(Color.TRANSPARENT);
        pane.getChildren().add(blackCircle);

        Circle greenCircle = new Circle(125, 60, 25);
        greenCircle.setStroke(Color.GREEN);
        greenCircle.setStrokeWidth(5);
        greenCircle.setFill(Color.TRANSPARENT);
        pane.getChildren().add(greenCircle);

        Circle redCircle = new Circle(155, 35, 25);
        redCircle.setStroke(Color.RED);
        redCircle.setStrokeWidth(5);
        redCircle.setFill(Color.TRANSPARENT);
        pane.getChildren().add(redCircle);

    }
}

