import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Opgave_2_f extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) {
        Pane root = this.initContent();
        Scene scene = new Scene(root);

        stage.setTitle("Shapes");
        stage.setScene(scene);
        stage.show();
    }

    private Pane initContent() {
        Pane pane = new Pane();
        pane.setPrefSize(400 + distanceShift, 400);
        this.drawShapes(pane);
        return pane;
    }

    // ------------------------------------------------------------------------

    int distanceShift = 100;
    Color color = Color.YELLOW;

    private void drawShapes(Pane pane) {

        Circle face = new Circle(85, 85, 75);
        face.setFill(Color.SANDYBROWN);
        pane.getChildren().add(face);

        Polygon mouth = new Polygon(85, 85, 165, 45, 165, 125);
        mouth.setFill(Color.WHITE);
        pane.getChildren().add(mouth);

        Circle eye = new Circle(85, 65, 3);
        eye.setFill(Color.WHITE);
        pane.getChildren().add(eye);

    }
}

