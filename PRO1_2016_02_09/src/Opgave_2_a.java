import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Opgave_2_a extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) {
        Pane root = this.initContent();
        Scene scene = new Scene(root);

        stage.setTitle("Shapes");
        stage.setScene(scene);
        stage.show();
    }

    private Pane initContent() {
        Pane pane = new Pane();
        pane.setPrefSize(400 + distanceShift, 400);
        this.drawShapes(pane);
        return pane;
    }

    // ------------------------------------------------------------------------

    int distanceShift = 100;
    Color color = Color.YELLOW;

    private void drawShapes(Pane pane) {
        Circle outerCircle = new Circle(60, 60, 50);
        outerCircle.setFill(Color.WHITE);
        outerCircle.setStroke(Color.BLACK);
        pane.getChildren().add(outerCircle);

        Circle leftEye = new Circle(45, 50, 5);
        leftEye.setFill(Color.WHITE);
        leftEye.setStroke(Color.BLACK);
        pane.getChildren().add(leftEye);

        Circle rightEye = new Circle(75, 50, 5);
        rightEye.setFill(Color.WHITE);
        rightEye.setStroke(Color.BLACK);
        pane.getChildren().add(rightEye);

        Line mouth = new Line(40, 80, 80, 80);
        pane.getChildren().add(mouth);
    }
}

