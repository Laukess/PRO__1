import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Opgave_1_Exercise_1_b extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) {
        Pane root = this.initContent();
        Scene scene = new Scene(root);

        stage.setTitle("Shapes");
        stage.setScene(scene);
        stage.show();
    }

    private Pane initContent() {
        Pane pane = new Pane();
        pane.setPrefSize(400, 400);
        this.drawShapes(pane);
        return pane;
    }

    // ------------------------------------------------------------------------

    private void drawShapes(Pane pane) {
        Circle circle = new Circle(35, 35, 25);
        pane.getChildren().add(circle);
        circle.setFill(Color.YELLOW);

        Rectangle rect = new Rectangle(70, 10, 60, 60);
        pane.getChildren().add(rect);
        rect.setFill(Color.AQUAMARINE);

        Line line = new Line(10, 70, 130, 100);
        pane.getChildren().add(line);
        line.setStroke(Color.SILVER);
        line.setStrokeWidth(5);

    }
}

