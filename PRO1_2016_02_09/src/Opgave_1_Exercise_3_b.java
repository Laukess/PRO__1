import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Opgave_1_Exercise_3_b extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) {
        Pane root = this.initContent();
        Scene scene = new Scene(root);

        stage.setTitle("Shapes");
        stage.setScene(scene);
        stage.show();
    }

    private Pane initContent() {
        Pane pane = new Pane();
        pane.setPrefSize(400 + distanceShift, 400);
        this.drawShapes(pane);
        return pane;
    }

    // ------------------------------------------------------------------------

    int distanceShift = 100;
    Color color = Color.YELLOW;

    private void drawShapes(Pane pane) {
        Rectangle house = new Rectangle(40, 200, 120, 120);
        house.setFill(Color.RED);
        pane.getChildren().add(house);

        Line groundLine = new Line(20, 320, 320, 320);
        pane.getChildren().add(groundLine);

        Rectangle window = new Rectangle(60, 220, 40, 40);
        pane.getChildren().add(window);

        Polygon roof = new Polygon(20, 200, 100, 100, 180, 200);
        roof.setFill(Color.GREEN);
        pane.getChildren().add(roof);

        Circle sun = new Circle(220, 100, 40);
        sun.setFill(Color.YELLOW);
        sun.setStroke(Color.BLACK);
        pane.getChildren().add(sun);

        Line grass = new Line(200, 320, 200, 300);
        grass.setStroke(Color.GREEN);
        pane.getChildren().add(grass);

        Line grass2 = new Line(210, 320, 210, 305);
        grass2.setStroke(Color.GREEN);
        pane.getChildren().add(grass2);

        Line grass3 = new Line(220, 320, 220, 295);
        grass3.setStroke(Color.GREEN);
        pane.getChildren().add(grass3);

        Line grass4 = new Line(230, 320, 230, 310);
        grass4.setStroke(Color.GREEN);
        pane.getChildren().add(grass4);

        Line grass5 = new Line(240, 320, 240, 290);
        grass5.setStroke(Color.GREEN);
        pane.getChildren().add(grass5);
    }
}

