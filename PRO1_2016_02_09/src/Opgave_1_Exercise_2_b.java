import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Opgave_1_Exercise_2_b extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) {
        Pane root = this.initContent();
        Scene scene = new Scene(root);

        stage.setTitle("Shapes");
        stage.setScene(scene);
        stage.show();
    }

    private Pane initContent() {
        Pane pane = new Pane();
        pane.setPrefSize(400 + distanceShift, 400);
        this.drawShapes(pane);
        return pane;
    }

    // ------------------------------------------------------------------------

    int distanceShift = 100;
    Color color = Color.YELLOW;

    private void drawShapes(Pane pane) {
        Circle circle = new Circle(85+distanceShift, 85, 75);
        pane.getChildren().add(circle);
        circle.setFill(color);

        Rectangle rect = new Rectangle(170 + distanceShift, 10, 150, 150);
        pane.getChildren().add(rect);
        rect.setFill(color);

        Line line = new Line(10 + distanceShift, 170, 330, 170);
        pane.getChildren().add(line);
        line.setStroke(color);
        line.setStrokeWidth(5);

    }
}

