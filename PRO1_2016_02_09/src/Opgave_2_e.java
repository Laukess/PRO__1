import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Opgave_2_e extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) {
        Pane root = this.initContent();
        Scene scene = new Scene(root);

        stage.setTitle("Shapes");
        stage.setScene(scene);
        stage.show();
    }

    private Pane initContent() {
        Pane pane = new Pane();
        pane.setPrefSize(400 + distanceShift, 400);
        this.drawShapes(pane);
        return pane;
    }

    // ------------------------------------------------------------------------

    int distanceShift = 100;
    Color color = Color.YELLOW;

    private void drawShapes(Pane pane) {

        Circle left = new Circle(55, 50, 40);
        left.setFill(Color.RED);
        pane.getChildren().add(left);

        Circle right = new Circle(110, 50, 40);
        right.setFill(Color.RED);
        pane.getChildren().add(right);

        Polygon buttom = new Polygon(16, 60, 80, 160, 149, 60);
        buttom.setFill(Color.RED);
        pane.getChildren().add(buttom);

    }
}

