package gui;

import javafx.scene.paint.Color;
import model.Hotel;
import model.Konference;
import model.Tilfoejelse;
import model.Tilmelding;
import service.Service;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.Optional;

public class HotelPane extends GridPane {

    final int ROW_HEIGHT = 25;
    final int DEFAULT_ROWS = 12;

    private Label lblPris;
    private Button btnOpretHotel, btnSletHotel, btnOpretTilfoejelse;
    private Konference konference;

    private ListView<Hotel> lvwHoteller;
    private ListView<Tilmelding> lvwTilmeldinger;
    private ListView<Tilfoejelse> lvwTilfoejelser;

    public HotelPane(Konference konference) {
        this.setPadding(new Insets(20));
        this.setHgap(20);
        this.setVgap(10);
        this.setGridLinesVisible(false);

        this.konference = konference;

        Label lblHotel = new Label("Hoteller");
        lvwHoteller = new ListView<>();

        lvwHoteller.getItems().setAll(initAlleHoteller(this.konference));
        lvwHoteller.getSelectionModel().select(0);
        lvwHoteller.requestFocus();

        ChangeListener<Hotel> listener =
                (ov, oldHotel, newHotel) -> this.selectedHotelChanged();
        lvwHoteller.getSelectionModel().selectedItemProperty().addListener(listener);

        lvwHoteller.setPrefHeight(ROW_HEIGHT * DEFAULT_ROWS);
        this.add(lblHotel, 0, 0);
        this.add(lvwHoteller, 0, 1, 2, 3);


        btnOpretHotel = new Button("Opret Hotel");
        this.add(btnOpretHotel, 0, 4);

        btnSletHotel = new Button("Slet Hotel");
        this.add(btnSletHotel, 1, 4);

        Label lblTilmeldte = new Label("Tilmeldte til hotel");
        lvwTilmeldinger = new ListView<>();



        lvwTilmeldinger.setPrefHeight(ROW_HEIGHT * DEFAULT_ROWS);
        this.add(lblTilmeldte, 2, 0);
        this.add(lvwTilmeldinger, 2, 1, 2, 3);

        ChangeListener<Tilmelding> listener2 =
                (ov, oldTilmelding, newTilmelding) -> this.selectedTilmeldtChanged(); // this.selectedHotelChanged()
        lvwTilmeldinger.getSelectionModel().selectedItemProperty().addListener(listener2);

        Label lblTilfoejelser = new Label("Tilfoejelser");
        lvwTilfoejelser = new ListView<>();

        lvwTilfoejelser.setPrefHeight(ROW_HEIGHT * (DEFAULT_ROWS / 4));
        lvwTilfoejelser.setPrefWidth(120);
        this.add(lblTilfoejelser, 4, 0);
        this.add(lvwTilfoejelser, 4, 1, 2, 1);

        lvwTilmeldinger.setCellFactory(lv -> new ListCell<Tilmelding>() {
            @Override
            public void updateItem(Tilmelding item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                    setText(null);
                } else {
                    setText(getLvwDeltagerName(item));
                }
            }
        });

        btnOpretTilfoejelse = new Button("Opret Tilføjelser");
        this.add(btnOpretTilfoejelse, 4, 2);


        lblPris = new Label("Pris: ");
        this.add(lblPris, 4, 4);

        btnOpretHotel.setOnAction(event -> this.opretHotelAction());
        btnSletHotel.setOnAction(event -> this.sletHotelAction());
        btnOpretTilfoejelse.setOnAction(event -> this.opretTilfoejelseAction());
    }

    /*************** Helper Metoder til CellFactory ***************/

    private String getLvwDeltagerName(Tilmelding tilmelding) {

        if (tilmelding.getLedsager() != null) {
            return tilmelding.toString()+ " (" + tilmelding.getLedsager().getNavn() + ")";
        }
        return tilmelding.getNavn();
    }

    private String textToDisplayInLvwTilfoejelser(Tilfoejelse tilfoejelse) {

        if (lvwTilmeldinger.getSelectionModel().getSelectedItem() != null && lvwTilmeldinger.getSelectionModel().getSelectedItem().getTilfoejelser().contains(tilfoejelse)) {

            if (lvwTilmeldinger.getSelectionModel().getSelectedItem() != null) {
                return tilfoejelse.toString() + "\t(" + lvwTilmeldinger.getSelectionModel().getSelectedItem().getDage(lvwTilmeldinger.getSelectionModel().getSelectedItem().getAnkomstDato(), lvwTilmeldinger.getSelectionModel().getSelectedItem().getAfgangsDato()) +" x " + tilfoejelse.getPris() + ")";
            }

            return tilfoejelse.toString();
        } else if (lvwTilmeldinger.getSelectionModel().getSelectedItem() == null) {
            return tilfoejelse.toString() + "\t(" +tilfoejelse.getPris() + ")";
        }else {

            if (lvwTilmeldinger.getSelectionModel().getSelectedItem() != null) {
                return tilfoejelse.toString() + "\t(" + lvwTilmeldinger.getSelectionModel().getSelectedItem().getDage(lvwTilmeldinger.getSelectionModel().getSelectedItem().getAnkomstDato(), lvwTilmeldinger.getSelectionModel().getSelectedItem().getAfgangsDato()) + " x " + tilfoejelse.getPris() + ")";
            }
            return tilfoejelse.toString();
        }
    }

    private Color colorForLvwTilfoejelser(Tilfoejelse tilfoejelse) {
        if (lvwTilmeldinger.getSelectionModel().getSelectedItem() != null && lvwTilmeldinger.getSelectionModel().getSelectedItem().getTilfoejelser().contains(tilfoejelse)) {
            return Color.GREEN;
        } else {
            return Color.BLACK;
        }
    }

    /*************** Metoder der return'er lister af objekter (til ListViews) ***************/

    private ArrayList<Hotel> initAlleHoteller(Konference konference) {
        return new ArrayList<>(Service.getHoteller(konference));
    }

    private ArrayList<Tilmelding> initAlleTilmeldinger(Hotel hotel) {
        return new ArrayList<>(Service.getTilmeldinger(konference, hotel));
    }

    private ArrayList<Tilfoejelse> initAlleTilfoejelser(Hotel hotel) {
        return new ArrayList<>(Service.getTilfoejelser(hotel));
    }

    /*************** Metoder der er bundet til ChangeListeners ***************/

    private void selectedHotelChanged() {
        this.updateControls();

        /* Tilfoejelses ListView, Med Custom CellFactory */
        lvwTilfoejelser.setCellFactory(lv -> new ListCell<Tilfoejelse>() {
            @Override
            public void updateItem(Tilfoejelse item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                    setText(null);
                } else {
                    setText(textToDisplayInLvwTilfoejelser(item));
                }
            }
        });

    }

    private void selectedTilmeldtChanged() {

        if (lvwTilmeldinger.getSelectionModel().getSelectedItem() != null) {

            Tilmelding tilmelding = lvwTilmeldinger.getSelectionModel().getSelectedItem();
            lblPris.setText("Pris: " + Service.getSamletHotelPris(tilmelding));

        } else {
            lblPris.setText("Pris: ");
        }

        lvwTilfoejelser.setCellFactory(lv -> new ListCell<Tilfoejelse>() {
            @Override
            public void updateItem(Tilfoejelse item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                    setText(null);
                } else {
                    setText(textToDisplayInLvwTilfoejelser(item));
                    setTextFill(colorForLvwTilfoejelser(item));
                }
            }
        });

    }

    /*************** Metoder Til Oprettelse og Sleting Af Objekter ***************/

    public void opretHotelAction() {
        HotelWindow opretHotel = new HotelWindow("Opret Hotel", this.konference);
        opretHotel.showAndWait();

        lvwHoteller.getItems().setAll(konference.getHoteller());
        int index = lvwHoteller.getItems().size() - 1;
        lvwHoteller.getSelectionModel().select(index);
    }

    public void opretTilfoejelseAction() {
        TilføjelsesWindow opretTilfoejelse = new TilføjelsesWindow("Opret Tilfoejelse", lvwHoteller.getSelectionModel().getSelectedItem());
        opretTilfoejelse.showAndWait();

        this.updateControls();
    }

    public void sletHotelAction() {
        Hotel hotel = lvwHoteller.getSelectionModel().getSelectedItem();
        if (hotel == null) {
            return;
        }

        Stage owner = (Stage) this.getScene().getWindow();
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Slet Hotel");
        alert.initOwner(owner);
        alert.setHeaderText("Er du sikker?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {

            for (Tilmelding tilmelding :hotel.getTilmeldinger()) {
                tilmelding.setHotel(null);
            }

            int selectIndex = lvwHoteller.getSelectionModel().getSelectedIndex();
            Service.deleteHotel(konference, hotel);
            lvwHoteller.getItems().setAll(this.initAlleHoteller(this.konference));
            if (selectIndex > 0) {
                lvwHoteller.getSelectionModel().select(selectIndex-1);
            } else {
                lvwHoteller.getSelectionModel().select(0);
            }

            this.updateControls();
        }
    }

    // ------------------------------------------------------------------------

    public void updateControls() {
        Hotel hotel = lvwHoteller.getSelectionModel().getSelectedItem();

        if (hotel != null) {
            ArrayList<Tilmelding> tilmeldinger = initAlleTilmeldinger(hotel);
            lvwTilmeldinger.getItems().setAll(tilmeldinger);

            ArrayList<Tilfoejelse> tilfoejelser = initAlleTilfoejelser(hotel);
            lvwTilfoejelser.getItems().setAll(tilfoejelser);
        } else {
            lvwTilmeldinger.getItems().setAll(new ArrayList<Tilmelding>());
            lvwTilfoejelser.getItems().setAll(new ArrayList<Tilfoejelse>());
        }

    }

}
