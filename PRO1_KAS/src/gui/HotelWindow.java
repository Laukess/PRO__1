package gui;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Adresse;
import model.Hotel;
import model.Konference;
import service.Service;

public class HotelWindow extends Stage {

	private Konference konference;

	public HotelWindow(String title, Konference konference) {
		this.initStyle(StageStyle.DECORATED);
		this.initModality(Modality.APPLICATION_MODAL);
		this.setResizable(false);

		this.konference = konference;

		this.setTitle(title);
		GridPane pane = new GridPane();
		this.initContent(pane);

		Scene scene = new Scene(pane);
		this.setScene(scene);
	}

	// -------------------------------------------------------------------------

	private TextField txfName, txfPrisSingle, txfPrisDouble, txfVej, txfBy,
			txfLand, txfPostNr, txfVejNr, txfEtage;
	private Label lblError;

	private void initContent(GridPane pane) {
		pane.setPadding(new Insets(10));
		pane.setHgap(10);
		pane.setVgap(10);
		pane.setGridLinesVisible(false);

		Label lblName = new Label("Navn");
		pane.add(lblName, 0, 0);
		txfName = new TextField();
		pane.add(txfName, 1, 0);

		Label lblPrisSingle = new Label("Pris for single");
		pane.add(lblPrisSingle, 0, 1);
		txfPrisSingle = new TextField();
		pane.add(txfPrisSingle, 1, 1);

		Label lblPrisDouble = new Label("Pris for double");
		pane.add(lblPrisDouble, 0, 2);
		txfPrisDouble = new TextField();
		pane.add(txfPrisDouble, 1, 2);

		Label lblAdresse = new Label("Adresse:");
		pane.add(lblAdresse, 1, 3);

		Label lblVej = new Label("Vej");
		pane.add(lblVej, 0, 4);
		txfVej = new TextField();
		pane.add(txfVej, 1, 4);

		Label lblVejNr = new Label("Vejnummer");
		pane.add(lblVejNr, 0, 5);
		txfVejNr = new TextField();
		pane.add(txfVejNr, 1, 5);

		Label lblEtage = new Label("Etage");
		pane.add(lblEtage, 0, 6);
		txfEtage = new TextField();
		pane.add(txfEtage, 1, 6);

		Label lblPostNr = new Label("Postnummer");
		pane.add(lblPostNr, 0, 7);
		txfPostNr = new TextField();
		pane.add(txfPostNr, 1, 7);

		Label lblBy = new Label("By");
		pane.add(lblBy, 0, 8);
		txfBy = new TextField();
		pane.add(txfBy, 1, 8);

		Label lblLand = new Label("Land");
		pane.add(lblLand, 0, 9);
		txfLand = new TextField();
		pane.add(txfLand, 1, 9);

		Button btnCancel = new Button("Cancel");
		pane.add(btnCancel, 0, 10);
		GridPane.setHalignment(btnCancel, HPos.LEFT);
		btnCancel.setOnAction(event -> this.cancelAction());

		Button btnOK = new Button("OK");
		btnOK.setDefaultButton(true);
		pane.add(btnOK, 1, 10);
		GridPane.setHalignment(btnOK, HPos.RIGHT);
		btnOK.setOnAction(event -> this.okAction());

		lblError = new Label();
		pane.add(lblError, 0, 11, 2, 1);
		lblError.setStyle("-fx-text-fill: red");

	}

	// -------------------------------------------------------------------------

	private void cancelAction() {
		this.hide();
	}

	private void okAction() {

		String name = txfName.getText().trim();
		if (name.length() == 0) {
			lblError.setText("Navn er tom");
			return;
		}

		double prisSingle;
		if (txfPrisSingle.getText().length() > 0) {
			try {
				prisSingle = Double.parseDouble(txfPrisSingle.getText().trim());
			} catch (Exception e) {
				lblError.setText("Pris for single skal være et tal");
				return;
			}
		} else {
			lblError.setText("Pris for single er tom");
			return;
		}

		double prisDouble;
		if (txfPrisDouble.getText().length() > 0) {
			try {
				prisDouble = Double.parseDouble(txfPrisDouble.getText().trim());
			} catch (Exception e) {
				lblError.setText("Pris for double skal være et tal");
				return;
			}
		} else {
			lblError.setText("Pris for double er tom");
			return;
		}

		String vej = txfVej.getText().trim();
		if (vej.length() == 0) {
			lblError.setText("Vej er tom");
			return;
		}

		String by = txfBy.getText().trim();
		if (by.length() == 0) {
			lblError.setText("By er tom");
			return;
		}

		String land = txfLand.getText().trim();
		if (land.length() == 0) {
			lblError.setText("Land er tom");
			return;
		}

		int postNr;
		if (txfPostNr.getText().length() > 0) {
			try {
				postNr = Integer.parseInt(txfPostNr.getText().trim());
			} catch (Exception e) {
				lblError.setText("Postnr skal være et tal");
				return;
			}
		} else {
			lblError.setText("Postnr er tom");
			return;
		}

		String vejNr = txfVejNr.getText().trim();
		if (vejNr.length() == 0) {
			lblError.setText("Vejnr er tom");
			return;
		}

		String etage = txfEtage.getText().trim();

		if (etage.equals("")) {
			etage = null;
		}

		Adresse adresse = Service.createAdresse(vej, vejNr, by, land, postNr, etage);
		Hotel hotel = Service.createHotel(name, prisDouble, prisSingle, adresse);
		Service.connectHotelTilKonference(this.konference, hotel);
		this.hide();

	}

}
