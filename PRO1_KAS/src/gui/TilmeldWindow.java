package gui;

import java.time.LocalDate;
import java.util.ArrayList;

import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Adresse;
import model.Firma;
import model.Hotel;
import model.Konference;
import model.Person;
import model.Tilfoejelse;
import model.Tilmelding;
import model.Udflugt;
import service.Service;

public class TilmeldWindow extends Stage {

	private Konference konference;
	private Person person;
    private Firma firma;

	public TilmeldWindow(String title, Konference konference) {
		this.initStyle(StageStyle.DECORATED);
		this.initModality(Modality.APPLICATION_MODAL);
		this.setResizable(false);

		this.konference = konference;

		this.setTitle(title);
		GridPane pane = new GridPane();
		this.initContent(pane);

		Scene scene = new Scene(pane);
		this.setScene(scene);
	}

	// -------------------------------------------------------------------------

	private TextField txfCpr, txfNavn, txfFirmanavn, txfFirmaTlf, txfVej, txfBy, txfLand, txfPostNr, txfVejNr, txfEtage, txfLedsager, txfAnkomstDato, txfAfgangsDato;
	private Label lblError;
	private ComboBox<Hotel> cbbHoteller;
	private CheckBox chkForedragsholder;
	private VBox vBoxTilfoejelser, vBoxUdflugter;

	private void initContent(GridPane pane) {
		pane.setPadding(new Insets(10));
		pane.setHgap(10);
		pane.setVgap(10);
		pane.setGridLinesVisible(false);

		Label lblCpr = new Label("CPR nummer");
		pane.add(lblCpr, 0, 0);
		txfCpr = new TextField();
		pane.add(txfCpr, 1, 0);

		Label lblNavn = new Label("Navn");
		pane.add(lblNavn, 0, 1);
		txfNavn = new TextField();
		pane.add(txfNavn, 1, 1);

        Label lblFirmaNavn = new Label("Firmanavn:");
        pane.add(lblFirmaNavn, 0, 2);
        txfFirmanavn = new TextField();
        pane.add(txfFirmanavn, 1, 2);

        Label lblFirmaTlf = new Label("Firma tlf:");
        pane.add(lblFirmaTlf, 0, 3);
        txfFirmaTlf = new TextField();
        pane.add(txfFirmaTlf, 1, 3);

		Label lblAdresse = new Label("Adresse oplysninger:");
		pane.add(lblAdresse, 1, 4);

		Label lblVej = new Label("Vej");
		pane.add(lblVej, 0, 5);
		txfVej = new TextField();
		pane.add(txfVej, 1, 5);

		Label lblVejNr = new Label("Vejnummer");
		pane.add(lblVejNr, 0, 6);
		txfVejNr = new TextField();
		pane.add(txfVejNr, 1, 6);

		Label lblEtage = new Label("Etage");
		pane.add(lblEtage, 0, 7);
		txfEtage = new TextField();
		pane.add(txfEtage, 1, 7);

		Label lblBy = new Label("By");
		pane.add(lblBy, 0, 8);
		txfBy = new TextField();
		pane.add(txfBy, 1, 8);

		Label lblPostNr = new Label("Postnummer");
		pane.add(lblPostNr, 0, 9);
		txfPostNr = new TextField();
		pane.add(txfPostNr, 1, 9);

		Label lblLand = new Label("Land");
		pane.add(lblLand, 0, 10);
		txfLand = new TextField();
		pane.add(txfLand, 1, 10);

		Label lblKonferenceOplysninger = new Label("Konferenceoplysninger:");
		pane.add(lblKonferenceOplysninger, 1, 11);

		Label lblLedsager = new Label("Ledsager");
		pane.add(lblLedsager, 0, 12);
		txfLedsager = new TextField();
		pane.add(txfLedsager, 1, 12);

		Label lblAnkomstDato = new Label("Ankomstdato");
		pane.add(lblAnkomstDato, 0, 13);
		txfAnkomstDato = new TextField();
		pane.add(txfAnkomstDato, 1, 13);

		Label lblAfgangsDato = new Label("Afgangsdato");
		pane.add(lblAfgangsDato, 0, 14);
		txfAfgangsDato = new TextField();
		pane.add(txfAfgangsDato, 1, 14);

		chkForedragsholder = new CheckBox("Foredragsholder"); //RYK NED
		pane.add(chkForedragsholder, 0, 15);

		lblError = new Label();
		pane.add(lblError, 1, 15);
		lblError.setStyle("-fx-text-fill: red");

		Label lblUdflugter = new Label("Udflugter:");
		pane.add(lblUdflugter, 0, 16);

		Label lblHoteller = new Label("Hoteller:");
		pane.add(lblHoteller, 1, 16);

		person = null;

		vBoxUdflugter = new VBox();
		pane.add(vBoxUdflugter, 0, 17, 1, 1);

		cbbHoteller = new ComboBox<>();
		pane.add(cbbHoteller, 1, 17);
		cbbHoteller.getItems().addAll(Service.getHoteller(konference));
		ChangeListener<Hotel> listener = (ov, oldHotel, newHotel) -> selectedHotelChanged(pane);
		cbbHoteller.getSelectionModel().selectedItemProperty().addListener(listener);

		Button btnCancel = new Button("Cancel");
		pane.add(btnCancel, 2, 17);
		btnCancel.setOnAction(event -> this.cancelAction());

		Button btnOK = new Button("OK");
		btnOK.setDefaultButton(true);
		pane.add(btnOK, 3, 17);
		btnOK.setOnAction(event -> this.okAction());

		vBoxTilfoejelser = new VBox();
		pane.add(vBoxTilfoejelser, 1, 18, 1, 1);

		Button btnSøg = new Button("Søg");
		pane.add(btnSøg, 3, 0);
		btnSøg.setOnAction(event -> søgAction(txfCpr.getText()));

		for(int i = 0; i < konference.getUdflugter().size(); i++) {
			Udflugt udflugt = konference.getUdflugter().get(i);
			CheckBox chk = new CheckBox(udflugt.getNavn());
			chk.setUserData(udflugt);
			vBoxUdflugter.getChildren().add(chk);
		}

	}

	// -------------------------------------------------------------------------

	private void søgAction(String cpr) {
		ArrayList<Person> personer = new ArrayList<>(Service.getPersoner());
		this.person = null;
		for (int i = 0; i < personer.size(); i++) {

			if(personer.get(i).getCPR().equalsIgnoreCase(cpr)) {
				txfNavn.setText(personer.get(i).getNavn());
				txfVej.setText(personer.get(i).getAdresse().getVejNavn());
				txfBy.setText(personer.get(i).getAdresse().getBy());
				txfLand.setText(personer.get(i).getAdresse().getLand());
				txfPostNr.setText(Integer.toString(personer.get(i).getAdresse().getPostNummer()));
				txfVejNr.setText(personer.get(i).getAdresse().getVejNummer());
				txfEtage.setText(personer.get(i).getAdresse().getEtage());

				person = personer.get(i);
				return;
			}
		}

		txfNavn.clear();
		txfVej.clear();
		txfBy.clear();
		txfLand.clear();
		txfPostNr.clear();
		txfVejNr.clear();
		txfEtage.clear();
	}

	private void cancelAction() {
		this.hide();
	}

	private void okAction() {

		String cpr = txfCpr.getText().trim();
		if (cpr.length() == 0) {
			lblError.setText("CPR er tom");
			return;
		}

		String navn = txfNavn.getText().trim();
		if(navn.length() == 0) {
			lblError.setText("Navn er tom");
			return;
		}

        String firmanavn = txfFirmanavn.getText().trim();
        String firmaNr = txfFirmaTlf.getText().trim();

        System.out.println("navn: " + firmanavn.length());
        System.out.println("tlf:" +firmaNr.length());

        if (firmanavn.length() <= 0 && firmaNr.length() > 0 ) {
            lblError.setText("Mangler firmanavn");
            return;
        }
        if (firmanavn.length() > 0 && firmaNr.length() <= 0 ) {
            lblError.setText("Mangler telefonnummer");
            return;
        }

        if (firmanavn.length() > 0 && firmaNr.length() > 0 ) {

            String regex = "[0-9]+";

            if (firmaNr.matches(regex)) {
                this.firma = Service.createFirma(firmanavn, firmaNr);
            } else {
                lblError.setText("firmanummer skal vaere et nummer");
                this.firma = null;
                return;
            }

        } else {
            this.firma = null;
        }

		String vej = txfVej.getText().trim();
		if (vej.length() == 0) {
			lblError.setText("Vej er tom");
			return;
		}

		String by = txfBy.getText().trim();
		if (by.length() == 0) {
			lblError.setText("By er tom");
			return;
		}

		String land = txfLand.getText().trim();
		if (land.length() == 0) {
			lblError.setText("Land er tom");
			return;
		}

		//		int postNr = -1;
		if (txfPostNr.getText().trim().length() == 0) {
			lblError.setText("Post nummer er tom");
			return;
		}
		int postNr;
		try {
			postNr = Integer.parseInt(txfPostNr.getText().trim());
		} catch (Exception e) {
			lblError.setText("post nummer skal vaere et tal");
			return;
		}

		String vejNr = txfVejNr.getText().trim();
		if (vejNr.length() == 0) {
			lblError.setText("Vej nummer er tom");
			return;
		}

        String etage = txfEtage.getText().trim();

        if (etage.equals("")) {
            etage = null;
        }

		String ledsager = txfLedsager.getText().trim();
		if (ledsager.equals("")) {
			ledsager = null;
		}

		String startDato = txfAnkomstDato.getText().trim();
		if (startDato.length() == 0) {
			lblError.setText("Manglende startdato");
			return;
		}
		LocalDate date1 = null;
		try {
			date1 = LocalDate.parse(txfAnkomstDato.getText().trim());
		} catch (Exception e) {
			lblError.setText("Forkert startdato input");
			return;
		}

		String slutDato = txfAfgangsDato.getText().trim();
		if (slutDato.length() == 0) {
			lblError.setText("Manglende slutdato");
			return;
		}

		LocalDate date2 = null;
		try {
			date2 = LocalDate.parse(txfAfgangsDato.getText().trim());
		} catch (Exception e) {
			lblError.setText("Forkert slutdato input");
			return;
		}

		boolean foredragsholder = chkForedragsholder.isSelected();

		Hotel valgtHotel = cbbHoteller.getSelectionModel().getSelectedItem();

		ArrayList<Udflugt> udflugter = new ArrayList<>();
		for (Node node : vBoxUdflugter.getChildren()) {
			CheckBox chk = (CheckBox) node;

			if (chk.isSelected()) {
				udflugter.add((Udflugt) chk.getUserData());
			}

		}

		ArrayList<Tilfoejelse> tilfoejelser = new ArrayList<>();
		for (Node node : vBoxTilfoejelser.getChildren()) {
			CheckBox chk = (CheckBox) node;

			if (chk.isSelected()) {
				tilfoejelser.add((Tilfoejelse) chk.getUserData());
			}

		}

		for (Tilmelding t : konference.getTilmeldinger()) {
			if (t.getPerson() == person) {
				lblError.setText("Du er allede tilmeldt");
				return;
			}
		}

		if (person == null) {
			Adresse adresse = new Adresse(vej, vejNr, by, land, postNr, etage);

			Service.createTilmelding(
					valgtHotel, date1, date2,
					foredragsholder, tilfoejelser, ledsager, udflugter, this.firma, konference,
					navn, cpr, adresse
			);

		} else {

			Service.createTilmelding(
					valgtHotel, date1, date2,
					foredragsholder, tilfoejelser, ledsager, udflugter, this.firma, konference,
					person
			);
		}

		this.hide();

	}

	private void selectedHotelChanged(GridPane pane) {
		if(cbbHoteller.getSelectionModel().getSelectedItem() != null) {
			Hotel hotel = cbbHoteller.getSelectionModel().getSelectedItem();
			vBoxTilfoejelser.getChildren().clear();
			for(int i = 0; i < hotel.getTilfoejelser().size(); i++) {
				Tilfoejelse tilfoejelse = hotel.getTilfoejelser().get(i);
				CheckBox chk = new CheckBox(hotel.getTilfoejelser().get(i).getNavn());
				chk.setUserData(tilfoejelse);
				vBoxTilfoejelser.getChildren().add(chk);
			}
			pane.requestLayout();
			pane.getScene().getWindow().sizeToScene();
		}
	}

}