package gui;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Adresse;
import model.Hotel;
import model.Tilfoejelse;
import service.Service;

public class TilføjelsesWindow extends Stage {
	private Hotel hotel;

	public TilføjelsesWindow(String title, Hotel hotel) {
		this.initStyle(StageStyle.DECORATED);
		this.initModality(Modality.APPLICATION_MODAL);
		this.setResizable(false);

		this.hotel = hotel;

		this.setTitle(title);
		GridPane pane = new GridPane();
		this.initContent(pane);

		Scene scene = new Scene(pane);
		this.setScene(scene);
	}

	// -------------------------------------------------------------------------

	private TextField txfName, txfPris;
	private Label lblError;

	private void initContent(GridPane pane) {
		pane.setPadding(new Insets(10));
		pane.setHgap(10);
		pane.setVgap(10);
		pane.setGridLinesVisible(false);

		Label lblName = new Label("Tilføjelse");
		pane.add(lblName, 0, 0);
		txfName = new TextField();
		pane.add(txfName, 1, 0);

		Label lblPris = new Label("Pris");
		pane.add(lblPris, 0, 2);
		txfPris = new TextField();
		pane.add(txfPris, 1, 2);

		Button btnCancel = new Button("Cancel");
		pane.add(btnCancel, 0, 5);
		GridPane.setHalignment(btnCancel, HPos.LEFT);
		btnCancel.setOnAction(event -> this.cancelAction());

		Button btnOK = new Button("OK");
		btnOK.setDefaultButton(true);
		pane.add(btnOK, 1, 5);
		GridPane.setHalignment(btnOK, HPos.RIGHT);
		btnOK.setOnAction(event -> this.okAction());

		lblError = new Label();
		pane.add(lblError, 0, 6, 2, 1);
		lblError.setStyle("-fx-text-fill: red");

	}

	// -------------------------------------------------------------------------

	private void cancelAction() {
		this.hide();
	}

	private void okAction() {

		String name = txfName.getText().trim();
		if (name.length() == 0) {
			lblError.setText("Tilføjelse er tom");
			return;
		}

		int pris = -1;

		try {
			pris = Integer.parseInt(txfPris.getText().trim());
			if (pris <= 0) {
				lblError.setText("Pris er tom");
				return;
			}
		} catch (Exception e) {
			lblError.setText("prisen skal være et tal");
			return;
		}

		Service.createTilfoejelse(name, pris, this.hotel);

		this.hide();
	}

}
