package storage;

import model.*;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by lukas on 27-04-2016.
 */
public class Storage {

    public static ArrayList<Konference> konferencer = new ArrayList<>();
    public static ArrayList<Person> personer = new ArrayList<>();

    public Storage() {

    }

    /****************** Get Metoder ******************/

    public static ArrayList<Konference> getKonferencer() {
        return konferencer;
    }

    public static ArrayList<Person> getPersoner() {
        return personer;
    }

    /****************** Add Metoder ******************/

    public static void addKonference(Konference konference) {
        konferencer.add(konference);
    }

    public static void addPerson(Person person) {
        personer.add(person);
    }

    /****************** Delete Metoder ******************/

    public static void deleteHotel(Konference konference, Hotel hotel) {
        ArrayList<Hotel> hoteller = konference.getHoteller();
        hoteller.remove(hotel);
    }

    public static void deleteUdflugt(Konference konference, Udflugt udflugt) {
        ArrayList<Udflugt> udflugter = konference.getUdflugter();
        udflugter.remove(udflugt);
    }

    public static void deleteKonference(Konference konference) {
        konferencer.remove(konference);
    }

    /****************** Remove Metoder ******************/

    public static void removeTilmelding(Tilmelding tilmelding) {

        for (Hotel h : tilmelding.getKonference().getHoteller()) {
            if (h.getTilmeldinger().contains(tilmelding)) {
                h.removeTilmelding(tilmelding);
            }
        }

        if(tilmelding.getLedsager() != null) {

            for (Udflugt u : tilmelding.getLedsager().getUdflugter()) {
                u.removeLedsager(tilmelding.getLedsager());
            }
            tilmelding.setLedsager(null);
        }
        tilmelding.getKonference().removeTilmelding(tilmelding);
    }

}
