package model;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Created by lukas on 27-04-2016.
 */
public class Udflugt {

    private double pris;
    private LocalDate dato;
    private String navn;
    private String moedested;
    private ArrayList<Ledsager> ledsagere = new ArrayList<>();

    public Udflugt(double pris, LocalDate dato, String navn, String moedested) {
        this.pris = pris;
        this.dato = dato;
        this.navn = navn;
        this.moedested = moedested;
    }

    @Override
    public String toString() {
        return this.navn;
    }

    /****************** Getter og Setter Metoder ******************/

    public double getPris() {
        return pris;
    }

    public void setPris(double pris) {
        this.pris = pris;
    }

    public LocalDate getDato() {
        return dato;
    }

    public void setDato(LocalDate dato) {
        this.dato = dato;
    }

    public String getNavn() {
        return navn;
    }

    public void setNavn(String navn) {
        this.navn = navn;
    }

    public String getMoedested() {
        return moedested;
    }

    public void setMoedested(String moedested) {
        this.moedested = moedested;
    }


    public void setLedsagere(ArrayList<Ledsager> ledsagere) {
        this.ledsagere = ledsagere;
    }

    /****************** ArrayList -> Get, Add og Remove ******************/

    public ArrayList<Ledsager> getLedsagere() {
        return ledsagere;
    }

    public void addLedsager(Ledsager ledsager) {
        if (ledsager != null) {
            this.ledsagere.add(ledsager);
        }
    }

    public void removeLedsager(Ledsager ledsager) {
        System.out.println(this.ledsagere.remove(ledsager));
    }


    /****************** Helper Metoder ******************/



}
