package model;

import service.Service;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

/**
 * Created by lukas on 27-04-2016.
 */
public class Tilmelding {

    private Person person;
    private LocalDate ankomstDato;
    private LocalDate afgangsDato;
    private boolean fordragsholder;
    private ArrayList<Tilfoejelse> tilfoejelser = new ArrayList<>();
    private Ledsager ledsager;
    private Firma firma;
    private Konference konference;
    private Hotel hotel;

    public Tilmelding(Hotel hotel, LocalDate ankomstDato, LocalDate afgangsDato,
                      boolean fordragsholder, ArrayList<Tilfoejelse> tilfoejelser,
                      Firma firma, Konference konference
                      ) {
        this.hotel = hotel;
        this.ankomstDato = ankomstDato;
        this.afgangsDato = afgangsDato;
        this.fordragsholder = fordragsholder;
        if (tilfoejelser != null) {
            this.tilfoejelser = tilfoejelser;
        }
        this.firma = firma;
        this.konference = konference;
    }

    public Tilmelding(Hotel hotel, LocalDate ankomstDato, LocalDate afgangsDato,
                      boolean fordragsholder, ArrayList<Tilfoejelse> tilfoejelser,
                      Firma firma, Konference konference, Person person) {
        this.hotel = hotel;
        this.ankomstDato = ankomstDato;
        this.afgangsDato = afgangsDato;
        this.fordragsholder = fordragsholder;
        if (tilfoejelser != null) {
            this.tilfoejelser = tilfoejelser;
        }
        this.firma = firma;
        this.konference = konference;
        this.person = person;
    }


    @Override
    public String toString() {
        return this.getNavn();
    }

    /****************** Getter og Setter Metoder ******************/

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public LocalDate getAnkomstDato() {
        return ankomstDato;
    }

    public void setAnkomstDato(LocalDate ankomstDato) {
        this.ankomstDato = ankomstDato;
    }

    public LocalDate getAfgangsDato() {
        return afgangsDato;
    }

    public void setAfgangsDato(LocalDate afgangsDato) {
        this.afgangsDato = afgangsDato;
    }

    public boolean isFordragsholder() {
        return fordragsholder;
    }

    public void setFordragsholder(boolean fordragsholder) {
        this.fordragsholder = fordragsholder;
    }

    public Ledsager getLedsager() {
        return this.ledsager;
    }

    public void setLedsager(Ledsager ledsager) {
        this.ledsager = ledsager;
    }

    public Firma getFirma() {
        return firma;
    }

    public void setFirma(Firma firma) {
        this.firma = firma;
    }

    public Konference getKonference() {
        return konference;
    }

    public void setKonference(Konference konference) {
        this.konference = konference;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }


    public void setTilfoejelser(ArrayList<Tilfoejelse> tilfoejelser) {
        this.tilfoejelser = tilfoejelser;
    }

    /****************** ArrayList -> Get, Add og Remove ******************/

    public ArrayList<Tilfoejelse> getTilfoejelser() {
        return tilfoejelser;
    }

    public void addTilfoejelse(Tilfoejelse tilfoejelse) {
        this.tilfoejelser.add(tilfoejelse);
    }

    public void removeTilfoejelse(Tilfoejelse tilfoejelse) {
        tilfoejelser.remove(tilfoejelse);
    }

    /****************** Create Objekter ******************/

    public Person createPerson(String navn, String CPR, Adresse adresse) {
        Person person = new Person(navn, CPR, adresse);
        this.person = person;
        return  person;
    }

    public Ledsager createLedsager(String navn, ArrayList<Udflugt> udflugter) {
        return new Ledsager(navn, this, udflugter);
    }

    /****************** Helper Metoder ******************/

    public double getPris() {
        double konferencePris = 0;
        if (!isFordragsholder()) {
            konferencePris = getKonference().getPris() * (getDage(ankomstDato, afgangsDato) + 1) ;
        }

        double ledsagerPris = 0;
        if (getLedsager() != null && getLedsager().getUdflugter() != null) {
            ArrayList<Udflugt> ledsagerUdflugter = getLedsager().getUdflugter();

            for (Udflugt u : ledsagerUdflugter) {
                ledsagerPris += u.getPris();
            }
        }

        double hotelPris = 0;
        if (getHotel() != null) {
            if(getLedsager() != null) {
                hotelPris += getHotel().getPrisDouble() * (getDage(ankomstDato, afgangsDato)) ;
            } else {
                hotelPris += getHotel().getPrisSingle() * (getDage(ankomstDato, afgangsDato));
            }

            if (getTilfoejelser() != null && getTilfoejelser().size() > 0 )  {
                ArrayList<Tilfoejelse> tilfoejelser = getTilfoejelser();
                for(Tilfoejelse t : tilfoejelser) {
                    hotelPris += t.getPris() * (getDage(ankomstDato, afgangsDato)); /*ADDED!!!*/
                }
            }
        }

        return konferencePris + ledsagerPris + hotelPris;
    }

    public long getDage(LocalDate first, LocalDate second) {
        return ChronoUnit.DAYS.between(first, second);
    }

    public String getNavn() {
        return this.person.getNavn();
    }
}
