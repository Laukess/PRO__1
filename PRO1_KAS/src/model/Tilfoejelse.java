package model;

/**
 * Created by lukas on 27-04-2016.
 */
public class Tilfoejelse {

    private String navn;
    private double pris;
    private Hotel hotel;

    public Tilfoejelse(String navn, double pris, Hotel hotel) {
        this.navn = navn;
        this.pris = pris;
        this.hotel = hotel;
    }

    @Override
    public String toString() {
        return this.navn;
    }

    /****************** Getter og Setter Metoder ******************/

    public String getNavn() {
        return navn;
    }

    public void setNavn(String navn) {
        this.navn = navn;
    }

    public double getPris() {
        return pris;
    }

    public void setPris(double pris) {
        this.pris = pris;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

}
