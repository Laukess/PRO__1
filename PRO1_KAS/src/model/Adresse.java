package model;

/**
 * Created by lukas on 27-04-2016.
 */
public class Adresse {

    private String vejNavn;
    private String vejNummer;
    private String by;
    private String land;
    private String etage;
    private int postNummer;

    public Adresse(String vejNavn, String vejNummer, String by, String land, int postNummer, String etage) {
        this.vejNavn = vejNavn;
        this.vejNummer = vejNummer;
        this.by = by;
        this.land = land;
        this.etage = etage;
        this.postNummer = postNummer;
    }

    /****************** Getter og Setter Metoder ******************/

    public String getVejNavn() {
        return vejNavn;
    }

    public void setVejNavn(String vejNavn) {
        this.vejNavn = vejNavn;
    }

    public String getVejNummer() {
        return vejNummer;
    }

    public void setVejNummer(String vejNummer) {
        this.vejNummer = vejNummer;
    }

    public String getBy() {
        return by;
    }

    public void setBy(String by) {
        this.by = by;
    }

    public String getLand() {
        return land;
    }

    public void setLand(String land) {
        this.land = land;
    }

    public String getEtage() {
        return etage;
    }

    public void setEtage(String etage) {
        this.etage = etage;
    }

    public int getPostNummer() {
        return postNummer;
    }

    public void setPostNummer(int postNummer) {
        this.postNummer = postNummer;
    }
}
