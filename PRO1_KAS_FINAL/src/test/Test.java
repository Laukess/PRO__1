package test;

import java.time.LocalDate;
import java.util.ArrayList;

import model.Adresse;
import model.Hotel;
import model.Konference;
import model.Tilfoejelse;
import model.Udflugt;

public class Test {

	public static void main(String[] args) {
		Adresse adresse = new Adresse("vej 1", "2b", "Brabrand", "Danmark", 8220, "2.6");
		Konference k = new Konference("test", LocalDate.of(2016, 4, 20), LocalDate.of(2016, 4, 22), adresse, 1500);

		Hotel hotel = new Hotel("hilton", 1250, 1050, adresse);

		Tilfoejelse tf = new Tilfoejelse("Wifi", 50, hotel);
		//        Tilfoejelse bb = new Tilfoejelse("bad", 820, hotel);
		ArrayList<Tilfoejelse> tilfoejelser = new ArrayList<>();
		tilfoejelser.add(tf);
		//        tilfoejelser.add(bb);

		Udflugt u = new Udflugt(75, LocalDate.of(2015, 12, 12), "striking", "der");
		Udflugt u1 = new Udflugt(200, LocalDate.of(2015, 12, 12), "striking", "der");
		Udflugt u2 = new Udflugt(125, LocalDate.of(2015, 12, 12), "striking", "der");
		ArrayList<Udflugt> udflugter = new ArrayList<>();
		ArrayList<Udflugt> udflugter2 = new ArrayList<>();
		udflugter.add(u);
		udflugter.add(u1);

		udflugter2.add(u);
		udflugter2.add(u2);

		//        Person p = new Person("peter", "12", adresse, null);
		//        Firma firma = new Firma("vertica", 12121212);
		//        Tilmelding tilmelding = new Tilmelding(hotel, p, LocalDate.of(2016, 4, 20), LocalDate.of(2016, 4, 22), false, tilfoejelser, "Kaj", udflugter, firma, k);

		//        System.out.println(Service.getSamletTilmeldingsPris(tilmelding));

		//        Tilmelding tilmelding1 = new Tilmelding(null, p, LocalDate.of(2016, 4, 20), LocalDate.of(2016, 4, 22), false, null, null, null, null, k);
		//        Tilmelding tilmelding2 = new Tilmelding(hotel, p, LocalDate.of(2016, 4, 20), LocalDate.of(2016, 4, 22), false, null, null, null, null, k);
		//        Tilmelding tilmelding3 = new Tilmelding(hotel, p, LocalDate.of(2016, 4, 20), LocalDate.of(2016, 4, 22), false, tilfoejelser, "aoeu", udflugter, null, k);
		//        Tilmelding tilmelding4 = new Tilmelding(hotel, p, LocalDate.of(2016, 4, 20), LocalDate.of(2016, 4, 22), true, tilfoejelser, "oeuou", udflugter2, null, k);


		//        System.out.println(Service.getSamletTilmeldingsPris(tilmelding1));
		//        System.out.println(Service.getSamletTilmeldingsPris(tilmelding2));
		//        System.out.println(Service.getSamletTilmeldingsPris(tilmelding3));
		//        System.out.println(Service.getSamletTilmeldingsPris(tilmelding4));

	}

}
