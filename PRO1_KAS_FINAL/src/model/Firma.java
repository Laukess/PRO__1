package model;

public class Firma {

	private String navn;
	private String tlf;

	public Firma(String navn, String tlf) {
		this.navn = navn;
		this.tlf = tlf;
	}

	/****************** Getter og Setter Metoder ******************/

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public String getTlf() {
		return tlf;
	}

	public void setTlf(String tlf) {
		this.tlf = tlf;
	}
}
