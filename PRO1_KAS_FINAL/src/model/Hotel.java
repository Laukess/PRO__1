package model;

import java.util.ArrayList;

public class Hotel {

	private String navn;
	private double prisDouble;
	private double prisSingle;
	private ArrayList<Tilfoejelse> tilfoejelser = new ArrayList<>();
	private Adresse adresse;
	private ArrayList<Tilmelding> tilmeldinger = new ArrayList<>();

	public Hotel(String navn, double prisDouble, double prisSingle, Adresse adresse) {
		this.navn = navn;
		this.prisDouble = prisDouble;
		this.prisSingle = prisSingle;
		this.adresse = adresse;
	}

	@Override
	public String toString() {
		return this.getNavn();
	}

	public double getSamletHotelPris(Tilmelding tilmelding) {
		double samletPris = 0.0;
		if (tilmelding.getLedsager() == null) {
			samletPris += tilmelding.getHotel().getPrisSingle() * (tilmelding.getDage(tilmelding.getAnkomstDato(), tilmelding.getAfgangsDato()));
		} else {
			samletPris += tilmelding.getHotel().getPrisDouble() * (tilmelding.getDage(tilmelding.getAnkomstDato(), tilmelding.getAfgangsDato()));
		}
		if (tilmelding.getTilfoejelser() != null && tilmelding.getTilfoejelser().size() > 0) {
			for (Tilfoejelse t : tilmelding.getTilfoejelser()) {
				samletPris += t.getPris() * (tilmelding.getDage(tilmelding.getAnkomstDato(), tilmelding.getAfgangsDato()));
			}
		}
		return samletPris;
	}

	/****************** Getter og Setter Metoder ******************/

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public double getPrisDouble() {
		return prisDouble;
	}

	public void setPrisDouble(double prisDouble) {
		this.prisDouble = prisDouble;
	}

	public double getPrisSingle() {
		return prisSingle;
	}

	public void setPrisSingle(double prisSingle) {
		this.prisSingle = prisSingle;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}


	public void setTilfoejelser(ArrayList<Tilfoejelse> tilfoejelser) {
		this.tilfoejelser = tilfoejelser;
	}

	public void setTilmeldinger(ArrayList<Tilmelding> tilmeldinger) {
		this.tilmeldinger = tilmeldinger;
	}

	/****************** ArrayList -> Get, Add og Remove ******************/

	public void addTilfoejelse(Tilfoejelse tilfoejelse) {
		this.tilfoejelser.add(tilfoejelse);
	}

	public void addTilmelding(Tilmelding tilmelding) {
		this.tilmeldinger.add(tilmelding);
	}

	public ArrayList<Tilmelding> getTilmeldinger() {
		return tilmeldinger;
	}

	public ArrayList<Tilfoejelse> getTilfoejelser() {
		return tilfoejelser;
	}

	public void removeTilmelding(Tilmelding tilmelding) {
		tilmeldinger.remove(tilmelding);
	}

	public void removeTilfoejelse(Tilfoejelse tilfoejelse) {
		tilfoejelser.remove(tilfoejelse);
	}
}
