package model;

import java.util.ArrayList;

public class Person {

	private String navn;
	private String CPR;
	private Adresse adresse;
	private ArrayList<Tilmelding> tilmeldinger = new ArrayList<>();

	public Person(String navn, String CPR, Adresse adresse) {
		this.navn = navn;
		this.CPR = CPR;
		this.adresse = adresse;
	}

	/****************** Getter og Setter Metoder ******************/

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public String getCPR() {
		return CPR;
	}

	public void setCPR(String CPR) {
		this.CPR = CPR;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}


	public void setTilmeldinger(ArrayList<Tilmelding> tilmeldinger) {
		this.tilmeldinger = tilmeldinger;
	}

	/****************** ArrayList -> Get, Add og Remove ******************/

	public ArrayList<Tilmelding> getTilmeldinger() {
		return tilmeldinger;
	}

	public void addTilmelding(Tilmelding tilmelding) {
		tilmeldinger.add(tilmelding);
	}

	public void removeTilmelding(Tilmelding tilmelding) {
		tilmeldinger.remove(tilmelding);
	}

}
