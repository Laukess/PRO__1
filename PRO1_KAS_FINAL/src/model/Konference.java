package model;

import java.time.LocalDate;
import java.util.ArrayList;

public class Konference {

	private String navn;
	private LocalDate startDato;
	private LocalDate slutDato;
	private double pris;

	private Adresse adresse;

	private ArrayList<Hotel> hoteller = new ArrayList<>();
	private ArrayList<Udflugt> udflugter = new ArrayList<>();
	private ArrayList<Tilmelding> tilmeldinger = new ArrayList<>();

	public Konference(String navn, LocalDate startDato, LocalDate slutDato, Adresse adresse, double pris) {
		this.navn = navn;
		this.startDato = startDato;
		this.slutDato = slutDato;
		this.adresse = adresse;
		this.pris = pris;
	}

	@Override
	public String toString() {
		return ""+ this.navn + "\t"+this.startDato ;
	}

	public Udflugt createUdflugt(double pris, LocalDate dato, String navn, String moedested) {
		Udflugt udflugt = new Udflugt(pris, dato, navn, moedested);
		this.addUdflugt(udflugt);
		return udflugt;
	}

	/****************** Getter og Setter Metoder ******************/

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public LocalDate getStartDato() {
		return startDato;
	}

	public void setStartDato(LocalDate startDato) {
		this.startDato = startDato;
	}

	public LocalDate getSlutDato() {
		return slutDato;
	}

	public void setSlutDato(LocalDate slutDato) {
		this.slutDato = slutDato;
	}

	public double getPris() {
		return pris;
	}

	public void setPris(double pris) {
		this.pris = pris;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

	/****************** ArrayList -> Get, Add og Remove ******************/

	public ArrayList<Hotel> getHoteller() {
		return this.hoteller;
	}

	public ArrayList<Udflugt> getUdflugter() {
		return udflugter;
	}

	public ArrayList<Tilmelding> getTilmeldinger() {
		return tilmeldinger;
	}

	public void addHotel(Hotel hotel) {
		this.hoteller.add(hotel);
	}

	public void addUdflugt(Udflugt udflugt) {
		this.udflugter.add(udflugt);
	}

	public void addTilmelding(Tilmelding tilmelding) {
		this.tilmeldinger.add(tilmelding);
	}

	public void removeHotel(Hotel hotel) { this.hoteller.remove(hotel); };

	public void removeUdflugt(Udflugt udflugt) { this.udflugter.remove(udflugt); }

	public void removeTilmelding(Tilmelding tilmelding) {
		this.tilmeldinger.remove(tilmelding);
	}

}
