package model;

import java.util.ArrayList;

public class Ledsager {

	private String navn;
	private ArrayList<Udflugt> udflugter;
	private Tilmelding tilmelding;

	public Ledsager(String navn, Tilmelding tilmelding, ArrayList<Udflugt> udflugter) {
		this.navn = navn;
		this.udflugter = udflugter;
		this.tilmelding = tilmelding;
	}

	@Override
	public String toString() {
		return this.navn + " (" + this.tilmelding.getPerson().getNavn()+ ")";
	}

	/****************** Getter og Setter Metoder ******************/

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public Tilmelding getTilmelding() {
		return tilmelding;
	}

	public void setTilmelding(Tilmelding tilmelding) {
		this.tilmelding = tilmelding;
	}


	public void setUdflugter(ArrayList<Udflugt> udflugter) {
		this.udflugter = udflugter;
	}

	/****************** ArrayList -> Get, Add og Remove ******************/

	public ArrayList<Udflugt> getUdflugter() {
		return udflugter;
	}

	public void addUdflugt(Udflugt udflugt) {
		this.udflugter.add(udflugt);
	}

	public void removeUdflugt(Udflugt udflugt) {
		udflugter.remove(udflugt);
	}

}
