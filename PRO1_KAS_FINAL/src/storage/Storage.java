package storage;

import java.util.ArrayList;

import model.Hotel;
import model.Konference;
import model.Person;
import model.Tilmelding;
import model.Udflugt;

public class Storage {

	public static ArrayList<Konference> konferencer = new ArrayList<>();
	public static ArrayList<Person> personer = new ArrayList<>();

	public Storage() {

	}

	/****************** Get Metoder ******************/

	public static ArrayList<Konference> getKonferencer() {
		return konferencer;
	}

	public static ArrayList<Person> getPersoner() {
		return personer;
	}

	/****************** Add Metoder ******************/

	public static void addKonference(Konference konference) {
		konferencer.add(konference);
	}

	public static void addPerson(Person person) {
		personer.add(person);
	}

	/****************** Delete Metoder ******************/

	public static void removeHotel(Konference konference, Hotel hotel) {
		ArrayList<Hotel> hoteller = konference.getHoteller();
		hoteller.remove(hotel);
	}

	public static void removeUdflugt(Konference konference, Udflugt udflugt) {
		ArrayList<Udflugt> udflugter = konference.getUdflugter();
		udflugter.remove(udflugt);
	}

	public static void removeKonference(Konference konference) {
		konferencer.remove(konference);
	}

	/****************** Remove Metoder ******************/

	public static void removeTilmelding(Tilmelding tilmelding) {

		for (Hotel h : tilmelding.getKonference().getHoteller()) {
			if (h.getTilmeldinger().contains(tilmelding)) {
				h.removeTilmelding(tilmelding);
			}
		}

		if(tilmelding.getLedsager() != null) {

			for (Udflugt u : tilmelding.getLedsager().getUdflugter()) {
				u.removeLedsager(tilmelding.getLedsager());
			}
			tilmelding.setLedsager(null);
		}
		tilmelding.getKonference().removeTilmelding(tilmelding);
	}

}
