package service;

import java.time.LocalDate;
import java.util.ArrayList;

import model.Adresse;
import model.Firma;
import model.Hotel;
import model.Konference;
import model.Ledsager;
import model.Person;
import model.Tilfoejelse;
import model.Tilmelding;
import model.Udflugt;
import storage.Storage;

public class Service {

	/****************** Create Metoder ******************/

	public static Firma createFirma(String navn, String tlf) {
		return new Firma(navn, tlf);
	}

	public static Adresse createAdresse(String vejNavn, String vejNummer, String by, String land, int postNummer, String etage) {
		return new Adresse(vejNavn, vejNummer, by, land, postNummer, etage);
	}

	public static Konference createKonference(String navn, LocalDate startDato, LocalDate slutDato, Adresse adresse, double pris) {
		Konference k = new Konference(navn, startDato, slutDato, adresse, pris);
		Storage.addKonference(k);
		return k;
	}

	public static Udflugt createUdflugt(Konference konference, double pris, LocalDate dato, String navn, String moedested) {
		return konference.createUdflugt(pris, dato, navn, moedested);
	}

	public static Tilfoejelse createTilfoejelse(String navn, int pris, Hotel hotel) {
		Tilfoejelse tilfoejelse = new Tilfoejelse(navn, pris, hotel);
		hotel.addTilfoejelse(tilfoejelse);
		return tilfoejelse;
	}

	public static Tilmelding createTilmelding(Hotel hotel, LocalDate startDato, LocalDate slutDato,
			boolean foredragsholder, ArrayList<Tilfoejelse> tilfoejelser,
			String ledsagerNavn, ArrayList<Udflugt> udflugter, Firma firma,
			Konference konference, String navn, String cpr, Adresse adresse) {

		Tilmelding tilmelding = new Tilmelding(hotel, startDato, slutDato, foredragsholder, tilfoejelser, firma, konference);
		konference.addTilmelding(tilmelding);
		if (hotel != null) {
			hotel.addTilmelding(tilmelding);
		}
		Person person = tilmelding.createPerson(navn, cpr, adresse);
		Storage.addPerson(person);

		if (ledsagerNavn == null) {
			udflugter = null;
		} else {
			Ledsager ledsager = tilmelding.createLedsager(ledsagerNavn, udflugter); //TODO KIG HER
			tilmelding.setLedsager(ledsager);
			if (ledsager.getUdflugter() != null) {
				for (Udflugt udflugt : ledsager.getUdflugter()) {
					udflugt.addLedsager(ledsager);
				}
			}

		}

		return tilmelding;
	}

	public static Tilmelding createTilmelding(Hotel hotel, LocalDate startDato, LocalDate slutDato,
			boolean foredragsholder, ArrayList<Tilfoejelse> tilfoejelser,
			String ledsagerNavn, ArrayList<Udflugt> udflugter, Firma firma,
			Konference konference, Person person) {

		Tilmelding tilmelding = new Tilmelding(hotel, startDato, slutDato, foredragsholder, tilfoejelser, firma, konference, person);

		konference.addTilmelding(tilmelding);

		if (hotel != null) {
			hotel.addTilmelding(tilmelding);
		}

		if (ledsagerNavn == null) {
			udflugter = null;
		} else {
			Ledsager ledsager = tilmelding.createLedsager(ledsagerNavn, udflugter);
			tilmelding.setLedsager(ledsager);
			for (Udflugt udflugt : ledsager.getUdflugter()) {
				udflugt.addLedsager(ledsager);
			}
		}

		return tilmelding;
	}

	public static Hotel createHotel(String navn, double prisDouble, double prisSingle, Adresse adresse) {
		return new Hotel(navn, prisDouble, prisSingle, adresse);
	}

	/****************** Remove Metoder ******************/

	public static void removeTilmelding(Tilmelding tilmelding) {
		Storage.removeTilmelding(tilmelding);
	}

	public static void removeHotel(Konference konference, Hotel hotel) {
		Storage.removeHotel(konference, hotel);
	}

	public static void removeUdflugt(Konference konference, Udflugt udflugt) {
		Storage.removeUdflugt(konference, udflugt);
	}

	public static void removeKonference(Konference konference) {
		Storage.removeKonference(konference);
	}

	/****************** Connect Metoder ******************/

	public static void connectHotelTilKonference(Konference konference, Hotel hotel) {
		konference.addHotel(hotel);
	}

	/****************** Get Metoder ******************/

	public static double getSamletHotelPris(Tilmelding tilmelding) {
		if (tilmelding.getHotel() != null) {
			return tilmelding.getHotel().getSamletHotelPris(tilmelding);
		} else {
			return 0.0;
		}
	}

	public static ArrayList<Konference> getKonferncer() {
		return Storage.getKonferencer();
	}

	public static double getSamletTilmeldingsPris(Tilmelding tilmelding) {
		return tilmelding.getPris();
	}

	public static ArrayList<Hotel> getHoteller(Konference konference) {
		ArrayList<Hotel> hoteller = new ArrayList<>();
		for(Hotel h : konference.getHoteller()) {
			hoteller.add(h);
		}
		return hoteller;
	}

	public static ArrayList<Ledsager> getLedsagere(Udflugt udflugt) {
		ArrayList<Ledsager> ledsagere = new ArrayList<>();
		for (Ledsager ledsager : udflugt.getLedsagere()) {
			ledsagere.add(ledsager);
		}
		return ledsagere;
	}

	public static ArrayList<Tilmelding> getTilmeldinger(Konference konference, Hotel hotel) {
		ArrayList<Tilmelding> tilmeldinger = new ArrayList<>();
		for(Tilmelding t : hotel.getTilmeldinger()) {
			if (t.getKonference() == konference) {
				tilmeldinger.add(t);
			}

		}
		return tilmeldinger;
	}

	public static ArrayList<Tilmelding> getTilmeldinger(Konference konference) {
		ArrayList<Tilmelding> tilmeldinger = new ArrayList<>();
		for(Tilmelding t : konference.getTilmeldinger()) {
			tilmeldinger.add(t);
		}
		return tilmeldinger;
	}

	public static ArrayList<Tilfoejelse> getTilfoejelser(Hotel hotel) {
		ArrayList<Tilfoejelse> tilfoejelser = new ArrayList<>();
		for (Tilfoejelse t : hotel.getTilfoejelser()) {
			tilfoejelser.add(t);
		}
		return tilfoejelser;
	}

	public static ArrayList<Udflugt> getUdflugter(Konference konference) {
		return konference.getUdflugter();
	}

	public static ArrayList<Person> getPersoner(){
		return Storage.getPersoner();
	};

	/****************** Helper Metoder ******************/

	public static void initStorage() {
		Adresse adresse = Service.createAdresse("Odense Vej", "1", "Odense", "Danmark", 5000, null);
		Konference konference = Service.createKonference("Hav og himmel", LocalDate.of(2016, 04, 18), LocalDate.of(2016, 04, 20), adresse, 1500);

		Adresse hotelAdresse1 = Service.createAdresse("Odense Vej", "2", "Odense", "Danmark", 5000, null);
		Hotel hotel1 = Service.createHotel("Den Hvide Svane", 1250, 1050, hotelAdresse1);
		Service.connectHotelTilKonference(konference, hotel1);

		Adresse hotelAdresse2 = Service.createAdresse("Odense Vej", "3", "Odense", "Danmark", 5000, null);
		Hotel hotel2 = Service.createHotel("Hotel Phoenix", 800, 700, hotelAdresse2);
		Service.connectHotelTilKonference(konference, hotel2);

		Adresse hotelAdresse3 = Service.createAdresse("Odense Vej", "4", "Odense", "Danmark", 5000, null);
		Hotel hotel3 = Service.createHotel("Pension Tusindfryd", 600, 500, hotelAdresse3);
		Service.connectHotelTilKonference(konference, hotel3);


		Tilfoejelse tilfoejelse1 = Service.createTilfoejelse("Wifi", 50, hotel1);
		Service.createTilfoejelse("Bad", 200, hotel2);
		Service.createTilfoejelse("Wifi", 75, hotel2);
		Service.createTilfoejelse("Morgenmad", 100, hotel3);

		ArrayList<Tilfoejelse> tilfoejelser1 = new ArrayList<>();
		tilfoejelser1.add(tilfoejelse1);


		Udflugt udflugt0 = Service.createUdflugt(konference, 125, LocalDate.of(2016, 04, 18), "byrundtur", "her");
		Udflugt udflugt1 = Service.createUdflugt(konference, 75, LocalDate.of(2016, 04, 19), "Egeskov", "her");
		Udflugt udflugt2 = Service.createUdflugt(konference, 200, LocalDate.of(2016, 04, 20), "Trapholt museum, Kolding", "her");

		ArrayList<Udflugt> udflugter1 = new ArrayList<>();
		udflugter1.add(udflugt1);
		udflugter1.add(udflugt2);

		ArrayList<Udflugt> udflugter2 = new ArrayList<>();
		udflugter2.add(udflugt0);
		udflugter2.add(udflugt1);

		Service.createTilmelding(null, LocalDate.of(2016, 04, 18), LocalDate.of(2016, 04, 20), false, null, null, null, null, konference, "Finn madsen", "1", adresse);
		Service.createTilmelding(hotel1, LocalDate.of(2016, 04, 18), LocalDate.of(2016, 04, 20), false, null, null, null, null, konference, "Niels Petersen", "2", adresse);
		Service.createTilmelding(hotel1, LocalDate.of(2016, 04, 18), LocalDate.of(2016, 04, 20), false, tilfoejelser1, "Mie Sommer", udflugter1, null, konference, "Peter sommer", "3", adresse);
		Service.createTilmelding(hotel1, LocalDate.of(2016, 04, 18), LocalDate.of(2016, 04, 20), true, tilfoejelser1, "Jan Madsen",  udflugter2, null, konference, "Lone Jensen", "4", adresse);
	}
}
