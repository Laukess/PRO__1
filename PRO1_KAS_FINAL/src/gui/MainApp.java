package gui;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import model.Konference;
import service.Service;

import java.util.ArrayList;


public class MainApp extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void init() {
        Service.initStorage();
    }

    @Override
    public void start(Stage stage) {
        stage.setTitle("Konference Administrationssystem");
        GridPane pane = new GridPane();
        this.initContent(pane);

        Scene scene = new Scene(pane);
        stage.setScene(scene);
        stage.show();
    }

    private Controller controller = new Controller();

    private ListView<Konference> lvwKonferencer;
    private Button btnOpret, btnSlet, btnVis;


    // -------------------------------------------------------------------------

    private void initContent(GridPane pane) {
        pane.setGridLinesVisible(false);
        pane.setPadding(new Insets(10));
        pane.setHgap(10);
        pane.setVgap(10);

        Label lblPeople = new Label("Konferencer:");
        pane.add(lblPeople, 0, 0);

        lvwKonferencer = new ListView<>();
        lvwKonferencer.getItems().setAll(controller.getKonferencer());
        final int ROW_HEIGHT = 25;
        final int DEFAULT_ROWS = 8;
        lvwKonferencer.setPrefHeight(ROW_HEIGHT * DEFAULT_ROWS);
        pane.add(lvwKonferencer, 0, 1, 3, 1);

        btnOpret = new Button("Opret");
        pane.add(btnOpret, 0, 2);

        btnSlet = new Button("Slet");
        pane.add(btnSlet, 1, 2);

        btnVis = new Button("Vis");
        pane.add(btnVis, 2, 2);

        btnVis.setOnAction(event -> controller.viewAction());
        btnSlet.setOnAction(event -> controller.deleteKonferenceAction());
        btnOpret.setOnAction(event -> controller.OpretKonferenceAction());

        controller.updateControls();


    }

    private class Controller {

        public ArrayList<Konference> getKonferencer() {
            return Service.getKonferncer();
        }

        public void OpretKonferenceAction() {
            KonferenceWindow konfWindow = new KonferenceWindow("Opret Konference");
            konfWindow.showAndWait();

            lvwKonferencer.getItems().setAll(getKonferencer());

            updateControls();
        }

        public void viewAction() {
            Konference valg = lvwKonferencer.getSelectionModel().getSelectedItem();

            ViewWindow viewWindow = new ViewWindow("Administartions tabs", valg);
            viewWindow.showAndWait();

            updateControls();
        }

        public void deleteKonferenceAction() {
            Konference konference = lvwKonferencer.getSelectionModel().getSelectedItem();
            Service.removeKonference(konference);

            lvwKonferencer.getItems().setAll(getKonferencer());
            updateControls();
        }

        public void updateControls() {
            if (Service.getKonferncer().size() > 0) {
                lvwKonferencer.getSelectionModel().select(0);
            }

        }

    }

}
