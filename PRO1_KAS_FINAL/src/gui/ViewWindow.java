package gui;

import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Konference;

public class ViewWindow extends Stage {

    Konference konference;

    public ViewWindow(String title, Konference konference) {
        this.initStyle(StageStyle.UTILITY);
        this.initModality(Modality.APPLICATION_MODAL);
        this.setResizable(false);

        this.konference = konference;

        this.setTitle(title);
        BorderPane pane = new BorderPane();
        this.initContent(pane);

        Scene scene = new Scene(pane);
        this.setScene(scene);
    }
    // -------------------------------------------------------------------------

    private void initContent(BorderPane pane) {
        TabPane tabPane = new TabPane();
        this.initTabPane(tabPane);
        pane.setCenter(tabPane);
    }

    private void initTabPane(TabPane tabPane) {
        tabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);

        Tab tabKonferencer = new Tab("Konferencer");
        tabPane.getTabs().addAll(tabKonferencer);

        KonferencePane konferencePane = new KonferencePane(this.konference);
        tabKonferencer.setContent(konferencePane);
        tabKonferencer.setOnSelectionChanged(event -> konferencePane.updateControls());


        Tab tabHoteller = new Tab("Hoteller");
        tabPane.getTabs().add(tabHoteller);

        HotelPane hotelPane = new HotelPane(this.konference);
        tabHoteller.setContent(hotelPane);
        tabHoteller.setOnSelectionChanged(event -> hotelPane.updateControls());

        Tab tabUdflugter = new Tab("Udflugter");
        tabPane.getTabs().add(tabUdflugter);

        UdflugtPane udflugtPane = new UdflugtPane(this.konference);
        tabUdflugter.setContent(udflugtPane);
        tabUdflugter.setOnSelectionChanged(event -> udflugtPane.updateControls());

    }


}
