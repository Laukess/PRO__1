package gui;

import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import model.Konference;
import model.Tilfoejelse;
import model.Tilmelding;
import model.Udflugt;
import service.Service;

import java.util.ArrayList;

public class KonferencePane extends GridPane {
	private ListView<Tilmelding> lvwTilmeldte;
	public Konference konference;

    Label lblpris;
    Label lblLedsager;
    Label lblFordragsholder;
    Label lblTilmeldteDage;
    Label lblFirma;

    Label lblPrisForedrag;
    Label lblPrisHotel;
    Label lblPrisUdflugter;

	final int ROW_HEIGHT = 25;
	final int DEFAULT_ROWS = 12;

	public KonferencePane(Konference konference) {
		this.setPadding(new Insets(20));
		this.setHgap(20);
		this.setVgap(10);
		this.setGridLinesVisible(false);
		this.konference = konference;

		Label lbltilmeldte = new Label("Tilmeldte Deltagere:");
		this.add(lbltilmeldte, 0, 0);

		lvwTilmeldte = new ListView<>();
        lvwTilmeldte.getItems().setAll(initAlleTilmeldinger(this.konference));

        ChangeListener<Tilmelding> listener =
                (ov, oldTilmelding, newTilmelding) -> this.selectedTilmeldtChanged();
        lvwTilmeldte.getSelectionModel().selectedItemProperty().addListener(listener);

		this.add(lvwTilmeldte, 0, 1, 2, 13);

		lvwTilmeldte.setPrefHeight(ROW_HEIGHT * DEFAULT_ROWS);

		Label lblnavn = new Label("Konference: " + konference.getNavn());
		Label lblFra = new Label("Fra: " + konference.getStartDato());
        Label lblTil = new Label("Til: " + konference.getSlutDato());
		Label lblsted = new Label("By: " + konference.getAdresse().getBy() + " " + konference.getAdresse().getPostNummer());
        lblLedsager = new Label();
        lblFordragsholder = new Label();
        lblTilmeldteDage = new Label();

        lblpris = new Label("Samlet Pris: ");

		this.add(lblnavn, 2, 1);

        this.add(lblFra, 2, 2);
		this.add(lblTil, 2, 3);
		this.add(lblsted, 2, 4);

		this.add(lblpris, 2, 14);

		Button btnTilmeld = new Button("Tilmeld Deltager");
		this.add(btnTilmeld, 0, 14);

        Button btnDelete = new Button("Slet Deltager");
        this.add(btnDelete, 1, 14);

		btnDelete.setOnAction(event -> deleteTilmeldingAction());
		btnTilmeld.setOnAction(event -> opretTilmeldingsAction());
	}

    private ArrayList<Tilmelding> initAlleTilmeldinger(Konference konference) {
        return new ArrayList<>(Service.getTilmeldinger(konference));
    }


	// -------------------------------------------------------------------------

	private void deleteTilmeldingAction() {
		Service.removeTilmelding(lvwTilmeldte.getSelectionModel().getSelectedItem());

		this.updateControls();
	}

	private void opretTilmeldingsAction() {
		TilmeldWindow tilmeldWindow = new TilmeldWindow("Opret Tilmelding", konference);
		tilmeldWindow.showAndWait();

		this.updateControls();
	}

	// -------------------------------------------------------------------------

    private void selectedTilmeldtChanged() {

        this.getChildren().remove(lblLedsager);
        this.getChildren().remove(lblFordragsholder);
        this.getChildren().remove(lblTilmeldteDage);
        this.getChildren().remove(lblFirma);

        this.getChildren().remove(lblPrisForedrag);
        this.getChildren().remove(lblPrisHotel);
        this.getChildren().remove(lblPrisUdflugter);


        int labelIndex = 6;

        Tilmelding selectedTilmelding = lvwTilmeldte.getSelectionModel().getSelectedItem();
        if (selectedTilmelding != null) {

            lblpris.setText("Samlet Pris: " + Service.getSamletTilmeldingsPris(selectedTilmelding));

            if (selectedTilmelding.getLedsager() != null) {
                lblLedsager.setText("Ledsager: " + selectedTilmelding.getLedsager().getNavn());
                this.add(lblLedsager, 2, labelIndex++);
            } else {
                lblLedsager.setText("");
            }

            lblFordragsholder.setText("Fordragsholder: " + (selectedTilmelding.isFordragsholder() ? "Ja" : "Nej"));
            this.add(lblFordragsholder, 2, labelIndex++);

            lblTilmeldteDage.setText("Tilmeldte Dage: " + (selectedTilmelding.getDage(selectedTilmelding.getAnkomstDato(), selectedTilmelding.getAfgangsDato()) + 1));
            this.add(lblTilmeldteDage, 2, labelIndex++);

            if (selectedTilmelding.getFirma() != null) {
                lblFirma = new Label("Firma: " + selectedTilmelding.getFirma().getNavn() + " (" + selectedTilmelding.getFirma().getTlf() + ")");
                this.add(lblFirma, 2, labelIndex++);
            }

            labelIndex++;

            try {
                double prisKonf = selectedTilmelding.isFordragsholder() ? 0 : konference.getPris() * (selectedTilmelding.getDage(selectedTilmelding.getAnkomstDato(), selectedTilmelding.getAfgangsDato()) + 1);
                lblPrisForedrag = new Label("konference: " + prisKonf + " kr.");
            } catch (Exception e) {
                System.out.println("Ja, Det er dumt der ikke er validering paa dato'erne!");
            }
            this.add(lblPrisForedrag, 2, labelIndex++);


            lblPrisHotel = new Label("Hotel: " + Service.getSamletHotelPris(selectedTilmelding)  + " kr.");
            this.add(lblPrisHotel, 2, labelIndex++);

            double prisUdflugter = 0.0;

            if (selectedTilmelding.getLedsager() != null) {
                for (Udflugt u : selectedTilmelding.getLedsager().getUdflugter()) {
                    prisUdflugter += u.getPris();
                }
            }

            lblPrisUdflugter = new Label("Udflugter: " + prisUdflugter + " kr.");
            this.add(lblPrisUdflugter, 2, labelIndex++);

        }




    }

	public void updateControls() {
		lvwTilmeldte.getItems().setAll(initAlleTilmeldinger(this.konference));
	}

}
