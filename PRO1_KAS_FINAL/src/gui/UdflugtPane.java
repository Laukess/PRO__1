package gui;

import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import model.Hotel;
import model.Konference;
import model.Ledsager;
import model.Udflugt;
import service.Service;

import java.util.ArrayList;
import java.util.Optional;

public class UdflugtPane extends GridPane {

	final int ROW_HEIGHT = 25;
	final int DEFAULT_ROWS = 12;
	Konference konference;

	private ListView<Udflugt> lvwUdflugter;
	private ListView<Ledsager> lvwDeltager;

	public UdflugtPane(Konference konference) {
		this.setPadding(new Insets(20));
		this.setHgap(20);
		this.setVgap(10);
		this.setGridLinesVisible(false);

		this.konference = konference;

		Label lblUdflugter = new Label("Udflugter:");
		Label lblDeltager = new Label("Deltagere:");
		this.add(lblUdflugter, 0, 0);
		this.add(lblDeltager, 2, 0);

		lvwUdflugter = new ListView<>();
		lvwUdflugter.getItems().setAll(this.konference.getUdflugter());
		ChangeListener<Udflugt> listener = (ov, oldUdflugt, newUdflugt) -> this.selectedUdflugtChanged();
		lvwUdflugter.getSelectionModel().selectedItemProperty().addListener(listener);

		lvwDeltager = new ListView<>();

		this.add(lvwUdflugter, 0, 1, 2, 1);
		lvwUdflugter.setPrefHeight(ROW_HEIGHT * DEFAULT_ROWS);
		this.add(lvwDeltager, 2, 1);
		lvwDeltager.setPrefHeight(ROW_HEIGHT * DEFAULT_ROWS);

		Button btnOpret = new Button("Opret Udflugt");
		Button btnSlet = new Button("Slet Udflugt");

		this.add(btnOpret, 0, 2);
		this.add(btnSlet, 1, 2);

		btnOpret.setOnAction(event -> this.opretUdflugtAction());
		btnSlet.setOnAction(event -> this.sletUdflugtAction());

	}

	public void opretUdflugtAction() {
		UdflugtWindow opretUdflugt = new UdflugtWindow("Opret Udflugt", this.konference);
		opretUdflugt.showAndWait();

		lvwUdflugter.getItems().setAll(konference.getUdflugter());
	}

	public void sletUdflugtAction() {
		Udflugt udflugt = lvwUdflugter.getSelectionModel().getSelectedItem();
		if (udflugt == null) {
			return;
		}

		Stage owner = (Stage) this.getScene().getWindow();
		Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
		alert.setTitle("Slet Udflugt");
		alert.initOwner(owner);
		alert.setHeaderText("Er du sikker?");

		Optional<ButtonType> result = alert.showAndWait();
		if (result.isPresent() && result.get() == ButtonType.OK) {

			int selectIndex = lvwUdflugter.getSelectionModel().getSelectedIndex();
			Service.removeUdflugt(konference, udflugt);
			lvwUdflugter.getItems().setAll(this.initAlleUdflugter(this.konference));
			if (selectIndex > 0) {
				lvwUdflugter.getSelectionModel().select(selectIndex-1);
			} else {
				lvwUdflugter.getSelectionModel().select(0);
			}

			this.updateControls();
		}
	}

	// -------------------------------------------------------------------------
	private ArrayList<Ledsager> initAlleLedsagere(Udflugt udflugt) {
		return new ArrayList<Ledsager>(udflugt.getLedsagere());
	}

	private ArrayList<Udflugt> initAlleUdflugter(Konference konference) {
		return new ArrayList<Udflugt>(konference.getUdflugter());
	}

	// -------------------------------------------------------------------------

	private void selectedUdflugtChanged() {
		this.updateControls();
	}

	public void updateControls() {
		Udflugt udflugt = lvwUdflugter.getSelectionModel().getSelectedItem();

		if (udflugt != null) {
			ArrayList<Ledsager> ledsagere = initAlleLedsagere(udflugt);
			lvwDeltager.getItems().setAll(ledsagere);
		}

	}

}
