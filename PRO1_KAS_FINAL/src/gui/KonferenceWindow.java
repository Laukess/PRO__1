package gui;

import java.time.LocalDate;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Adresse;
import model.Konference;
import service.Service;

public class KonferenceWindow extends Stage {

	public KonferenceWindow(String title) {
		this.initStyle(StageStyle.DECORATED);
		this.initModality(Modality.APPLICATION_MODAL);
		this.setResizable(false);

		this.setTitle(title);
		GridPane pane = new GridPane();
		this.initContent(pane);

		Scene scene = new Scene(pane);
		this.setScene(scene);
	}

	// -------------------------------------------------------------------------

	private TextField txfName, txfStartDato, txfSlutDato, txfVej, txfBy,
			txfLand, txfPostNr, txfVejNr, txfEtage, txfpris;
	private Label lblError;

	private void initContent(GridPane pane) {
		pane.setPadding(new Insets(10));
		pane.setHgap(10);
		pane.setVgap(10);
		pane.setGridLinesVisible(false);

		Label lblName = new Label("Navn");
		pane.add(lblName, 0, 0);
		txfName = new TextField();
		pane.add(txfName, 1, 0);

		Label lblStartDato = new Label("Startdato");
		pane.add(lblStartDato, 0, 1);
		txfStartDato = new TextField();
		pane.add(txfStartDato, 1, 1);

		Label lblSlutDato = new Label("Slutdato");
		pane.add(lblSlutDato, 0, 2);
		txfSlutDato = new TextField();
		pane.add(txfSlutDato, 1, 2);

		Label lblpris = new Label("Pris");
		pane.add(lblpris, 0, 3);
		txfpris = new TextField();
		pane.add(txfpris, 1, 3);

		Label lblAdresse = new Label("Adresse:");
		pane.add(lblAdresse, 1, 4);

		Label lblVej = new Label("Vej");
		pane.add(lblVej, 0, 5);
		txfVej = new TextField();
		pane.add(txfVej, 1, 5);

		Label lblVejNr = new Label("Vejnummer");
		pane.add(lblVejNr, 0, 6);
		txfVejNr = new TextField();
		pane.add(txfVejNr, 1, 6);

		Label lblEtage = new Label("Etage");
		pane.add(lblEtage, 0, 7);
		txfEtage = new TextField();
		pane.add(txfEtage, 1, 7);

		Label lblBy = new Label("By");
		pane.add(lblBy, 0, 8);
		txfBy = new TextField();
		pane.add(txfBy, 1, 8);

		Label lblPostNr = new Label("Postnummer");
		pane.add(lblPostNr, 0, 9);
		txfPostNr = new TextField();
		pane.add(txfPostNr, 1, 9);

		Label lblLand = new Label("Land");
		pane.add(lblLand, 0, 10);
		txfLand = new TextField();
		pane.add(txfLand, 1, 10);

		Button btnCancel = new Button("Cancel");
		pane.add(btnCancel, 0, 11);
		GridPane.setHalignment(btnCancel, HPos.LEFT);
		btnCancel.setOnAction(event -> this.cancelAction());

		Button btnOK = new Button("OK");
		btnOK.setDefaultButton(true);
		pane.add(btnOK, 1, 11);
		GridPane.setHalignment(btnOK, HPos.RIGHT);
		btnOK.setOnAction(event -> this.okAction());

		lblError = new Label();
		pane.add(lblError, 0, 12, 2, 1);
		lblError.setStyle("-fx-text-fill: red");

	}

	// -------------------------------------------------------------------------

	private void cancelAction() {
		this.hide();
	}

	private void okAction() {

		String name = txfName.getText().trim();
		if (name.length() == 0) {
			lblError.setText("Navn er tom");
			return;
		}

		String startDato = txfStartDato.getText().trim();
		if (startDato.length() == 0) {
			lblError.setText("Manglende startdato");
			return;
		}
		LocalDate date1 = null;
		try {
			date1 = LocalDate.parse(txfStartDato.getText().trim());
		} catch (Exception e) {
			lblError.setText("Forkert startdato input");
			return;
		}

		String slutDato = txfSlutDato.getText().trim();
		if (slutDato.length() == 0) {
			lblError.setText("Manglende slutdato");
			return;
		}

		LocalDate date2 = null;
		try {
			date2 = LocalDate.parse(txfSlutDato.getText().trim());
		} catch (Exception e) {
			lblError.setText("Forkert slutdato input");
			return;
		}

		String vej = txfVej.getText().trim();
		if (vej.length() == 0) {
			lblError.setText("Vej er tom");
			return;
		}

		String by = txfBy.getText().trim();
		if (by.length() == 0) {
			lblError.setText("By er tom");
			return;
		}

		String land = txfLand.getText().trim();
		if (land.length() == 0) {
			lblError.setText("Land er tom");
			return;
		}

		int postNr = -1;
		try {
			postNr = Integer.parseInt(txfPostNr.getText().trim());
			if (postNr <= 0) {
				lblError.setText("Postnummer er tom");
				return;
			}
		} catch (Exception e) {
			lblError.setText("Postnummer skal være et tal");
		}

		String vejNr = txfVejNr.getText().trim();
		if (vejNr.length() == 0) {
			lblError.setText("Vejnummer er tom");
			return;
		}

		double pris;
		if (txfpris.getText().length() > 0) {
			try {
				pris = Double.parseDouble(txfpris.getText().trim());
			} catch (Exception e) {
				lblError.setText("Pris skal være et tal");
				return;
			}
		} else {
			lblError.setText("Pris er tom");
			return;
		}

		String etage = txfEtage.getText().trim();

		if (etage.equals("")) {
			etage = null;
		}

		Adresse adresse = Service.createAdresse(vej, vejNr, by, land, postNr, etage);
		Service.createKonference(name, date1, date2, adresse, pris);

		this.hide();

	}

}
