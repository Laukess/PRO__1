/**
 * Created by lukas on 14-02-2016.
 */
public class Opgave_2 {
    /*
    * R2.1
    * refrencen referere til et object. Der kan vaere flere refrences der peger til samme object. De er mere lightweight
    * og er nemmere at passe til funktioner.
    *
    * R2.2
    * Object variable is a synonym of "instance variable". Som et objects variable.
    * http://stackoverflow.com/questions/17433631/object-vs-object-variable-in-java
    *
    * R2.3
    * En class er en slags blueprint, et object er en instance af den class.
    *
    * R2.4
    * String
    * PrintStream
    *
    * R2.5
    * Det public interface, er de methods der er public og som man kan kalde, udefra.
    * Implementationen er alle funktioner i klassen, ogsaa de private funktioner, der kun bliver brugt af klassen selv.
    *
    * funktion header **
    * imp -> kroppen
    *
    *
    * R2.7
    * 0
    *
    * R2.8
    * i java asigner man en vaerdi med =, i math er det en equality ting.
    *
    * R2.14
    *
    * a) Rectangle r = new Rectangle(5, 10, 15, 20);
    * b) double width = new Rectangle(5, 10, 15, 20).getWidth();
    * c) Rectangle r = new Rectangle(); r.translage(15, 25);
    * d) Rectangle r = new Rectangle(); r.translate(15, 25);
    *
    * R2.22
    * The data is only available to the object itself, if you want to allow access to it, you need to create a getter
    * method.
    *
    *
    *
    * */

}
