/**
 * Created by lukas on 14-02-2016.
 */
public class Car {

    private double efficiancy; // KM pr L
    private double fuel;

    public Car(double efficiancy) {
        this.efficiancy = efficiancy;
        this.fuel = 0;
    }

    public void drive(double distance) {
        this.fuel -= distance / efficiancy;
    }

    public double getGasInTank() {
        return fuel;
    }

    public void addGas(double amount) {
        this.fuel += amount;
    }


}
