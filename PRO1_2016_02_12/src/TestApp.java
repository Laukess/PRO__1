public class TestApp {

    public static void main(String[] args) {
        Employee emp = new Employee("Kristian");
        System.out.println(emp.getName());
        
        emp.setName("Christian");
        System.out.println(emp.getName());

        emp.setAge(32);
        System.out.println(emp.getAge());

        Employee emp2 = new Employee("John", 44);
        System.out.println("name: " + emp2.getName() + "\nage: " + emp2.getAge());
        emp2.increaseAgeByOne();
        System.out.println("name: " + emp2.getName() + "\nage: " + emp2.getAge());



        Employee emp3 = new Employee("John", "Doe", 12);
        System.out.println("********************************************");
        System.out.println("Fornavn: " + emp3.getFName());
        System.out.println("Efternavn: " + emp3.getEName());
        System.out.println("Trainee: " + emp3.isTrainee());
        System.out.println("alder: " + emp3.getAge());
        System.out.println("********************************************");
    }

}
