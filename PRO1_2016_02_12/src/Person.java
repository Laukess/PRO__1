/**
 * Created by lukas on 14-02-2016.
 */
public class Person {
    private String name;
    private String address;
    private int monthlySalary;
    private int numberOfEmployments;

    public Person(){}
    public void setName(String name){
        this.name = name;
    }
    public String getName(){
        return this.name;
    }
    public void setAddress(String address){
        this.address = address;
    }
    public String getAddress() {
        return this.address;
    }
    public void setMonthlySalary(int salary) {
        this.monthlySalary = salary;
    }
    public int getMonthlySalary() {
        return this.monthlySalary;
    }
    public void printPerson() {
        System.out.println("**********************************************");
        System.out.println("name: " + getName());
        System.out.println("address: " + getAddress());
        System.out.println("Salary: " + getMonthlySalary());
        System.out.println("**********************************************");
    }

    public double getYearlySalary() {
        return (getMonthlySalary() * 12.0) * 1.025;
    }

    public int getNumberOfEmployments() {
        return numberOfEmployments;
    }

    public void setNumberOfEmployments(int increment) {
        this.numberOfEmployments = getNumberOfEmployments() + increment;
    }
}
