/**
 * Created by lukas on 14-02-2016.
 */
public class CarTester {

    public static void main(String[] args) {
        Car car = new Car(13);
        System.out.println("Available gas: " + car.getGasInTank());
        car.addGas(30);
        System.out.println("Available gas after refill: " + car.getGasInTank());
        car.drive(39);
        System.out.println("Gas availbale after 39KM: " + car.getGasInTank());
    }

}
