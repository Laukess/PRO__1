
/**
 * Klasse der beskriver en ansat.
 */
public class Employee {
    /**
     * Attributter der beskriver den ansattes tilstand
     */
    private String name;
    private String fNavn;
    private String eNavn;
    private boolean trainee;
    private int age;

    /**
     * Constructor, når den ansatte oprettes, skal den have et navn. Ved
     * oprettelse er den ansatte en trainee
     */
    public Employee(String inputName, int age) {
        this.name = inputName;
        this.age = age;
        this.trainee = true;
    }

    public Employee(String inputName) {
        this.name = inputName;
        this.trainee = true;
    }

    public Employee(String fNavn, String eNavn, int age) {
        this.fNavn = fNavn;
        this.eNavn = eNavn;
        this.age = age;
    }

    /**
     * Den ansattes navn kan ændres ved kald af setName metoden
     */
    public void setName(String inputName) {
        this.name = inputName;
    }

    /**
     * Man kan få oplyst den ansattes navn, ved at kalde metoden getName
     */
    public String getName() {
        return this.name;
    }

    /**
     * Man kan få oplyst den ansattes alder, ved at kalde metoden getAge
     */
    public int getAge() {
        return age;
    }

    /**
     * Den ansattes alder kan ændres ved kald af setAge metoden
     */
    public void setAge(int age) {
        this.age = age;
    }

    public void increaseAgeByOne() {
        this.age++;
    }

    public String getFName() {
        return fNavn;
    }

    public String getEName() {
        return eNavn;
    }

    public void setFName(String fName) {
        this.fNavn = fName;
    }

    public void setEName(String eName) {
        this.eNavn = eName;
    }



















    /**
     * Den ansatte kan få ændret trainee status ved at kalde metoden setTrainess
     */
    public void setTrainee(boolean trainee) {
        this.trainee = trainee;
    }

    /**
     * Man kan få oplyst den ansatte er trainess aktivitet, ved at kalde metoden
     * isTrainee
     */
    public boolean isTrainee() {
        return this.trainee;
    }

    @Override
    public String toString() {
        return this.name + " is trainee: " + this.trainee;
    }

    public void printEmployee() {
        System.out.println("*******************");
        System.out.println("Name " + this.name);
        System.out.println("Trainee " + this.trainee);
        System.out.println();
    }
}
