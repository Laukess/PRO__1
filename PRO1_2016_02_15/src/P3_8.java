import java.util.Scanner;

/**
 * Created by lukas on 15-02-2016.
 */
public class P3_8 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("indtast den foerste laengde \n>>> ");
        String len1 = in.next();
        System.out.print("indtast den anden laengde  \n>>> ");
        String len2 = in.next();
        System.out.println("firkanten paa " + len1 + "cm og " + len2 + "cm, har et areal paa " + (len1 + len2));
        System.out.println("og har en omkreds paa " + (len1 + len1 + len2 + len2) );
        //System.out.println("Diagonalen er: " + (Double.parseDouble(len1) * Math.sqrt(2)));
        System.out.println("Diagonalen er: " + (Math.sqrt( Math.pow(Double.parseDouble(len1), 2) + Math.pow(Double.parseDouble(len2), 2) )));
        in.close();
    }
}
