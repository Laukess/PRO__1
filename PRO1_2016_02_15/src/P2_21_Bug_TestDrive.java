/**
 * Created by lukas on 15-02-2016.
 */
public class P2_21_Bug_TestDrive {
    public static void main(String[] args) {
        P2_21_Bug bug = new P2_21_Bug(10);
        System.out.println("pos: " + bug.getPosition());
        bug.move();
        System.out.println("pos: " + bug.getPosition());
        bug.turn();
        System.out.println("pos: " + bug.getPosition());
        bug.move();
        System.out.println("pos: " + bug.getPosition());
    }
}
