/**
 * Created by lukas on 15-02-2016.
 */
public class Opgave_3_Name {
    private String firstName;
    private String middleName;
    private String lastName;

    public Opgave_3_Name(String firstName, String middleName, String lastName) {
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
    }

    public String getInitials() {
        return firstName.substring(0, 1) + middleName.substring(0, 1) + lastName.substring(0, 1);
    }

    public String getUsername() {
        String username = this.firstName.substring(0, 2).toUpperCase();
        username += this.middleName.length();
        username += this.lastName.substring(lastName.length()-2).toLowerCase();

        return username;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
}
