/**
 * Created by lukas on 15-02-2016.
 */
public class Opgave_1_og_2 {
    public static void main(String[] args) {
        /*
        * Opgave 1:
        *
        * i1 = b / a -> i1 = 11 / 4 -> int i1 = 2
        * c = (int) (s / r) -> c = (int) (11.0 / 4.0) -> int c = 2
        * d1 = b / a -> d1 = 11 / 4 -> double d1 = 2.0
        * d2 = s / r -> d2 = 11 / 4.0 -> double d2 = 2.75
        * d1 = b / r -> d1 =  11 / 4.0 -> double d1 = 2.75
        * d2 = 11.0 / 4.0 -> double d2 = 2.75
        * d1 = 11.0 / a * c -> d1 = 11.0 / 4 * 2 -> double d1 = 5.5
        * i1++ = 3
        * i1 = b % a -> i1 = 11 % 4 -> int i1 = 3
        * i1 = 3 + 7 * 2 -> int i1 = 17
        * i2 = (3 + 7) * 2 -> int i2 = 20
        *
        * */

        // Opgave 2
        String ord1 = "Datamatiker";
        String ord2 = "Uddannelsen";
        String ord3 = ord1 + " " + ord2.toLowerCase();

        System.out.println(ord1.toUpperCase());
        System.out.println(ord2.toLowerCase());
        System.out.println(ord1 + " " + ord2);
        System.out.println("length: " + ord3.length());
        System.out.println(ord1.substring(0, 7));
        System.out.println(ord2.substring(2, 7));

        String[] ordx = ord3.split(" ");

        System.out.println(ordx[1]);
    }
}
