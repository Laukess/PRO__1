/**
 * Created by lukas on 15-02-2016.
 */
public class P2_21_Bug {

    private int position;
    private int direction = 1;
//  private boolean isMovingLeft = true;

    public P2_21_Bug(int initialPosition) {
        this.position = initialPosition;
    }

    public void turn() {
        System.out.println("Turning.");
        direction = -direction;
//        this.isMovingLeft = !isMovingLeft;
    }

    public void move() {
        System.out.println("Moving.");
        position += direction;
//        if (isMovingLeft) {
//            this.position -= 1;
//        } else {
//            this.position += 1;
//      }
    }

    public int getPosition() {
        return this.position;
    }
}
