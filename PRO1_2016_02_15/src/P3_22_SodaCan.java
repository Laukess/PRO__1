/**
 * Created by lukas on 15-02-2016.
 */
public class P3_22_SodaCan {

    double height;
    double width;

    public P3_22_SodaCan(double height, double width) {
        this.height = height;
        this.width = width;
    }

    public double getVolume() {
        return Math.PI * Math.pow(width/2, 2) * height;
    }

    public double getSurfaceArea() {
        double L = 2 * Math.PI * (width/2) * height;
        double T = Math.PI * (Math.pow(width/2, 2));
        //double surfaceOfSide = 2 * Math.PI * (width/2) * width;
        //double surfaceOfTopAndBottom = 2 * Math.PI * Math.pow(width/2, 2);
        //return surfaceOfSide + surfaceOfTopAndBottom;
        return L + T + T;
    }

}
