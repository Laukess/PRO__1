/**
 * Created by lukas on 15-02-2016.
 */
public class P3_22_SodaCan_TexstDrive {
    public static void main(String[] args) {
        P3_22_SodaCan can = new P3_22_SodaCan(23, 10);
        System.out.println("Volume: " + can.getVolume());
        System.out.println("Surface: " + can.getSurfaceArea());
    }
}
