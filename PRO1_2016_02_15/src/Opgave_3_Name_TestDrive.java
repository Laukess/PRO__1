/**
 * Created by lukas on 15-02-2016.
 */
public class Opgave_3_Name_TestDrive {
    public static void main(String[] args) {
        Opgave_3_Name person = new Opgave_3_Name("Margrethe", "Mosbak", "Dybdahl");
        System.out.println("initials: " + person.getInitials());
        System.out.println("Username: " + person.getUsername());
    }
}
