package Opgave_3;

/**
 * Created by lukas on 11-04-2016.
 */
public class Car {

    private String license;
    private double pricePerDay;
    private int purchaseYear;

    public Car(String license, int year) {
        this.license = license;
        this.purchaseYear = year;
    }

    public void setDayPrice(double price) {
        this.pricePerDay = price;
    }

    public double getDayPrice() {
        return pricePerDay;
    }

    public String getLicense() {
        return this.license;
    }

    public int getPurchaseYear() {
        return this.purchaseYear;
    }
}
