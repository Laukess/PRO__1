package Opgave_3;

import java.util.ArrayList;

/**
 * Created by lukas on 11-04-2016.
 */
public class Rental {

    private int number;
    private int days;
    private String date;
    private ArrayList<Car> cars;

    public Rental(int number, String date, int days) {
        this.number = number;
        this.date = date;
        this.days = days;
        this.cars = new ArrayList<>();
    }

    public double getPrice() {
        double pricePerDayForAllCars = 0;
        for (Car car : cars) {
            pricePerDayForAllCars += car.getDayPrice();
        }
        if (this.days > 0 && cars != null && !cars.isEmpty()) {
            return pricePerDayForAllCars * this.days;
        } else {
            return 0;
        }
    }

    public void setDays(int days) {
        this.days = days;
    }

    public int getDays() {
        return this.days;
    }

    public ArrayList<Car> getCars() {
        return cars;
    }

    public void addCar(Car car) {
        this.cars.add(car);
    }

    public void removeCar(Car car) {
        this.cars.remove(car);
    }

}
