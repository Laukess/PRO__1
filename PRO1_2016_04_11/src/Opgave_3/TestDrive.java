package Opgave_3;

/**
 * Created by lukas on 11-04-2016.
 */
public class TestDrive {

    public static void main(String[] args) {
        Car car1 = new Car("1", 1989);
        car1.setDayPrice(1100);
        Car car2 = new Car("2", 1988);
        car2.setDayPrice(1200);
        Car car3 = new Car("3", 1999);
        car3.setDayPrice(1300);
        Car car4 = new Car("4", 2000);
        car4.setDayPrice(1400);
        Car car5 = new Car("5", 2007);
        car5.setDayPrice(1500);

        Rental r1 = new Rental(12, "2016_04_11", 14);
        r1.addCar(car1);
        r1.addCar(car2);
        Rental r2 = new Rental(3, "2016_04_11", 7);
        r2.addCar(car3);
        r2.addCar(car4);
        r2.addCar(car5);

    }

}
