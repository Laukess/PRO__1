package Opgave_4;

/**
 * Created by lukas on 11-04-2016.
 */
public class Service {

    // metoden skal sikre, at svømmeren følger planen
    public static void connectSwimmerToPlan(TrainingPlan plan, Swimmer swimmer) {
        swimmer.setPlan(plan);
        plan.addSwimmer(swimmer);
    }

    // metoden skal sikre, at svømmeren ikke længere følger en plan
    public static void clearPlanOfSwimmer(Swimmer swimmer) {
        swimmer.setPlan(null);
    }


}
