package Opgave_4;

import java.util.ArrayList;

/**
 * Created by lukas on 11-04-2016.
 */
public class SwimmerApp {
    public static void main(String[] args) {

        Service service = new Service();

        TrainingPlan planA = new TrainingPlan('A', 10, 5);
        TrainingPlan planB = new TrainingPlan('B', 5, 3);

        ArrayList<Double> janTimes = new ArrayList<>();
        janTimes.add(5.0);
        janTimes.add(15.0);
        janTimes.add(50.0);

        ArrayList<Double> boTimes = new ArrayList<>();
        boTimes.add(5.0);
        boTimes.add(15.0);
        boTimes.add(50.0);

        ArrayList<Double> MikkelTimes = new ArrayList<>();
        MikkelTimes.add(5.0);
        MikkelTimes.add(15.0);
        MikkelTimes.add(50.0);

        Swimmer jan = new Swimmer("Jan", 89, janTimes, "CCC");
        service.connectSwimmerToPlan(planA, jan);
        Swimmer bo = new Swimmer("Bo", 89, boTimes, "CCC");
        service.connectSwimmerToPlan(planA, bo);
        Swimmer mikkel = new Swimmer("Mikkel", 89, MikkelTimes, "CCC");
        service.connectSwimmerToPlan(planB, mikkel);

        System.out.println("mikkels plan: " + mikkel.getPlan().getLevel());
        System.out.println("alle paa plan A: ");
        for (Swimmer swimmer : planA.getSwimmers()) {
            System.out.println(swimmer.getName());
        }

        System.out.println("<--------------------------------------->");

        service.connectSwimmerToPlan(planA, mikkel);
        for (Swimmer swimmer : planA.getSwimmers()) {
            System.out.println(swimmer.getName());
        }

    }
}
