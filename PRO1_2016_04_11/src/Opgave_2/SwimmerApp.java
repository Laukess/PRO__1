package Opgave_2;

import java.util.ArrayList;

/**
 * Created by lukas on 11-04-2016.
 */
public class SwimmerApp {
    public static void main(String[] args) {
        ArrayList<Swimmer> Swimmers = new ArrayList<>();

        TrainingPlan planA = new TrainingPlan('A', 10, 5);
        TrainingPlan planB = new TrainingPlan('B', 5, 3);

        ArrayList<Double> janTimes = new ArrayList<>();
        janTimes.add(5.0);
        janTimes.add(15.0);
        janTimes.add(50.0);

        ArrayList<Double> boTimes = new ArrayList<>();
        boTimes.add(5.0);
        boTimes.add(15.0);
        boTimes.add(50.0);

        planA.addSwimmer(new Swimmer("Jan", 89, janTimes, "CCC"));
        planB.addSwimmer(new Swimmer("Bo", 89, boTimes, "CCC"));
        planB.addSwimmer(new Swimmer("Aske", 89, boTimes, "CCC"));
        planB.addSwimmer(new Swimmer("Ash", 89, boTimes, "CCC"));
        planB.addSwimmer(new Swimmer("BurnedStuff", 89, boTimes, "CCC"));

        System.out.println("Plan A: ");
        for (Swimmer swimmer : planA.getSwimmers()) {
            System.out.println("--- " + swimmer.getName() + " ---");
            System.out.println("club: " + swimmer.getClub());
            System.out.println("year: " + swimmer.getYearGroup());
            System.out.println("Best Time: " +swimmer.bestLapTime());
            System.out.println("--------\n");
        }

        System.out.println("Plan B: ");
        for (Swimmer swimmer : planB.getSwimmers()) {
            System.out.println("--- " + swimmer.getName() + " ---");
            System.out.println("club: " + swimmer.getClub());
            System.out.println("year: " + swimmer.getYearGroup());
            System.out.println("Best Time: " +swimmer.bestLapTime());
            System.out.println("--------\n");
        }
    }
}
