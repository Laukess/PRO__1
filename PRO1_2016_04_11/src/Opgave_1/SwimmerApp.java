package Opgave_1;

import java.util.ArrayList;

/**
 * Created by lukas on 11-04-2016.
 */
public class SwimmerApp {
    public static void main(String[] args) {
        ArrayList<Swimmer> Swimmers = new ArrayList<>();

        TrainingPlan planA = new TrainingPlan('A', 10, 5);
        TrainingPlan planB = new TrainingPlan('B', 5, 3);

        ArrayList<Double> janTimes = new ArrayList<>();
        janTimes.add(5.0);
        janTimes.add(15.0);
        janTimes.add(50.0);

        ArrayList<Double> boTimes = new ArrayList<>();
        boTimes.add(5.0);
        boTimes.add(15.0);
        boTimes.add(50.0);

        ArrayList<Double> MikkelTimes = new ArrayList<>();
        MikkelTimes.add(5.0);
        MikkelTimes.add(15.0);
        MikkelTimes.add(50.0);

        Swimmer jan = new Swimmer("Jan", 89, janTimes, "CCC");
        jan.setPlan(planA);
        Swimmers.add(jan);
        Swimmer bo = new Swimmer("Bo", 89, boTimes, "CCC");
        bo.setPlan(planA);
        Swimmers.add(bo);
        Swimmer mikkel = new Swimmer("Mikkel", 89, MikkelTimes, "CCC");
        mikkel.setPlan(planB);
        Swimmers.add(mikkel);

        System.out.println("timer i alt for Jan: " + jan.allTrainingHours());
        System.out.println("timer i alt for Bo: " + bo.allTrainingHours());
        System.out.println("timer i alt for Mikkel: " + mikkel.allTrainingHours());
    }
}
