package Opgave_1;

import java.util.*;

/**
 * Modeling a Swimmer
 */
public class Swimmer {
    private String name;
    private String club;
    private int yearGroup;
    private ArrayList<Double> lapTimes;
    private TrainingPlan plan;
    
    /**
     * Initialize a new swimmer with name, club, yearGroup, and lap times.
     */ 

   public Swimmer(String name, int yearGroup, ArrayList<Double> lapTimes, String club) {
       this.name = name;
       this.yearGroup = yearGroup;
       this.lapTimes = lapTimes;
       this.club = club;
   }
    /**
     * Return the name of the swimmer
     */
    public String getName() {
        return this.name;
    }

    /**
     * Return the yearGroup of the swimmer
     */
    public int getYearGroup() {
        return this.yearGroup;
    }

    /**
     * Return the club of the swimmer
     */
    public String getClub() {
        return this.club;
    }

    /**
     * Register the club of the swimmer
     * @param club
     */
    public void setClub(String club) {
        this.club = club;
    }
    
    /**
     * Return the <i>fastest</i> lap time
     */
    public double bestLapTime() {

        double min = Double.MAX_VALUE;
        for (double lapTime : lapTimes) {
            if (lapTime < min) {
                min = lapTime;
            }
        }

        return Collections.min(this.lapTimes);
    }

    public void setPlan(TrainingPlan plan) {
        this.plan = plan;
    }

    public TrainingPlan getPlan() {
        return this.plan;
    }

    /**
     * Return how many training hours the swimmer has each week.
     */
    public int allTrainingHours() {
        return this.plan.getWeeklyWaterHours() + this.plan.getWeeklyStrengthHours();
    }

}
