package Opgave_1;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Laukess on 12-05-2016.
 */
public class Opgave_1 {

    public static void main(String[] args) {
        String[] s = {"Erna", "Elly", "Laurits", "Bertha", "Christian", "August", "Marius", "John", "Tove", "Poul", "Torkild"};

        Customer[] c = {new Customer("parker", "peter", 12), new Customer("Doe", "John", 22), new Customer("Carmack", "John", 44)};
        ArrayList<Customer> c2 = new ArrayList<>();
        c2.add(new Customer("parker", "peter", 12));
        c2.add(new Customer("Doe", "John", 22));
        c2.add(new Customer("Carmack", "John", 44));


        System.out.println(c);
//        System.out.println(Arrays.toString(s));
//        bubbleSort(s);
//        selectionSort(s);
//        selectionSort(c);
//        insertionSort(s);
        insertionSort(c2);
    }
//  Opgave 1
    public static void bubbleSort(String[] s) {

        boolean flipped = true;
        int counter = 0;

        String cur;
        String next;

//      outer loop
        while (flipped && counter < s.length) {
            //      inner loop
            flipped = false;
            for (int i = 0 ; i < s.length - 1 ; i++) {
                if (s[i].compareTo(s[i+1]) > 0) {
                    cur = s[i];

                    s[i] = s[i+1];
                    s[i+1] = cur;
                    flipped = true;
                }
            }
            counter++;
            System.out.println(Arrays.toString(s));
        }
    }

//  Opgave 2.1
    public static void selectionSort(String[] s) {

        int index = 0;
        int lowestIndex = 0;
        String temp = "";

        for (int k = 0 ; k < s.length ; k++) {
            System.out.print("");
            for (int i = index ; i < s.length ; i++) {
                if (s[i].compareTo(s[lowestIndex]) < 0) {
                    lowestIndex = i;
                }
            }
            temp = s[index];
            s[index++] = s[lowestIndex];
            s[lowestIndex] = temp;
            System.out.println(Arrays.toString(s));
        }
    }

//  Opgave 2.2
    public static void selectionSort(Customer[] c) {

        int index = 0;
        int lowestIndex = 0;
        Customer temp = null;

        for (int k = 0 ; k < c.length ; k++) {
            for (int i = index ; i < c.length ; i++) {
                if (c[i].compareTo(c[lowestIndex]) < 0) {
                    lowestIndex = i;
                }
            }
            temp = c[index];
            c[index++] = c[lowestIndex];
            c[lowestIndex] = temp;
            System.out.println(Arrays.toString(c));
        }
    }

//  Opgave 3;
    public static void insertionSort(String[] s) {

        String key;

        // Løb usorteret del af liste igennem
        for (int i = 1 ; i < s.length ; i++) {
            key = s[i];
            // Flyt sorterede elementer en op
            int j = i;
            while (j > 0 && s[j-1].compareTo(key) > 0) {
                s[j] = s[j-1];
                j--;
                s[j] = key;
            }
            System.out.println(Arrays.toString(s));

        }

    }

    public static void insertionSort(ArrayList<Customer> a) {

        Customer key;

        // Løb usorteret del af liste igennem
        for (int i = 1 ; i < a.size() ; i++) {
            key = a.get(i);
            // Flyt sorterede elementer en op
            int j = i;
            while (j > 0 && a.get(j-1).compareTo(key) > 0) {
                a.set(j, a.get(j-1));
                j--;
                a.set(j, key);
            }
            System.out.println(a);

        }

    }


}
