package Opgave_1;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * Created by Laukess on 13-05-2016.
 */
public class Opgave_4 {

    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<String>();
        // TODO: Add nogle elementer til list
        list.add("Erna");
        list.add("Elly");
        list.add("Laurits");
        list.add("Bertha");
        list.add("Christian");
        list.add("August");

        ArrayList<Customer> c = new ArrayList<>();
        c.add(new Customer("parker", "peter", 12));
        c.add(new Customer("Doe", "John", 22));
        c.add(new Customer("Carmack", "John", 44));


//        System.out.println(list);
//        Collections.sort(list);
//        System.out.println(list);

        System.out.println(c);
        Collections.sort(c);
        System.out.println(c);
    }



}
