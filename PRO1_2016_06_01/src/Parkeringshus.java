import java.lang.reflect.Array;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Created by Laukess on 01-06-2016.
 */
public class Parkeringshus {

    private String adresse;
    private ArrayList<Parkeringsplads> parkeringspladser = new ArrayList<>();
    private double saldo;

    public Parkeringshus(String adresse) {
        this.adresse = adresse;
    }

    public Parkeringsplads createParkeringsplads(int nummer) {
        return new Parkeringsplads(nummer, this);
    }

    public Invalideplads createInvalideParkeringsplads(int nummer, double areal) {
        return new Invalideplads(nummer, this, areal);
    }

    public boolean removeParkeringsplads(Parkeringsplads parkeringsplads) {
        return this.parkeringspladser.remove(parkeringsplads);
    }

    public ArrayList<Parkeringsplads> getParkeringspladser() {
        return this.getParkeringspladser();
    }

    public void addParkeringsplads(Parkeringsplads parkeringsplads) {
        parkeringspladser.add(parkeringsplads);
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    //    S2
    public int antalLedigePladser() {
        int ledigePladser = 0;
        for (Parkeringsplads parkeringsplads : this.parkeringspladser) {
            if (parkeringsplads.getBil() == null) {
                ledigePladser++;
            }
        }
        return ledigePladser;
    }

//    S3 TODO: Lineaer soegning.
    public int findPladsMedBil(String regNummer) {
        for (Parkeringsplads parkeringsplads : parkeringspladser) {
            if (parkeringsplads.getBil().getRegNr().equals(regNummer)) {
                return parkeringsplads.getNummer();
            }
        }
        return -1;
    }

    public int findAntalBiler(Bilmaerke bilmaerke) {
        int total = 0;
        for (Parkeringsplads parkeringsplads : parkeringspladser) {
            if (parkeringsplads.getBil().getBilmaerke() == bilmaerke) {
                total++;
            }
        }
        return total;
    }

    public ArrayList<String> optagnePladser() {
        ArrayList<String> list = new ArrayList<>();
        String text = "";
        for (Parkeringsplads parkeringsplads : parkeringspladser) {
            if (parkeringsplads.getBil() == null) {
                text += parkeringsplads.getNummer() +
                        " " + parkeringsplads.getBil().getRegNr() +
                        " " + parkeringsplads.getBil().getBilmaerke();
                list.add(text);
            }
        }
        return list;
    }





}
