import java.util.ArrayList;

/**
 * Created by Laukess on 01-06-2016.
 */
public class Storage {

    private static ArrayList<Parkeringshus> parkeringshuse = new ArrayList<Parkeringshus>();
    private static ArrayList<Bil> biler = new ArrayList<>();

    public static boolean addParkeringshus(Parkeringshus parkeringshus) {
        return parkeringshuse.add(parkeringshus);
    }

    public static boolean addBil(Bil bil)  {
        return biler.add(bil);
    }

    public static ArrayList<Parkeringshus> getParkeringshuse() {
        return parkeringshuse;
    }

    public static ArrayList<Bil> getBiler() {
        return biler;
    }

}
