import java.awt.*;
import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * Created by Laukess on 01-06-2016.
 */
public class Service {

    public static void createSomeObjects() {
        Bil bil1 = createBil("AB 11 222");
        Bil bil2 = createBil("EF 33 444");
        Bil bil3 = createBil("BD 55 666");

        Parkeringshus p1 = createParkeringshus("Havnegade 12, 8000 Aarhus");

        for (int i = 1 ; i <= 100 ; i++) {
            createParkeringsplads(i, p1);
        }

        for (int i = 101 ; i <= 110 ; i++) {
            createInvalideplads(i, p1, 18);
        }

        ArrayList<Parkeringsplads> parkeringspladser = p1.getParkeringspladser();
        parkeringspladser.get(22).setBil(bil1);
        parkeringspladser.get(44).setBil(bil2);
        parkeringspladser.get(105).setBil(bil3);
    }

    public static Bil createBil(String regNr) {
        Bil bil = new Bil(regNr);
        Storage.addBil(bil);
        return bil;
    }

    public static Parkeringshus createParkeringshus(String adresse) {
        Parkeringshus parkeringshus = new Parkeringshus(adresse);
        Storage.addParkeringshus(parkeringshus);
        return parkeringshus;
    }

    public static Parkeringsplads createParkeringsplads(int nummer, Parkeringshus parkeringshus) {
        return parkeringshus.createParkeringsplads(nummer);
    }

    public static Invalideplads createInvalideplads(int nummer, Parkeringshus parkeringshus, double areal) {
        return parkeringshus.createInvalideParkeringsplads(nummer, areal);
    }

    public void writeOptagnePladser(Parkeringshus hus, String filnavn) {
        try {
            String text = "";
            File outputFile = new File(filnavn + ".txt");
            PrintWriter out = new PrintWriter(outputFile);

            out.println("test");

            for (Parkeringsplads plads: hus.getParkeringspladser()) {
                if (plads.getBil() != null) {

                    text += plads.getNummer() +
                            " " + plads.getBil().getRegNr() +
                            " " + plads.getBil().getBilmaerke();


                    out.println(text);
                }
            }
            out.close();

        } catch (Exception e) {
            System.out.println("Exception: " + e.getMessage());
        }
    }

}
