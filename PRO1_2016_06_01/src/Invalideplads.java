import java.time.LocalDate;

/**
 * Created by Laukess on 01-06-2016.
 */
public class Invalideplads extends Parkeringsplads {

    private double areal;

    public Invalideplads(int nummer, Parkeringshus parkeringshus, double areal) {
        super(nummer, parkeringshus);
        this.areal = areal;
    }

    public double getAreal() {
        return areal;
    }

    public void setAreal(double areal) {
        this.areal = areal;
    }
}
