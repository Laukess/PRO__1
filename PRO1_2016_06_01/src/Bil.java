/**
 * Created by Laukess on 01-06-2016.
 */
public class Bil {

    private String regNr;
    private Bilmaerke bilmaerke;

    public Bil(String regNr) {
        this.regNr = regNr;
    }

    public String getRegNr() {
        return regNr;
    }

    public void setRegNr(String regNr) {
        this.regNr = regNr;
    }

    public Bilmaerke getBilmaerke() {
        return bilmaerke;
    }

    public void setBilmaerke(Bilmaerke bilmaerke) {
        this.bilmaerke = bilmaerke;
    }
}
