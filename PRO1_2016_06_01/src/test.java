import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * Created by Laukess on 01-06-2016.
 */
public class test {

    public static void main(String[] args) {
        System.out.println(test());
    }

    public static int test() {
        int unitPris = 6;
        double parkingDurationInMinutes = Duration.between(LocalDateTime.of(2016, 05, 10, 12, 00, 00), LocalDateTime.of(2016, 05, 10, 12, 21, 00)).toMinutes();
        System.out.println(parkingDurationInMinutes);
        int durationInUnits = (int) parkingDurationInMinutes / 10;
        if (parkingDurationInMinutes % 10 > 0) {
            return (durationInUnits + 1) * unitPris;
        } else {
            return durationInUnits * unitPris;
        }
    }
}
