import java.time.Duration;
import java.time.LocalDate;

/**
 * Created by Laukess on 01-06-2016.
 */
public class Parkeringsplads {

    private int nummer;
    private LocalDate ankomst;
    private Parkeringshus parkeringshus;
    private Bil bil;

    public Parkeringsplads(int nummer, Parkeringshus parkeringshus) {
        this.nummer = nummer;
        this.ankomst = null;
        this.parkeringshus = parkeringshus;
    }

    public int getNummer() {
        return nummer;
    }

    public void setNummer(int nummer) {
        this.nummer = nummer;
    }

    public LocalDate getAnkomst() {
        return ankomst;
    }

    public void setAnkomst(LocalDate ankomst) {
        this.ankomst = ankomst;
    }

    public Parkeringshus getParkeringshus() {
        return parkeringshus;
    }

    public void setParkeringshus(Parkeringshus parkeringshus) {
        this.parkeringshus = parkeringshus;
    }

    public Bil getBil() {
        return bil;
    }

    public void setBil(Bil bil) {
        this.bil = bil;
    }

//    S4
    public double getPris(LocalDate afgang) {
        int unitPris = 6;
        double parkingDurationInMinutes = Duration.between(this.ankomst, afgang).toMinutes();
        int durationInUnits = (int) parkingDurationInMinutes / 10;
        if (parkingDurationInMinutes % 10 > 0) {
            return (durationInUnits + 1) * unitPris;
        } else {
            return parkingDurationInMinutes * unitPris;
        }
    }

//    S5
    public void hentBil(LocalDate afgangstidspunkt) {
        this.parkeringshus.setSaldo(this.parkeringshus.getSaldo() + getPris(afgangstidspunkt));
        this.setBil(null);
    }
}
