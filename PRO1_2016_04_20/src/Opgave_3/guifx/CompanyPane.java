package Opgave_3.guifx;

import Opgave_3.application.model.Company;
import Opgave_3.application.model.Customer;
import Opgave_3.application.model.Employee;
import Opgave_3.application.service.Service;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.util.Optional;


public class CompanyPane extends GridPane {
    private TextField txfName, txfHours;
    private TextArea txaEmps, txaCus;
    private ListView<Company> lvwCompanies;

    public CompanyPane() {
        this.setPadding(new Insets(20));
        this.setHgap(20);
        this.setVgap(10);
        this.setGridLinesVisible(false);

        Label lblComp = new Label("Companies");
        this.add(lblComp, 0, 0);

        lvwCompanies = new ListView<>();
        this.add(lvwCompanies, 0, 1, 1, 4);
        lvwCompanies.setPrefWidth(200);
        lvwCompanies.setPrefHeight(200);
        lvwCompanies.getItems().setAll(Service.getCompanies());

        ChangeListener<Company> listener =
            (ov, oldCompny, newCompany) -> this.selectedCompanyChanged();
        lvwCompanies.getSelectionModel().selectedItemProperty().addListener(listener);

        Label lblName = new Label("Name:");
        this.add(lblName, 1, 1);

        txfName = new TextField();
        this.add(txfName, 2, 1);
        txfName.setEditable(false);

        Label lblHours = new Label("Weekly Hours:");
        this.add(lblHours, 1, 2);

        txfHours = new TextField();
        this.add(txfHours, 2, 2);
        txfHours.setEditable(false);

        Label lblEmps = new Label("Employees:");
        this.add(lblEmps, 1, 3);
        GridPane.setValignment(lblEmps, VPos.BASELINE);
        lblEmps.setPadding(new Insets(4, 0, 4, 0));

        txaEmps = new TextArea();
        this.add(txaEmps, 2, 3);
        txaEmps.setPrefWidth(200);
        txaEmps.setPrefHeight(60);
        txaEmps.setEditable(false);

        Label lblCus = new Label("Customers:");
        this.add(lblCus, 1, 4);
        GridPane.setValignment(lblCus, VPos.BASELINE);
        lblCus.setPadding(new Insets(4, 0, 4, 0));

        txaCus = new TextArea();
        this.add(txaCus, 2, 4);
        txaCus.setPrefWidth(200);
        txaCus.setPrefHeight(10);
        txaCus.setEditable(false);

        HBox hbxButtons = new HBox(40);
        this.add(hbxButtons, 0, 5, 4, 1);
        hbxButtons.setPadding(new Insets(10, 0, 0, 0));
        hbxButtons.setAlignment(Pos.BASELINE_CENTER);

        Button btnCreate = new Button("Create");
        hbxButtons.getChildren().add(btnCreate);
        btnCreate.setOnAction(event -> this.createAction());

        Button btnUpdate = new Button("Update");
        hbxButtons.getChildren().add(btnUpdate);
        btnUpdate.setOnAction(event -> this.updateAction());

        Button btnDelete = new Button("Delete");
        hbxButtons.getChildren().add(btnDelete);
        btnDelete.setOnAction(event -> this.deleteAction());

        Button btnCustomer = new Button("Add Customer");
        hbxButtons.getChildren().add(btnCustomer);
        btnCustomer.setOnAction(event -> this.AddCustomerAction());

        if (lvwCompanies.getItems().size() > 0) {
            lvwCompanies.getSelectionModel().select(0);
        }
    }

    // -------------------------------------------------------------------------

    private void createAction() {
        CompanyWindow dia = new CompanyWindow("Create Company");
        dia.showAndWait();

        // Wait for the modal dialog to close

        lvwCompanies.getItems().setAll(Service.getCompanies());
        int index = lvwCompanies.getItems().size() - 1;
        lvwCompanies.getSelectionModel().select(index);
    }

    private void AddCustomerAction() {
        Company company = lvwCompanies.getSelectionModel().getSelectedItem();
        if (company == null) return;

        CustomerWindow dia2 = new CustomerWindow("Add Customer", company);
        dia2.showAndWait();

        int selectIndex = lvwCompanies.getSelectionModel().getSelectedIndex();
        lvwCompanies.getItems().setAll(Service.getCompanies());
        lvwCompanies.getSelectionModel().select(selectIndex);
    }

    private void updateAction() {
        Company company = lvwCompanies.getSelectionModel().getSelectedItem();
        if (company == null)
            return;

        CompanyWindow dia = new CompanyWindow("Update Company", company);
        dia.showAndWait();

        // Wait for the modal dialog to close

        int selectIndex = lvwCompanies.getSelectionModel().getSelectedIndex();
        lvwCompanies.getItems().setAll(Service.getCompanies());
        lvwCompanies.getSelectionModel().select(selectIndex);
    }

    private void deleteAction() {
        Company company = lvwCompanies.getSelectionModel().getSelectedItem();
        if (company == null)
            return;

        if (company.employeesCount() == 0) {
            Stage owner = (Stage) this.getScene().getWindow();
            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Delete Company");
            alert.initOwner(owner);
            alert.setHeaderText("Are you sure?");

            // Wait for the modal dialog to close
            Optional<ButtonType> result = alert.showAndWait();
            if (result.isPresent() && result.get() == ButtonType.OK) {
                Service.deleteCompany(company);
                lvwCompanies.getItems().setAll(Service.getCompanies());
                this.updateControls();
            }
        } else {
            Stage owner = (Stage) this.getScene().getWindow();
            Alert alert = new Alert(AlertType.ERROR, "Delete Company");
            alert.setHeaderText("Can't delete a company that has employees");
            alert.initOwner(owner);
            alert.showAndWait();
        }
    }

    // -------------------------------------------------------------------------

    private void selectedCompanyChanged() {
        this.updateControls();
    }

    public void updateControls() {
        Company company = lvwCompanies.getSelectionModel().getSelectedItem();
        if (company != null) {
            txfName.setText(company.getName());
            txfHours.setText("" + company.getHours());
            StringBuilder sb = new StringBuilder();
            for (Employee emp : company.getEmployees()) {
                sb.append(emp + "\n");
            }
            txaEmps.setText(sb.toString());
            sb.delete(0, sb.length());
            for (Customer cus : company.getCustomers()) {
                sb.append(cus.getNavn() + "\n");
            }
            txaCus.setText(sb.toString());
        } else {
            txfName.clear();
            txfHours.clear();
            txaEmps.clear();
        }
    }

}
