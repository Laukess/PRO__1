package Opgave_3.application.model;

/**
 * Created by lukas on 20-04-2016.
 */
public class Customer {
    private String navn;

    public Customer(String navn) {
        this.navn = navn;
    }

    public String getNavn() {
        return this.navn;
    }
}
