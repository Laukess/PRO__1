import java.util.ArrayList;
import java.util.IntSummaryStatistics;

/**
 * Created by lukas on 14-03-2016.
 */
public class ArrayListApp {

    public static void main(String[] args) {

        ArrayList<String> myList = new ArrayList<>();

        myList.add("Hans");
        myList.add("Viggo");
        myList.add("Jens");
        myList.add("Børge");
        myList.add("Bent");

        System.out.println(myList.size());
        myList.add(2, "Jane");

        System.out.println(myList);

        myList.remove(1);

        System.out.println(myList);

        myList.add(0, "Pia");

        System.out.println(myList);

        myList.add("Ib");

        System.out.println(myList);

        System.out.println(myList.size());

        myList.set(2, "Hansi");

        System.out.println(myList);

        System.out.println(myList);

        for (int i = 0 ; i < myList.size() ; i++) {
            System.out.print(myList.get(i).length() + "," );
        }

        for (String item : myList) {
            System.out.print(item.length() + "," );
        }


        System.out.println("\n-----------------------------------------------------");

        /* FRA KLASSEN! START */

        ArrayList<Integer> list = new ArrayList<Integer>();
        list.add(19);
        list.add(35);
        list.add(14);
        list.add(7);

        ArrayListMethods methods = new ArrayListMethods();

        System.out.println(list);
        System.out.println("Summen af tallene i listen (med for): "
                + methods.sumListeFor(list));
        System.out.println("Summen af tallene i listen (med forEach): "
                + methods.sumListeForEach(list));
        System.out.println("Indeks for det fÃ¸rste lige tal: "
                + methods.hasEvenAtIndex(list));

        /* FRA KLASSEN! SLUT */
        ArrayList<Integer> emptyArrayListn = new ArrayList<>();

        System.out.println("min -> " + methods.min(list));
        System.out.println("avg -> " + methods.gennemsnit(list));
        System.out.println("avg -> " + methods.gennemsnit(emptyArrayListn));

        System.out.println("Antal nuller -> " + methods.antalNuller(list));
        list.add(0);
        list.add(0);
        list.add(0);
        System.out.println(list);
        System.out.println("Antal nuller -> " + methods.antalNuller(list));
        System.out.println("ligetal bliver erstattet med 0'er -> ");
        methods.erstatLigeMedNul(list);
        System.out.println(list);
        list.set(2, 14);
        list.set(4, 20);
        list.set(5, 22);
        list.set(6, 30);
        System.out.println("aendret nogle vaerdier, nu ser listen saadan her ud ->");
        System.out.println(list);
        System.out.println("Alle lige elementer i listen -> " + methods.enLigeListe(list));

        System.out.println("------------ Opgave 3 ----------------------------");

        ArrayList<Integer> ex3List = new ArrayList<>();
        ex3List.add(7);
        ex3List.add(6);
        ex3List.add(5);
        ex3List.add(4);
        ex3List.add(3);
        ex3List.add(2);
        ex3List.add(1);
        System.out.println(ex3List);
        methods.four_one(ex3List);
        System.out.println(ex3List);
        System.out.println("Det naest stoerste element er -> " + methods.four_two(ex3List));

        ArrayList<Integer> ex3List1 = new ArrayList<>();
        ArrayList<Integer> ex3List2 = new ArrayList<>();
        ArrayList<Integer> ex3List3 = new ArrayList<>();

        ex3List1.add(1);
        ex3List1.add(2);
        ex3List1.add(3);

        ex3List2.add(1);
        ex3List2.add(3);
        ex3List2.add(2);

        ex3List3.add(3);
        ex3List3.add(2);
        ex3List3.add(1);

        System.out.println("Ër " + ex3List1 + " i orden ? " + methods.isInOrder(ex3List1));
        System.out.println("Ër " + ex3List2 + " i orden ? " + methods.isInOrder(ex3List2));
        System.out.println("Ër " + ex3List3 + " i orden ? " + methods.isInOrder(ex3List3));

        System.out.println("har " + ex3List +  " dublicates -> " + methods.hasDuplicates(ex3List));
        ex3List.add(4);
        System.out.println("har " + ex3List +  " dublicates -> " + methods.hasDuplicates(ex3List));

        ArrayList<Integer> ex3List4 = new ArrayList<>();
        ArrayList<Integer> ex3List5 = new ArrayList<>();
        ex3List4.add(1);
        ex3List4.add(1);
        ex3List4.add(3);
        ex3List4.add(4);

        ex3List5.add(1);
        ex3List5.add(3);
        ex3List5.add(4);
        ex3List5.add(4);

        System.out.println("har " + ex3List4 + " og " + ex3List5 + " det samme indhold? " + methods.isIdentical(ex3List4, ex3List5));

        ex3List4.clear();
        ex3List4.add(1);
        ex3List4.add(2);
        ex3List4.add(3);
        ex3List4.add(4);

        ex3List5.clear();
        ex3List5.add(3);
        ex3List5.add(2);
        ex3List5.add(1);
        ex3List5.add(4);

        System.out.println("har " + ex3List4 + " og " + ex3List5 + " det samme indhold? " + methods.isIdentical(ex3List4, ex3List5));
        ex3List4.clear();
        ex3List5.clear();
        System.out.println("har " + ex3List4 + " og " + ex3List5 + " det samme indhold? " + methods.isIdentical(ex3List4, ex3List5));

    }

}