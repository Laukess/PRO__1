package Opgave_3;

import java.util.ArrayList;

/**
 * Created by lukas on 14-03-2016.
 */
public class Team {
    private String name;
    private ArrayList<Player> players;

    public Team(String name) {
        this.name = name;
        this.players = new ArrayList<>();
    }

    public String getName() {
        return this.name;
    }

    public void addPlayer(Player player) {
        this.players.add(player);
    }

    public void printPlayer() {
        for (Player player : this.players) {
            System.out.println(player.getName() + " " + player.getAge() + " " + player.getScore() + ".");
        }
    }

    public double calcAvgAge() {
        double sum = 0.0;
        for (Player player : this.players) {
            sum += player.getAge();
        }

        if (players.size() > 0) {
            return sum / this.players.size();
        } else {
            return sum;
        }

    }

    public int calcTotalScore() {
        int total = 0;
        for (Player player : players) {
            total += player.getScore();
        }
        return total;
    }

    public int calcOldPlayersScore(int agelimit) {
        int sum = 0;
        for (Player player : players) {
            if (player.getAge() > agelimit) {
                sum += player.getScore();
            }
        }
        return sum;
    }

    public int maxScore() {
        int max = Integer.MIN_VALUE;
        for (Player player : players) {
            if (player.getScore() > max) {
                max = player.getScore();
            }
        }
        return max;
    }

    public String bestPlayer() {
        int maxScore = maxScore();

        for (Player player : players) {
            if (player.getScore() == maxScore) {
                return player.getName();
            }
        }
        return "";
    }


}
