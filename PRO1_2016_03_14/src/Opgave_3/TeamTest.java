package Opgave_3;

/**
 * Created by lukas on 14-03-2016.
 */
public class TeamTest {

    public static void main(String[] args) {
        Team team = new Team("FC Bold");
        team.addPlayer(new Player("Peter", 23, 2));
        team.addPlayer(new Player("john", 24, 3));
        team.addPlayer(new Player("alex", 25, 0));
        team.addPlayer(new Player("doe", 26, 2));
        team.addPlayer(new Player("smoe", 27, 10));

        team.printPlayer();

        System.out.println("avg age -> " + team.calcAvgAge());
        System.out.println("holdets samlede score -> " + team.calcTotalScore());
        System.out.println("Score for spillere over 10 aar -> " + team.calcOldPlayersScore(10));
        System.out.println("Score for spillere over 25 aar -> " + team.calcOldPlayersScore(25));
        System.out.println("Max Score blandt Spillerne -> " + team.maxScore());
    }
}
