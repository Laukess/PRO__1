import java.util.ArrayList;
import java.util.IdentityHashMap;

/**
 * Created by lukas on 14-03-2016.
 */
public class ArrayListMethods {

    /**
     * Returnerer det mindste tal i list
     */
    public int min(ArrayList<Integer> list) {
        int min = Integer.MAX_VALUE;

        for (int item : list) {
            if (item < min) {
                min = item;
            }
        }
        return min;
    }

    /**
     * Returnerer gemmensnittet af tallene i list
     */
    public double gennemsnit(ArrayList<Integer> list) {
        Double sum = 0.0;

        for (int item : list) {
            sum += item;
        }

        if (list.size() > 0) {
            return sum / list.size();
        } else {
            return sum;
        }

    }

    /**
     * Returnerer antallet af gange 0 er i list
     */
    public int antalNuller(ArrayList<Integer> list) {
        int numOfZero = 0;

        for (int item : list) {
            if (item == 0) {
                numOfZero++;
            }
        }
        return numOfZero;
    }

    /**
     * Opdater listen så alle lige tal erstattes med 0
     */
    public void erstatLigeMedNul(ArrayList<Integer> list) {
        for (int i = 0 ; i < list.size() ; i++) {
            if(list.get(i) % 2 == 0) {
                list.set(i, 0);
            }
        }
    }

    /**
     * Returnerer en ny liste indeholdende alle de lige tal fra list
     */
    public ArrayList<Integer> enLigeListe(ArrayList<Integer> list) {
        ArrayList<Integer> newList = new ArrayList<>();

        for (int item : list) {
            if (item % 2 == 0) {
                newList.add(item);
            }
        }
        return newList;
    }

    public void four_one(ArrayList<Integer> list) {
        int newIndex = 0;
        for (int i = 0 ; i < list.size() ; i++) {
            if (list.get(i) % 2 == 0) {
                list.add(newIndex++, list.remove(i));
            }
        }
    }

    public int four_two(ArrayList<Integer> list) {
        int largest = Integer.MIN_VALUE;
        int nextLargest = largest;

        for (int item : list) {
            if (item > largest) {
                nextLargest = largest;
                largest = item;
            } else if(item > nextLargest) {
                nextLargest = item;
            }
        }
        return nextLargest;
    }

    public boolean isInOrder(ArrayList<Integer> list) {

        for (int i = 0 ; i < list.size() - 1 ; i++) {
            if (list.get(i) > list.get(i+1)) {
                return false;
            }
        }
        return true;

    }

    public boolean hasDuplicates(ArrayList<Integer> list ){

        for (int i = 0 ; i < list.size() ; i++) {
            for (int j = 0 ; j < list.size() ; j++) {
                if (list.get(i) == list.get(j) && i != j) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isIdentical(ArrayList<Integer> list, ArrayList<Integer> list2) {

        ArrayList<Integer> control = new ArrayList<>(list2);

        if (control.size() == list.size()) {
            for (int item : list) {
                if (control.contains(item)) {
                    control.remove(control.indexOf(item));
                }
            }
        } else {
            return false;
        }
        // skal den return false hvis de 2 arrayLists er tomme ? goer den nu, men er det en go' idee?
        if (control.size() == 0 && (list.size() != 0 && list2.size() != 0)) {
            return true;
        } else {
            return false;
        }

        /*
        if (list.size() == list2.size()) {
            for (int item : list) {
                if(!list2.contains(item)) {
                    return false;
                }
            }
        }
        return true;
        */
    }


    /*************************************************************************************/


    /**
     * Sums the numbers in the list using a for loop each
     */
    public int sumListeForEach(ArrayList<Integer> list) {
        int resultat = 0;
        for (int tal : list) {
            resultat = resultat + tal;
        }
        return resultat;
    }

    /**
     * Sums the numbers in the list using a for loop
     */
    public int sumListeFor(ArrayList<Integer> list) {
        int resultat = 0;
        for (int i = 0; i < list.size(); i++) {
            resultat = resultat + list.get(i);
        }
        return resultat;
    }

    /**
     * Returns the index of the first even number
     */
    public int hasEvenAtIndex(ArrayList<Integer> list) {
        int index = -1;
        int i = 0;
        while (index == -1 && i < list.size()) {
            if (list.get(i) % 2 == 0) {
                index = i;
            }
            i++;
        }
        return index;
    }


    /*************************************************************************************/

}
