package Child;

public class ChildApp {

    public static void main(String[] args) {
        Child child = new Child(2, false, new double[]{4.2, 9.3, 12.4, 17.5, 23.2, 25.3, 28.6, 30.4, 33.9, 35.1, 37.3});

        System.out.println(String.format("Alder: %s", child.getAge()));
        String inst = child.institution();
        System.out.println(String.format("Institution: %s", inst));

        System.out.println("weight at 10 -> " + child.getWeight(10));
        System.out.println("biggest gain -> " + child.getBiggestWeightGain());

        System.out.println("-------------------------------------------------------------");

        Institution institution = new Institution("lillehoej", "EnGadeIEnBy12");
        Child[] children = new Child[]{
                new Child(3, false, new double[]{4.2, 9.3, 12.4, 17.5, 23.2, 25.3, 28.6, 30.4, 33.9, 35.1, 37.3}),
                new Child(4, false, new double[]{4.2, 9.3, 12.4, 17.5, 23.2, 25.3, 28.6, 30.4, 33.9, 35.1, 37.3}),
                new Child(2, false, new double[]{4.2, 9.3, 12.4, 17.5, 23.2, 25.3, 28.6, 30.4, 33.9, 35.1, 37.3}),
                new Child(2, true, new double[]{4.2, 9.3, 12.4, 17.5, 23.2, 25.3, 28.6, 30.4, 33.9, 35.1, 37.3}),
                new Child(5, true, new double[]{4.2, 9.3, 12.4, 17.5, 23.2, 25.3, 28.6, 30.4, 33.9, 35.1, 37.3}),
                new Child(3, false, new double[]{4.2, 9.3, 12.4, 17.5, 23.2, 25.3, 28.6, 30.4, 33.9, 35.1, 37.3}),
                new Child(3, true, new double[]{4.2, 9.3, 12.4, 17.5, 23.2, 25.3, 28.6, 30.4, 33.9, 35.1, 37.3}),
                new Child(4, true, new double[]{4.2, 9.3, 12.4, 17.5, 23.2, 25.3, 28.6, 30.4, 33.9, 35.1, 37.3}),
                new Child(3, false, new double[]{4.2, 9.3, 12.4, 17.5, 23.2, 25.3, 28.6, 30.4, 33.9, 35.1, 37.3}),
                new Child(5, false, new double[]{4.2, 9.3, 12.4, 17.5, 23.2, 25.3, 28.6, 30.4, 33.9, 35.1, 37.3})
        };

        institution.addChildren(children);

        System.out.println("AVG alder -> " + institution.gennemsnitsalder());
        System.out.println("Piger -> " + institution.girlsCount());
        System.out.println("Drenge -> " + institution.boysCount());

    }

}
