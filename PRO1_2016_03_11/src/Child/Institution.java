package Child;

/**
 * Created by lukas on 13-03-2016.
 */
public class Institution {

    private String navn;
    private String adresse;
    private int numOfChilden;
    private int MAX_CHILDREN = 25;
    private Child[] children;


    public Institution(String navn, String adresse) {
        this.navn = navn;
        this.adresse = adresse;
        this.numOfChilden = 0;

        this.children = new Child[MAX_CHILDREN];
    }

    public void addChildren(Child[] children) {
        for (Child child : children) {
            if (!addChild(child)) {
                System.out.println("Der er desvaerre ikke plads til dette barn");
            }
        }
    }

    public boolean addChild(Child child) {
        if (numOfChilden < MAX_CHILDREN) {
            children[numOfChilden] = child;
            numOfChilden++;
            return true;
        }
        return false;
    }

    public double gennemsnitsalder() {
        double sum = 0;

        for (int i = 0 ; i < numOfChilden ; i++) {
            sum += children[i].getAge();
        }

        if (numOfChilden != 0) {
            return sum / children.length;
        } else {
            return -1;
        }

    }

    public int girlsCount() {
        int sum = 0;

        for (int i = 0 ; i < numOfChilden ; i++) {
            if (!children[i].isBoy()) {
                sum += 1;
            }
        }
        return sum;
    }

    public int boysCount() {
        int sum = 0;

        for (int i = 0 ; i < numOfChilden ; i++) {
            if (children[i].isBoy()) {
                sum += 1;
            }
        }
        return sum;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getNavn() {
        return navn;
    }

    public void setNavn(String navn) {
        this.navn = navn;
    }


}
