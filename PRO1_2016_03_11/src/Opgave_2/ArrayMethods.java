package Opgave_2;

/**
 * Created by lukas on 11-03-2016.
 */

public class ArrayMethods {

    public int sum(int[] array) {
        int sum = 0;
        for (int item : array) {
            sum += item;
        }
        return sum;
    }

    public int[] createSum(int[] a, int[] b) {
        int[] largestArray;
        int[] smallestArray;

        largestArray = a.length > b.length ? a : b;
        smallestArray = a.length < b.length ? a : b;

        int[] newArray = new int[largestArray.length];

        for (int i = 0 ; i < smallestArray.length ; i++) {
            newArray[i] =  a[i] + b[i];
        }

        for (int i = smallestArray.length ; i < largestArray.length ; i++) {
            newArray[i] = largestArray[i];
        }

        return newArray;

    }

    public int[] createSum2(int[] a, int[] b) {
        int[] new_list = new int[a.length];

        for (int i = 0 ; i < new_list.length ; i++) {
            new_list[i] = a[i] + b[i];
        }

        return new_list;
    }

    public boolean hasUneven(int[] t) {
        for (int item : t) {
            if (item % 2 != 0 ) {
                return true;
            }
        }
        return false;
    }

}
