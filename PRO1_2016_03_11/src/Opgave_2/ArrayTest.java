package Opgave_2;

/**
 * Created by lukas on 11-03-2016.
 */

public class ArrayTest {
    public static void main(String[] args) {
        ArrayMethods method = new ArrayMethods();

        System.out.println(method.sum(new int[]{4, 6, 7, 2, 3}));

        int[] anArray = method.createSum(new int[]{2, 3, 4, 2, 5}, new int[]{2, 3, 4, 16});

        for (int item : anArray) {
            System.out.println(item);
        }

        int[] anArray2 = method.createSum2(new int[]{2, 3, 4, 12}, new int[]{2, 3, 4, 16});

        for (int item : anArray2) {
            System.out.println(" -> " + item);
        }

        System.out.println(method.hasUneven(new int[]{4, 6, 7, 2, 3}));
        System.out.println(method.hasUneven(new int[]{4, 6, 8, 2, 6}));

    }
}
