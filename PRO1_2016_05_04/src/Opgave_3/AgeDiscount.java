package Opgave_3;

/**
 * Created by Laukess on 08-05-2016.
 */
public class AgeDiscount implements Discount {

    private double discount = 0;

    public double getDiscount() {
        return discount * 10;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getDiscountedPrice(double price) {
        return price - getDiscount();
    }
}
