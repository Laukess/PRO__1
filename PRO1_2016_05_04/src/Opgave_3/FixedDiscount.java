package Opgave_3;

/**
 * Created by lukas on 21-04-2016.
 */
public class FixedDiscount implements Discount {

    public double discount = 0;

    public double getDiscount() {
        return 250;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getDiscountedPrice(double origPrice) {
        return origPrice > 1000 ? getDiscount() : 0;
    }

}
