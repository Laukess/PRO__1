package Opgave_3;

/**
 * Created by Laukess on 08-05-2016.
 */
public interface Discount {

    double getDiscount();

    void setDiscount(double price);

    double getDiscountedPrice(double origPrice);




}
