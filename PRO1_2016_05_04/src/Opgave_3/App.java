package Opgave_3;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Created by lukas on 21-04-2016.
 */
public class App {

    public static void main(String[] args) {

        Product p0 = new Product(1, "1000W PSU", 750);
        Product p1 = new Product(2, "380x", 2000);
        Product p2 = new Product(3, "her", 140);
        Product p3 = new Product(4, "oeu", 1458);
        Product p4 = new Product(5, "oeuoeuoe", 45);

        Customer c1 = new Customer("peter parker", LocalDate.of(2016, 12, 24));
        Discount discount1 = new PercentDiscount();
        discount1.setDiscount(0.15);
        c1.setDiscount(discount1);

        Customer c2 = new Customer("John Doe", LocalDate.of(2015, 4, 14));
        Discount discount2 = new FixedDiscount();
        discount2.setDiscount(250);
        c2.setDiscount(discount2);

        Order o1 = new Order(12);
        o1.createOrderLine(2, p0);
        o1.createOrderLine(2, p1);

        Order o2 = new Order(13);
        o2.createOrderLine(2, p2);
        o2.createOrderLine(2, p3);
        o2.createOrderLine(2, p4);
        o2.createOrderLine(1, p0);

        c1.addOrder(o1);
        c2.addOrder(o2);

        ArrayList<Customer> cl = new ArrayList<>();
        cl.add(c1);
        cl.add(c2);

        for (Customer c : cl) {
            double totalPrice = 0;
            double totalDiscount = 0;
            for(Order o : c.getOrders()) {
                totalPrice += o.getOrderPrice();
                totalDiscount += c.getDiscount(o);
            }
            System.out.println("Final pris: " + (totalPrice - totalDiscount));
            System.out.println("Withou Discount: " + totalPrice);
            System.out.println("Discount: " + totalDiscount);
            System.out.println("----------------------------------------------------");
        }

    }


}
