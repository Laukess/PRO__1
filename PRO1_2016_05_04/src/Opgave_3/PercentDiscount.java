package Opgave_3;

/**
 * Created by lukas on 21-04-2016.
 */
public class PercentDiscount implements Discount {

    double discount = 0.00;

    public double getDiscount() {
       return 1 - this.discount;
    }

    public void setDiscount(double percent) {
        this.discount = percent;
    }

    public double getDiscountedPrice(double origPrice) {
        return getDiscount() * origPrice;
    }

}
