package Opgave_2;

import java.util.Arrays;
import java.util.Comparator;

/**
 * Created by Laukess on 04-05-2016.
 */
public class Customer implements Comparable<Customer>{

    private String fornavn;
    private String efternavn;
    private int alder;

    public Customer(String fornavn, String efternavn, int alder) {
        this.fornavn = fornavn;
        this.efternavn = efternavn;
        this.alder = alder;
    }

    public String toString() {
        return this.fornavn + " " + this.efternavn + "(" + this.alder + ")";
    }

    @Override
    public int compareTo(Customer other) {
        if(this.efternavn.compareTo(other.efternavn) == 0) {
            if(this.fornavn.compareTo(other.fornavn) == 0) {
                if (this.alder == other.alder) {
                    return 0;
                } else if (this.alder > other.alder) {
                    return 1;
                } else {
                    return -1;
                }
            }
            else {
                return this.fornavn.compareTo(other.fornavn);
            }
        } else {
            return this.efternavn.compareTo(other.efternavn);
        }
    }

    public String getFornavn() {
        return fornavn;
    }

    public void setFornavn(String fornavn) {
        this.fornavn = fornavn;
    }

    public String getEfternavn() {
        return efternavn;
    }

    public void setEfternavn(String efternavn) {
        this.efternavn = efternavn;
    }

    public int getAlder() {
        return alder;
    }

    public void setAlder(int alder) {
        this.alder = alder;
    }

    public static Customer lastCustomer(Customer[] customers) {
        Arrays.sort(customers);
        return customers[customers.length -1];
    }

    public static Customer[] afterCustomer(Customer[] customers, Customer customer) {
        Arrays.sort(customers);

        for (int i = 0 ; i < customers.length ; i++)  {

            if (customers[i] == customer) {
                return Arrays.copyOfRange(customers, i+1, customers.length);
            }

        }

        return null;
    }

}