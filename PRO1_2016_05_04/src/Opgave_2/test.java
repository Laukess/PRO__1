package Opgave_2;

/**
 * Created by Laukess on 04-05-2016.
 */
public class test {

    public static void main(String[] args) {

        Customer c4 = new Customer("aaa", "aaa", 10);       // 1
        Customer c41 = new Customer("aaa", "aax", 10);      // 4
        Customer c5 = new Customer("aab", "aab", 11);       // 3
        Customer c6 = new Customer("aaa", "aaa", 11);       // 2

        Customer[] customers = new Customer[]{c4, c41, c5, c6};

        System.out.println(Customer.lastCustomer(customers) + "\n");

        for (Customer c : Customer.afterCustomer(customers, c5)) {
            System.out.println(c);
        }

    }
}
