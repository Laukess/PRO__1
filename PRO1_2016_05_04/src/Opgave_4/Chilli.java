package Opgave_4;

/**
 * Created by Laukess on 04-05-2016.
 */
public class Chilli implements Measurable {

    private String name;
    private double strength;

    public Chilli(String name, double strength) {
        this.name = name;
        this.strength = strength;
    }

    @Override
    public double getMeasure() {
        return this.strength;
    }
}
