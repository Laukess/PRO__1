package Opgave_4;

/**
 * Created by Laukess on 08-05-2016.
 */
public interface Filter {

    public boolean accept(Measurable x);

}
