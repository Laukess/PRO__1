package Opgave_4;

/**
 * Created by Laukess on 04-05-2016.
 */
public interface Measurable {

   double getMeasure();

}
