package Opgave_4;

import java.util.ArrayList;

/**
 * Created by Laukess on 04-05-2016.
 */
public class Data {

    public static double max(Measurable[] objects) {
        double max = 0;

        for(Measurable m : objects) {
            if (m.getMeasure() > max) {
                max = m.getMeasure();
            }
        }
        return max;
    }

    public static double max(ArrayList<? extends Measurable> objects) {
        double max = 0;

        for(Measurable m : objects) {
            if (m.getMeasure() > max) {
                max = m.getMeasure();
            }
        }
        return max;
    }

    public static double avg(Measurable[] objects) {
        double sum = 0.0;

        for (Measurable m : objects) {
            sum += m.getMeasure();
        }

        if (sum != 0) {
            return sum / objects.length;
        } else {
            return sum;
        }

    }

    public static double avg(ArrayList<? extends Measurable> objects) {
        double sum = 0.0;

        for (Measurable m : objects) {
            sum += m.getMeasure();
        }

        if (sum != 0) {
            return sum / objects.size();
        } else {
            return sum;
        }

    }

    public static double avg(Measurable[] objects, Filter filter) {
        double sum = 0.0;
        int counter = 0;

        for (Measurable m : objects) {
            if (filter.accept(m)) {
                counter++;
                sum += m.getMeasure();
            }
        }

        if (sum != 0) {
            return sum / counter;
        } else {
            return sum;
        }
    }

}
