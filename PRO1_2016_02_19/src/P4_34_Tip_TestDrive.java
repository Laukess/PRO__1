import java.util.Scanner;

/**
 * Created by lukas on 20-02-2016.
 */
public class P4_34_Tip_TestDrive {

    public static void main(String[] args) {
        P4_34_Tip tip = new P4_34_Tip();
        Scanner s = new Scanner(System.in);
        int satisfaction;
        double bill;

        do {
            System.out.print("How satisfied were you with the service [1, 2, 3] ? >>> ");
            while (!s.hasNextInt()) {
                System.out.print("How satisfied were you with the service [1, 2, 3] ?? >>> ");
            }
            satisfaction = s.nextInt();
        } while ( satisfaction < 1 || satisfaction > 3 );

        do {
            System.out.print("How big is the bill >>> ");
            while (!s.hasNextDouble()) {
                System.out.print("How big is the bill >>> ");
                s.next();
            }
            bill = s.nextInt();
        } while (bill < 0);

        System.out.print(
                "You should tip $" + tip.calculateTip(satisfaction, bill) +
                        ", total price is " +
                        (bill + tip.calculateTip(satisfaction, bill)));

    }

}
