/**
 * Created by lukas on 20-02-2016.
 */
public class P4_18_Card_TestDrive {

    public static void main(String[] args) {
        P4_18_Card card = new P4_18_Card("AD");

        String[] cardValue = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"};
        String[] cardSuit = {"D", "H", "S", "C"};

        for (int i = 0 ; i < 13 ; i++) {
            for (int j = 0 ; j < 4 ; j++) {
                card.setCard(cardValue[i] + cardSuit[j]);
                System.out.println(card.getDescription());
            }
        }

    }

}
