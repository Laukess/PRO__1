import java.lang.reflect.Array;

/**
 * Created by lukas on 20-02-2016.
 */
public class P4_34_Tip {

    final private double TOTALLY_SATISFIED = 0.20;
    final private double SATISFIED = 0.15;
    final private double DISSATISFIED = 0.10;

    double[] satisfaction = {TOTALLY_SATISFIED, SATISFIED, DISSATISFIED};

    public double calculateTip(int satisfaction, double bill) {
        return this.satisfaction[satisfaction - 1] * bill;
    }

}
