import org.joda.time.LocalDate;

import java.time.Year;

public class PersonApp {

	public static void main(String[] args) {
		Person p = new Person("Bent", "Janus", "Christensen", 27, 12, 1990);
        Person p2 = new Person("Kristian", "Dorland", 22, 2, 1990);

        System.out.println("****************************************************");

        p.printPerson();
		System.out.println("Initialer: " + p.getInit());
		System.out.println("Username: " + p.getUserName());

        System.out.println("****************************************************");

        p2.printPerson();
        System.out.println("Initialer: " + p2.getInit());
        System.out.println("Username: " + p2.getUserName());

        System.out.println("****************************************************");

		System.out.println("Age: " + p.age(27, 12, 1990));

		// int year = 2400; // true
        // int year = 2100; // false
        int year = 2016; // true

		System.out.println("Er " + year + " skudår: " + p.leapYear(year));

        System.out.println("Foedselsdagen falder paa en " + p2.ugedagen(2016) + " i 2016");

	}
}
