/**
 * Created by lukas on 20-02-2016.
 */
public class P4_14_Date {

    public String getSeason(int month, int day) {
        String season = "";

        if (month > 0 && month < 4) {
            season = "Winter";
        } else if (month >= 4 && month < 7) {
            season = "Spring";
        } else if (month >= 7 && month < 10) {
            season = "Summer";
        } else if (month >= 10 && month < 13) {
            season = "Fall";
        }

        if (month % 3 == 0 && day >= 21) {
            if (season.equals("Winter")) {
                season = "Spring";
            } else if (season.equals("Spring")) {
                season = "Summer";
            } else if (season.equals("Summer")) {
                season = "Fall";
            } else {
                season = "Winter";
            }
        }
        return season;
    }

}
