import java.util.Scanner;
/**
 * Created by lukas on 20-02-2016.
 */
public class P4_14_Date_TestDrive {

    public static void main(String[] args) {

        Scanner s = new Scanner(System.in);
        int month;
        int day;

        System.out.print("month >>> ");
        while (!s.hasNextInt()) {
            System.out.print("month >>> ");
            s.next();
        }
        month = s.nextInt();

        System.out.print("day >>> ");
        while (!s.hasNextInt()) {
            System.out.print("day >>> ");
            s.next();
        }
        day = s.nextInt();


        P4_14_Date date = new P4_14_Date();
        System.out.println(date.getSeason(month, day));
    }

}
