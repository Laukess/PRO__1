import org.joda.time.*;

import java.util.Calendar;
import java.util.Date;

/**
 * Write a description of class Person here.
 *
 * @author (Margrethe Dybdahl)
 * @version (12/09/2014)
 */
public class Person {
	private String firstName;
	private String middleName;
	private String lastName;
	private int dayOfBirth; // 1..31
	private int monthOfBirth; // 1..12
	private int yearOfBirth; // 1900..2010

	private DateTime birthDate = null;

	/**
	 * Constructor for objects of class Person
	 */

	public Person(String firstName, String middleName, String lastName,
				  int dayOfBirth, int monthOfBirth, int yearOfBirth) {
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.dayOfBirth = dayOfBirth;
		this.monthOfBirth = monthOfBirth;
		this.yearOfBirth = yearOfBirth;

		this.birthDate = new DateTime(yearOfBirth, monthOfBirth, dayOfBirth, 0, 0);

	}

    public Person(String firstName, String lastName,
                  int dayOfBirth, int monthOfBirth, int yearOfBirth) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dayOfBirth = dayOfBirth;
        this.monthOfBirth = monthOfBirth;
        this.yearOfBirth = yearOfBirth;

        this.birthDate = new DateTime(yearOfBirth, monthOfBirth, dayOfBirth, 0, 0);

    }

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return this.middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void printPerson() {
		System.out.println(firstName + " " + middleName + " " + lastName + " "
				+ dayOfBirth + "." + monthOfBirth + "." + yearOfBirth);

	}

	public String getInit() {

        String initials;

        if (this.middleName == null) {
            initials = "" + firstName.substring(0,2) + lastName.charAt(0);
        } else {
            initials = "" + firstName.charAt(0) + middleName.charAt(0)
                    + lastName.charAt(0);
        }

		return initials;
	}

	public String getUserName() {

        String username;

        if (this.middleName == null) {
            username =
                    this.firstName.substring(0,2).toUpperCase() +
                    (this.firstName.length() - this.lastName.length()) +
                    this.lastName.substring(this.lastName.length()-2).toLowerCase();
        } else {
            username =
                    firstName.substring(0, 2).toUpperCase() +
                    middleName.length() + "" +
                    lastName.substring(lastName.length() - 2);
        }

		return username;

	}

	public int age(int dayToDay, int monthToDay, int yearToDay) {
        DateTime now = new DateTime();
        DateTime bd = new DateTime(yearToDay, monthToDay, dayToDay, 0, 0);
        Period per = new Period(bd, now);
		return per.getYears();
	}

	/**
	 * Returns whether year is a leapyear
	 *
	 * @param year
	 * @return
	 */
	public boolean leapYear(int year) {
        boolean isLeap = false;
        // kan deles med 4
        if (year % 4 == 0) {
            isLeap = true;
            if (year % 100 == 0 && year % 400 != 0) {
                isLeap = false;
            }
        }

        isLeap = new LocalDate(year, 1, 1).year().isLeap();

		return isLeap;
	}

    // Opgave 5
    public String ugedagen(int year) {
        DateTime dt = new DateTime(year, this.monthOfBirth, this.dayOfBirth, 1, 1);
        String[] dag = {"Mandag", "Tirsdag", "Onsdag", "Torsdag", "Fredag", "Loerdag", "Soendag"};
        return dag[dt.getDayOfWeek() -1];
    }

}
