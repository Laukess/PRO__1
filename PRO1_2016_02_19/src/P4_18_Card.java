/**
 * Created by lukas on 20-02-2016.
 */
public class P4_18_Card {

    private String cardShortHand;

    private String diamond =    " of diamonds";
    private String heart =      " of hearts";
    private String spade =      " of spades";
    private String club =       " of clubs";

    public  P4_18_Card(String cardShortHand) {
        this.cardShortHand = cardShortHand;
    }

    public void setCard(String cardShortHand) {
        this.cardShortHand = cardShortHand;
    }

    public String getDescription() {

        String description = "Unknown";

        switch (cardShortHand.substring(0, cardShortHand.length()-1)) {
            case "A":
                description = "Ace";
                break;
            case "K":
                description = "King";
                break;
            case "Q":
                description = "Queen";
                break;
            case "J":
                description = "Jack";
                break;
            case "10":
                description = "Ten";
                break;
            case "9":
                description = "Nine";
                break;
            case "8":
                description = "Eight";
                break;
            case "7":
                description = "Seven";
                break;
            case "6":
                description = "Six";
                break;
            case "5":
                description = "Five";
                break;
            case "4":
                description = "Four";
                break;
            case "3":
                description = "Three";
                break;
            case "2":
                description = "Two";
                break;
        }

        if (this.cardShortHand.substring(this.cardShortHand.length()-1).equals("D")) {
            description += diamond;
        } else if (this.cardShortHand.substring(this.cardShortHand.length()-1).equals("H")) {
            description += heart;
        } else if (this.cardShortHand.substring(this.cardShortHand.length()-1).equals("S")) {
            description += spade;
        } else if (this.cardShortHand.substring(this.cardShortHand.length()-1).equals("C")) {
            description += club;
        }

        return description;

    }

}
