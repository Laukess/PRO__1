package Opgave_2;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Laukess on 26-05-2016.
 */
public class Hand {

    ArrayList<Card> cards = new ArrayList<>();

    public boolean add(Card c) {
        if (cards.contains(c)) {
            return false;
        } else {
            return cards.add(c);
        }
    }

    public boolean remove(Card c) {
        if (cards.contains(c)) {
            return cards.remove(c);
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        String text = "";
        Collections.sort(cards);
        for (Card c : cards) {
            text += c.toString() + "\n";
        }
        return text;
    }
}
