package Opgave_2;

import java.util.ArrayList;

/**
 * Created by Laukess on 26-05-2016.
 */
public class test {

    public static void main(String[] args) {
        Hand hand = new Hand();
        Rank[] ranks = new Rank[]{Rank.TWO, Rank.THREE, Rank.FOUR, Rank.FIVE, Rank.SIX, Rank.SEVEN, Rank.EIGHT, Rank.NINE, Rank.TEN, Rank.JACK, Rank.QUEEN, Rank.KING, Rank.ACE};
        Suit[] suits = new Suit[]{Suit.CLUB, Suit.DIAMOND, Suit.HEART, Suit.SPADE};

        for (Rank rank : ranks) {
            for (Suit suit : suits) {
                hand.add(new Card(suit, rank));
            }
        }

        System.out.println(hand);


    }

}
