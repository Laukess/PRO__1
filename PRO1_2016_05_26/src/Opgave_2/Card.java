package Opgave_2;

/**
 * Created by Laukess on 26-05-2016.
 */
public class Card implements Comparable<Card> {

    private Suit suit;
    private Rank rank;

    public Card(Suit s, Rank r) {
        this.suit = s;
        this.rank = r;
    }

//    @Override
//    public int compareTo(Card other) {
//        if (getValue(this.rank) == getValue(other.rank)) {
//            return getValue(other.suit) - getValue(this.suit);
//        } else {
//            return getValue(other.rank) - getValue(this.rank);
//        }
//    }

    @Override
    public int compareTo(Card other) {
        if (this.rank.ordinal() == other.rank.ordinal()) {
            return other.suit.ordinal() - this.suit.ordinal();
        } else {
            return other.rank.ordinal() - this.rank.ordinal();
        }
    }

    public int getValue(Rank rank) {
        switch (rank) {
            case TWO:
                return 1;
            case THREE:
                return 2;
            case FOUR:
                return 3;
            case FIVE:
                return 4;
            case SIX:
                return 5;
            case SEVEN:
                return 6;
            case EIGHT:
                return 7;
            case NINE:
                return 8;
            case TEN:
                return 9;
            case JACK:
                return 10;
            case QUEEN:
                return 11;
            case KING:
                return 12;
            case ACE:
                return 13;
        }
        return -1;
    }

    public int getValue(Suit suit) {
        switch (suit) {
            case DIAMOND:
                return 1;
            case CLUB:
                return 2;
            case HEART:
                return 3;
            case SPADE:
                return 4;
        }
        return -1;
    }

    @Override
    public String toString() {
        return this.suit + " " + this.rank;
    }
}
