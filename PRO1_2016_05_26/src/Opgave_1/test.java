package Opgave_1;

import java.util.ArrayList;

/**
 * Created by Laukess on 26-05-2016.
 */
public class test {

    public static void main(String[] args) {

        Race puddle = Race.PUDDLE;
        Race bokser = Race.BOKSER;
        Race terrier = Race.TERRIER;

        ArrayList<Hund> hunde = new ArrayList<>();
        hunde.add(new Hund("hund1", false, 1200, puddle));
        hunde.add(new Hund("hund1", false, 1200, bokser));
        hunde.add(new Hund("hund1", false, 1000, terrier));
        hunde.add(new Hund("hund1", false, 1000, terrier));
        hunde.add(new Hund("hund1", false, 1000, terrier));

        System.out.println(samletPris(hunde, Race.TERRIER));
    }




    public static int samletPris(ArrayList<Hund> hunde, Race race) {
        int price = 0;

        for (Hund hund : hunde) {
            if (hund.getRace() == race) {
                price += hund.getPris();
            }
        }

        return price;
    }

}
