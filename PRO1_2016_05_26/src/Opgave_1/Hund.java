package Opgave_1;

/**
 * Created by Laukess on 26-05-2016.
 */
public class Hund {

    private String navn;
    private boolean stamtavle;
    private int pris;
    private Race race;

    public Hund(String navn, boolean stamtavle, int pris, Race race) {
        this.navn = navn;
        this.stamtavle = stamtavle;
        this.pris = pris;
        this.race = race;
    }

    public int getPris() {
        return this.pris;
    }

    public Race getRace() {
        return this.race;
    }

}
