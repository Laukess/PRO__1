package Opgave_3;

/**
 * Created by Laukess on 26-05-2016.
 */
public class Deltagelse {

    private boolean afbud;
    private String begrundese;

    private Spiller spiller;
    private Kamp kamp;

    public Deltagelse(boolean afbud, String begrundese, Spiller spiller) {
        this.afbud = afbud;
        this.begrundese = begrundese;
        this.spiller = spiller;
    }

    public boolean isAfbud() {
        return afbud;
    }

    public String getBegrundese() {
        return begrundese;
    }

    public Spiller getSpiller() {
        return spiller;
    }

    public Kamp getKamp() {
        return kamp;
    }

    public void setKamp(Kamp kamp) {
        this.kamp = kamp;
    }
}
