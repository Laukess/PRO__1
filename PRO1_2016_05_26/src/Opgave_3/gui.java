package Opgave_3;

import javafx.application.Application;
import javafx.geometry.*;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class gui extends Application {

    private ListView lvwKampe;

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) {

        Service.createSomeObjects();

        stage.setTitle("");
        GridPane pane = new GridPane();
        this.initContent(pane);

        Scene scene = new Scene(pane);
        stage.setScene(scene);
        stage.show();
    }

    // ------------------------------------------------------------------------

    private void initContent(GridPane pane) {
        pane.setGridLinesVisible(false);
        pane.setPadding(new Insets(20));
        pane.setHgap(10);
        pane.setVgap(10);

        Label lblName = new Label("Kampe:");
        pane.add(lblName, 0, 0);

        lvwKampe = new ListView();
        pane.add(lvwKampe, 0, 1, 1, 5);
        updateList();

        Label lblSted = new Label("Sted");
        pane.add(lblSted, 1, 0, 1, 1);

        TextField txfSted = new TextField();
        pane.add(txfSted, 2, 0, 2, 1);

        Label lblDato = new Label("SpilleDato");
        pane.add(lblDato, 1, 1, 1, 1);

        TextField txfDato = new TextField();
        pane.add(txfDato, 2, 1, 2, 1);

        Label lblTid = new Label("SpilleTid");
        pane.add(lblTid, 1, 2, 1, 1);

        TextField txfTid = new TextField();
        pane.add(txfTid, 2, 2, 2, 1);

        Button btnOpret = new Button("Opret");
        pane.add(btnOpret, 2, 3);

        Button btnUpdate = new Button("Updater");
        pane.add(btnUpdate, 3, 3);

        Button btnCreateFile = new Button("Lav Fil");
        pane.add(btnCreateFile, 2, 4);

    }

    private void updateList() {
        lvwKampe.getItems().setAll(Service.getAlleKampe());
    }
}
