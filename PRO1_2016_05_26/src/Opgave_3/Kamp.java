package Opgave_3;

import java.io.File;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Laukess on 26-05-2016.
 */
public class Kamp implements Comparable<Kamp> {

    private String sted;
    private LocalDate dato;
    private LocalTime tid;

    private ArrayList<Deltagelse> deltagelser = new ArrayList<>();


    public Kamp(String sted, LocalDate dato, LocalTime tid) {
        this.sted = sted;
        this.dato = dato;
        this.tid = tid;
    }

    public String getSted() {
        return sted;
    }

    public LocalDate getDato() {
        return dato;
    }

    public LocalTime getTid() {
        return tid;
    }

    public ArrayList<Deltagelse> getDeltagelser() {
        return deltagelser;
    }

    public void setDeltagelser(ArrayList<Deltagelse> deltagelser) {
        this.deltagelser = deltagelser;
    }

    public void createDeltagelse(Deltagelse deltagelse) {
        deltagelser.add(deltagelse);
    }

    public Deltagelse createDeltagelse(boolean afbud, String begrundelse, Spiller spiller) {

        try {
            Deltagelse d = new Deltagelse(afbud, begrundelse, spiller);
            deltagelser.add(d);
            return d;
        } catch (Exception e) {
            return null;
        }
    }

    public void addDeltagelse(Deltagelse deltagelse) {
        this.deltagelser.add(deltagelse);
    }

    public boolean removeDeltagelse(Deltagelse deltagelse) {
        return deltagelser.remove(deltagelse);
    }

    public ArrayList<String> afbud() {
        ArrayList<String> text = new ArrayList<>();

        for (Deltagelse deltagelse : deltagelser) {
            if (deltagelse.isAfbud()) {
                text.add(deltagelse.getSpiller() + ": " + deltagelse.getBegrundese());
            }
        }
        return text;
    }

    @Override
    public int compareTo(Kamp o) {
        if (this.getDato() == o.getDato()) {

            if (this.getTid() == this.getTid()) {
                return this.getSted().compareTo(o.getSted());
            }

            else if(this.getTid().isBefore(o.getTid())) {
                return -1;
            } else {
                return 1;
            }
        } else if (this.getDato().isBefore(o.getDato())) {
            return -1;
        } else {
            return 1;
        }
    }

    public void spillerHonorar(String tekstFil) {
        try {
            File outputFile = new File(tekstFil + ".txt");
            PrintWriter out = new PrintWriter(outputFile);

            out.println("test");

            for (Deltagelse deltagelse : deltagelser) {
                if (!deltagelse.isAfbud()) {
                    out.println(deltagelse.getSpiller().getNavn() + "\t" + deltagelse.getSpiller().kampHonorar());
                }
            }
            out.close();

        } catch (Exception e) {
            System.out.println("Exception: " + e.getMessage());
        }

    }

    @Override
    public String toString() {
        return this.sted + "\t" + this.dato + "\t" + this.tid;
    }
}
