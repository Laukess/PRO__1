package Opgave_3;

import java.awt.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

/**
 * Created by Laukess on 26-05-2016.
 */
public class Service {

    public static void createSomeObjects() {
        Spiller s1 = createSpiller("Jane Jensen", 1999);
        Spiller s2 = createSpiller("Lene Hansen", 2000);
        Spiller s3 = createSpiller("Mette Petersen", 1999);

        ProfSpiller ps1 = createProfSpiller("Sofia Kjeldsen", 1999, 50);
        ProfSpiller ps2 = createProfSpiller("Maria Nielsen", 2000, 55);

        Kamp k1 = createKamp("Herning", LocalDate.of(2015, 01, 26), LocalTime.of(10, 30));
        Kamp k2 = createKamp("Ikast", LocalDate.of(2015, 01, 27), LocalTime.of(13, 30));

        k1.createDeltagelse(true, "Moster Oda har foedselsdag", s1);
        k1.createDeltagelse(false, "", s2);
        k1.createDeltagelse(false, "", s3);
        k1.createDeltagelse(false, "", ps1);
        k1.createDeltagelse(false, "", ps2);

        k2.createDeltagelse(false, "", s1);
        k2.createDeltagelse(false, "", s2);
        k2.createDeltagelse(false, "", s3);
        k2.createDeltagelse(true, "Daarlig form", ps1);
        k2.createDeltagelse(false, "", ps2);
    }

    public static Spiller createSpiller(String navn, int aargang) {
        Spiller s = new Spiller(navn, aargang);
        Storage.addSpiller(s);
        return s;
    }

    public static ProfSpiller createProfSpiller(String navn, int aargang, double honorar) {
        ProfSpiller ps = new ProfSpiller(navn, aargang, honorar);
        Storage.addSpiller(ps);
        return ps;
    }

    public static Kamp createKamp(String sted, LocalDate dato, LocalTime tid) {
        Kamp k = new Kamp(sted, dato, tid);
        Storage.addKamp(k);
        return k;
    }

//  Bliver den ikke kun created gennem Kamp???
    public static Deltagelse createDeltagelse(Kamp kamp, boolean afbud, String begrundese, Spiller spiller) {
        Deltagelse d = kamp.createDeltagelse(afbud, begrundese, spiller);
        spiller.addDeltagelse(d);
        return d;
    }

    /**
     * Opdaterer sammenhængen mellem spiller og deltagelse så de
     * linker til hinanden
     * Precondition: spiller != null og deltagelse != null
     */
    public static void updateSpillerDeltagelse(Spiller spiller,
                                               Deltagelse deltagelse) {

        spiller.addDeltagelse(deltagelse);
//      deltagelse.setSpillec ????? den er allerede bundet med spiller naar den er created.
    }

    public static ArrayList<Kamp> alleKampe(ArrayList<Kamp> list1,
                                            ArrayList<Kamp> list2) {

        int i1 = 0;
        int i2 = 0;

        ArrayList<Kamp> alleKampe = new ArrayList<>();

        while (i1 < list1.size() || i2 < list2.size()) {
            if (list1.get(i1).compareTo(list2.get(i2)) < 0) {
                alleKampe.add(list1.get(i1));
                i1++;
            } else if (list1.get(i1).compareTo(list2.get(i2)) > 0) {
                alleKampe.add(list2.get(i2));
                i2++;
            } else {
                alleKampe.add(list1.get(i1));
                alleKampe.add(list2.get(i2));
                i1++;
                i2++;
            }
        }

        while (i1 < list1.size()) {
            alleKampe.add(list1.get(i1));
            i1++;
        }

        while (i2 < list2.size()) {
            alleKampe.add(list2.get(i2));
            i2++;
        }

        return alleKampe;

    }


    public static ArrayList<Kamp> getAlleKampe() {
        return Storage.getKampe();
    }
}
