package Opgave_3;

/**
 * Created by Laukess on 26-05-2016.
 */
public class ProfSpiller extends Spiller {

    private double kampHonorar;

    public ProfSpiller(String navn, int aargang, double kampHonorar) {
        super(navn, aargang);
        this.kampHonorar = kampHonorar;
    }

    @Override
    public double kampHonorar() {
        int afbud = 0;
        double afbudIProcent = 0;
        for (Deltagelse deltagelse : getDeltagelser()) {
            if (!deltagelse.isAfbud()) {
                afbud++;
            }
        }

        if (getDeltagelser().size() == 0) {
            return kampHonorar;
        } else {
            afbudIProcent = afbud / getDeltagelser().size();
            return (1.0 - afbudIProcent) * kampHonorar;
        }

    }



}
