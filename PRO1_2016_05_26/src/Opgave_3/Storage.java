package Opgave_3;

import java.util.ArrayList;

/**
 * Created by Laukess on 26-05-2016.
 */
public class Storage {

    private static ArrayList<Kamp> kampe = new ArrayList<>();
    private static ArrayList<Spiller> spillere = new ArrayList<>();

    public static void addKamp(Kamp kamp) {
        kampe.add(kamp);
    }

    public static void addSpiller(Spiller spiller) {
        spillere.add(spiller);
    }

    public static ArrayList<Kamp> getKampe() {
        return kampe;
    }

    public static ArrayList<Spiller> getSpillere() {
        return spillere;
    }

}
