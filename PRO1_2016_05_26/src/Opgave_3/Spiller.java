package Opgave_3;

import java.util.ArrayList;

/**
 * Created by Laukess on 26-05-2016.
 */
public class Spiller {

    private String navn;
    private int aargang;

    private ArrayList<Deltagelse> deltagelser = new ArrayList<>();

    public Spiller(String navn, int aargang) {
        this.navn = navn;
        this.aargang = aargang;
    }

    public String getNavn() {
        return navn;
    }

    public int getAargang() {
        return aargang;
    }

    public ArrayList<Deltagelse> getDeltagelser() {
        return deltagelser;
    }

    public void addDeltagelse(Deltagelse deltagelse) {
        deltagelser.add(deltagelse);
    }

    public void removeDeltagelse(Deltagelse deltagelse) {
        deltagelser.remove(deltagelse);
    }

    public double kampHonorar() {
        int multiplier = 10;
        int kampDeltagelser = 0;
        for (Deltagelse deltagelse : deltagelser) {
            if (!deltagelse.isAfbud()) {
                kampDeltagelser++;
            }
        }

        return kampDeltagelser * multiplier;
    }
}
