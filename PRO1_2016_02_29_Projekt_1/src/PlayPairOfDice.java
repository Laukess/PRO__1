import java.util.Scanner;

/**
 * Created by lukas on 29-02-2016.
 */
public class PlayPairOfDice {

    Scanner s;
    PairOfDices pod;

    public PlayPairOfDice() {
        s = new Scanner(System.in);
        pod = new PairOfDices();
    }

    public void startGame() {
        System.out.println("Velkommen til spillet KAST terning");
        boolean finished = false;

        while (!finished) {
            System.out.println("Vil du rulle med terningerne? Angiv Ja eller Nej: ");
            String proceedWithGame = s.nextLine();
            if (proceedWithGame.equalsIgnoreCase("nej")) {
                finished = true;
            } else {
                pod.rollBothDices();

                for(int roll : pod.getFaceValues()) {
                    System.out.println("Du rullede " + roll);
                }
                System.out.println("total: " + pod.sumOfDices());

            }
        }
        gameOver();
    }


    private void gameOver() {
        int[] rolls = this.pod.getRollArray();
        System.out.println("Tak for spillet du kastede " + pod.getNrOfRolls() + " " + "gange.");
        System.out.println("dit hoejeste roll var " + pod.getHighestRoll() + ".");
        System.out.println("du rullede " + pod.getPairs() + " par.");
        System.out.println("du rullede " + rolls[5] + " sekser(e).");

        System.out.println("\nDu rullede: ");
        for (int i = 1 ; i <= rolls.length ; i++) {
            System.out.println(i + ": " + rolls[i - 1] + " gange.");
        }

        s.close();
    }

}
