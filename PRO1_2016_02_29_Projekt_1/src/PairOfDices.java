/**
 * This class models one pair of dices. This is useful for games where you have to
 * throw two dice at once.
 */
public class PairOfDices {
    /**
     * The first die in the pair.
     */
	private Die die1;
	/**
	 * The second die in the pair.
	 */
	private Die die2;

    private int nrOfRolls;
    private int numOfsixes;
    private int numberOfOneToFive;
    private int pairs;
    private int highestRoll;
    private int[] rolls = {0, 0, 0, 0, 0, 0};

	public PairOfDices(int sides) {
		this.die1 = new Die(sides);
		this.die2 = new Die(sides);

        this.pairs = 0;
        this.nrOfRolls = 0;
        this.numOfsixes = 0;
        this.numberOfOneToFive = 0;
        this.highestRoll = 0;
	}

	public PairOfDices() {
		this(6);
	}

    public void rollBothDices() {
        this.nrOfRolls++;

        this.die2.roll();
        this.die1.roll();

        /*
        if (this.die1.getFaceValue() == 6) {
            this.numOfsixes++;
        } else {
            this.numberOfOneToFive++;
        }

        if (this.die2.getFaceValue() == 6) {
            this.numOfsixes++;
        } else {
            this.numberOfOneToFive++;
        }
        */
        if (this.die2.getFaceValue() == this.die1.getFaceValue()) {
            this.pairs++;
        }

        if (this.die1.getFaceValue() + this.die2.getFaceValue() > this.highestRoll) {
            this.highestRoll = this.die1.getFaceValue() + this.die2.getFaceValue();
        }

        this.rolls[this.die1.getFaceValue() - 1]++;
        this.rolls[this.die2.getFaceValue() - 1]++;
    }

    public int[] getRollArray() {
        return this.rolls;
    }

    public int sumOfDices() {
        return this.die1.getFaceValue() + this.die2.getFaceValue();
    }

    public int getPairs() {
        return this.pairs;
    }

    public int getHighestRoll() {
        return this.highestRoll;
    }

    public void resetPairOfDice() {
        this.pairs = 0;
        this.nrOfRolls = 0;
        this.numOfsixes = 0;
        this.numberOfOneToFive = 0;
        this.highestRoll = 0;

        this.die1.setFaceValue(1);
        this.die2.setFaceValue(1);
    }

    public int getNrOfRolls() {
        return nrOfRolls;
    }

    public int getNumOfsixes() {
        return numOfsixes;
    }

    public int getNumberOfOneToFive() {
        return numberOfOneToFive;
    }

    public int[] getFaceValues() {
        int[] result = {this.die1.getFaceValue(), this.die2.getFaceValue()};
        return result;
    }

}
