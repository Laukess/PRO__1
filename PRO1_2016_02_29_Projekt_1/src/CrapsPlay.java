import java.util.Scanner;

/**
 * Created by lukas on 02-03-2016.
 */
public class CrapsPlay {

    private Die die1;
    private Die die2;
    // 0 = continue, 1=win, 2 = lose;
    private int gameState;
    private int firstRoll;
    Scanner s;

    public CrapsPlay() {
        die1 = new Die();
        die2 = new Die();
        s = new Scanner(System.in);

        gameState = 0;
        firstRoll = 0;
    }

    private void WelcomeToGame() {
        System.out.println("Welkommen til.... trummevirvel... CRAPS");
    }

    private void gameOver() {
        if (gameState == 1) {
            System.out.println("du har vundet");
        } else if(gameState == 2) {
            System.out.println("du har tabt");
        }
    }

    public void takeTurn() {
        die1.roll();
        die2.roll();
        int totalRoll = die1.getFaceValue() + die2.getFaceValue();

        System.out.println("Du rullede " + totalRoll);

        // first roll
        if(firstRoll == 0) {

            if(totalRoll == 7 || totalRoll == 11) {
                this.gameState = 1;
            } else if(totalRoll == 2 || totalRoll == 3 || totalRoll == 12) {
                this.gameState = 2;
            } else {
                this.firstRoll = totalRoll;
                System.out.println("Dine points er " + firstRoll);
            }

        } else {
            if (totalRoll == this.firstRoll) {
                this.gameState = 1;
            } else if (totalRoll == 7) {
                this.gameState = 2;
            } else {
                this.gameState = 0;
            }
        }
    }

    public void startGame() {
        WelcomeToGame();
        while(gameState == 0) {
            System.out.println("Vil du rulle med terningerne? Angiv Ja eller Nej: ");
            String proceedWithGame = s.nextLine();
            if (proceedWithGame.equalsIgnoreCase("nej")) {
                gameState = 2;
            } else {
                takeTurn();
            }
        }
        gameOver();
    }

}
