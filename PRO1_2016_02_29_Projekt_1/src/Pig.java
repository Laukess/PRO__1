import java.util.Scanner;

/**
 * Created by lukas on 02-03-2016.
 */
public class Pig {

    private Die die;
    private int scoreP1;
    private int scoreP2;
    private int gameState;
    private int tempScore;
    private int playerTurn;
    private Scanner s;
    private int pointsToWin;

    public Pig(int pointsToWin) {
        die = new Die();
        scoreP1 = 0;
        scoreP2 = 0;
        gameState = 0;
        tempScore = 0;
        playerTurn = 1;
        s = new Scanner(System.in);
        this.pointsToWin = pointsToWin;
    }

    public Pig() {
        this(100);
    }

    public void WelcomeToGame() {
        System.out.println("Welkommen til.... trummevirvel... PIG");
    }

    public void GameOver() {
        if (gameState == 1) {
            System.out.println("Spiller 1 har vundet");
        } else if(gameState == 2) {
            System.out.println("Spiller 2 har vundet");
        }
    }

    public void TakeTurn() {
        die.roll();
        int roll = die.getFaceValue();

        System.out.println("du slog " + roll);

        if (roll == 1) {
            this.playerTurn = -playerTurn;
            this.tempScore = 0;
        } else {
            this.tempScore += roll;

            if (this.tempScore + scoreP1 >= this.pointsToWin) {
                this.gameState = 1;
            } else if (this.tempScore + scoreP2 >= this.pointsToWin) {
                this.gameState = 2;
            }
        }
    }

    public void StartGame() {
        WelcomeToGame();
        while(gameState == 0) {
            String playerName = playerTurn == 1 ? "Player 1" : "Player 2";
            System.out.println(playerName +" Vil du rulle med terningerne? Angiv Ja eller Nej: ");
            String proceedWithGame = s.nextLine();
            if (proceedWithGame.equalsIgnoreCase("nej")) {
                playerTurn = -playerTurn;

                if (playerTurn == 1) {
                    this.scoreP1 += this.tempScore;
                    System.out.println("Din samlede score er: " + scoreP1);
                } else {
                    this.scoreP2 += this.tempScore;
                    System.out.println("Din samlede score er: " + scoreP2);
                }
                this.tempScore = 0;
            } else {
                TakeTurn();
            }
        }
        GameOver();
    }


}
