
public class P1_18 {
    public static void main(String[] args) {
        P1_18 opg = new P1_18();
        System.out.println(opg.IsTheTrainCheaper(100, 120));
    }

    public boolean IsTheTrainCheaper(int distance, int TrainTicketPrice) {
        float pricePrLiterGas = 10; //10kr
        float efficiency = 12; // 12km per liter
        float maintenancePrKM = 0.15f; //15 oere per km

        float pricePerKM = (pricePrLiterGas / efficiency) + maintenancePrKM;

        System.out.println("PPK: " + pricePerKM);
        System.out.println("Car: " + (pricePerKM * distance));
        System.out.println("Train: " + TrainTicketPrice);

        /*
        if (pricePerKM * distance > TrainTicketPrice) {
            return true;
        } else {
            return false;
        }
        */

        return pricePerKM * distance > TrainTicketPrice;

    }
}
