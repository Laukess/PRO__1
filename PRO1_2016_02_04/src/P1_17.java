import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class P1_17 {
    public static void main(String[] args) throws Exception {
        //URL imageLocation = new URL("http://horstmann.com/java4everyone/duke.gif");
        URL imageLocation = new URL("https://pbs.twimg.com/profile_images/1267003859/fyrst-walther.jpg");
        JOptionPane.showMessageDialog(null, "Kommer tid kommer baad!", "Title", JOptionPane.PLAIN_MESSAGE, new ImageIcon(imageLocation));
    }
}
