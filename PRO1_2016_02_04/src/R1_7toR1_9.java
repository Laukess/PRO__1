
public class R1_7toR1_9 {
    public static void main(String[] args) {
        System.out.println("R1.7:");
        System.out.println("----------------------------------------------------");

        System.out.println("39 + 3");
        System.out.println(42);

        System.out.println("----------------------------------------------------\n\n");


        System.out.println("R1.8:");
        System.out.println("----------------------------------------------------");

        System.out.println("HelloWorld");

        System.out.println("----------------------------------------------------\n\n");


        System.out.println("R1.9:");
        System.out.println("----------------------------------------------------");

        System.out.println("System.out.print tager kun 1 parameter, men der er givet 2 strings i eksemplet.");

        System.out.println("----------------------------------------------------\n\n");

    }
}
