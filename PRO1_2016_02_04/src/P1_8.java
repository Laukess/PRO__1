/**
 * Created by lukas on 06-02-2016.
 */
public class P1_8 {
    public static void main(String[] args) {
        System.out.println("+-----------------+");
        System.out.println("|&&&         ::   |");
        System.out.println("|&&&         ::   |");
        System.out.println("|&&&              |");
        System.out.println("|                 |");
        System.out.println("| ;;     ***      |");
        System.out.println("| ;;     ***      |");
        System.out.println("+-----------------+");
    }
}
