import java.math.BigInteger;

/**
 * Created by lukas on 06-02-2016.
 */
public class P1_20 {
    public static void main(String[] args) {
        int counter = 3;
        float pi = 1f - (1f/counter);
        boolean isPositive = true;

        System.out.println(pi*4);

        while(pi*4 < 3.1415920 || pi*4 > 3.1415929 ) {

            counter += 2;
            if (isPositive) {
                pi += 1f/counter;
                isPositive = false;
            } else {
                pi -= 1f/counter;
                isPositive = true;
            }

            System.out.println("pi = " + pi * 4);
        }
    }
}
