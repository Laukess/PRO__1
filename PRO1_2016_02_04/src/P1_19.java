/**
 * Created by lukas on 06-02-2016.
 */
public class P1_19 {
    public static void main(String[] args) {
        float distanceToWork = 75f;
        int workdays = 22;
        float startDis = 153277f;
        float endDis = 159277f;

        float deltaDis = endDis - startDis;

        float workDistanceTotal = (distanceToWork * 2 * workdays);

        System.out.println("work usage: " + workDistanceTotal);
        System.out.println("Personal usage: " + (deltaDis - workDistanceTotal));
        System.out.println("total usage: " + (workDistanceTotal + (deltaDis - workDistanceTotal)) +
                " and " + (endDis - startDis));
    }
}
