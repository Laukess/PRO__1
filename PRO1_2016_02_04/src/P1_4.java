/**
 * Created by lukas on 05-02-2016.
 */
public class P1_4 {
    public static void main(String[] args) {
        int balance = 1000;

        for(int i = 1 ; i <= 3 ; i++) {
            balance *= 1.05;
            System.out.println("after " + i + " year(s): " + balance);
        }
    }
}
