package Opgave_3;

import java.time.LocalDateTime;

public class FlightTest {

    public static void main(String[] args) {
        Flight flight = new Flight("AY025", "Seoul");

        // par timer over MN
        flight.setDepartDate(LocalDateTime.of(2016, 12, 24, 22, 0, 0));
        flight.setArrivalDate(LocalDateTime.of(2016, 12, 25, 2, 0, 0));

        System.out.println("hours -> " + flight.flightDurationInHours());
        System.out.println("is midnight flight -> " + flight.midnightFlight());

        // lang rejse der ikke er MN
        flight.setDepartDate(LocalDateTime.of(2016, 12, 24, 2, 0, 0));
        flight.setArrivalDate(LocalDateTime.of(2016, 12, 24, 23, 0, 0));

        System.out.println("hours -> " + flight.flightDurationInHours());
        System.out.println("is midnight flight -> " + flight.midnightFlight());

        // lang rejse der er MN
        flight.setDepartDate(LocalDateTime.of(2016, 12, 24, 2, 0, 0));
        flight.setArrivalDate(LocalDateTime.of(2016, 12, 25, 23, 0, 0));

        System.out.println("hours -> " + flight.flightDurationInHours());
        System.out.println("is midnight flight -> " + flight.midnightFlight());

        // lander MN
        flight.setDepartDate(LocalDateTime.of(2016, 12, 24, 2, 0, 0));
        flight.setArrivalDate(LocalDateTime.of(2016, 12, 24, 0, 0, 0));

        System.out.println("hours -> " + flight.flightDurationInHours());
        System.out.println("is midnight flight -> " + flight.midnightFlight());

        // takeoff MN
        flight.setDepartDate(LocalDateTime.of(2016, 12, 24, 0, 0, 0));
        flight.setArrivalDate(LocalDateTime.of(2016, 12, 25, 3, 0, 0));

        System.out.println("hours -> " + flight.flightDurationInHours());
        System.out.println("is midnight flight -> " + flight.midnightFlight());

        flight.createPassenger("Arnold", 31);
        flight.createPassenger("Bruce", 59);
        flight.createPassenger("Dolph", 24);
        flight.createPassenger("Linda", 59);
        flight.createPassenger("Jennifer", 65);

        System.out.println(flight.averageAgeOfPassengers());
    }

}
