package Opgave_2;

import java.time.LocalDate;

/**
 * Created by lukas on 04-04-2016.
 */
public class Rental {

    private int number;
    private int days;
    private double price;
    private LocalDate startDate;

    public Rental(int number, int days, double price, LocalDate startDate) {
        this.number = number;
        this.days = days;
        this.price = price;
        this.startDate = startDate;
    }

    public double getPricePrDay() {
        return this.price;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public int getDays() {
        return days;
    }

    public LocalDate getStartDate() {
        return this.startDate;
    }

    public LocalDate getEndDate() {
        //LocalDate endDate = LocalDate.of(this.startDate.getYear(), this.startDate.getMonth(), this.startDate.getDayOfMonth());
        LocalDate startDate = LocalDate.from(this.startDate);

        return startDate.plusDays(this.days);
    }

    public double getTotalPrice() {
        return price * days;
    }

}
