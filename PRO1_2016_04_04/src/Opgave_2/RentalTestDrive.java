package Opgave_2;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

/**
 * Created by lukas on 04-04-2016.
 */
public class RentalTestDrive {


    public static void main(String[] args) {
        LocalDate today = LocalDate.now();
        LocalDate nextMonthDate = LocalDate.of(today.getYear(), today.plusMonths(1).getMonthValue(), 1);
        LocalDate tenMonthsFromToday = LocalDate.from(today).plusMonths(10);

        Rental rental = new Rental(1, 14, 100, nextMonthDate);
        Rental rental2 = new Rental(1, 14, 100, tenMonthsFromToday);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");

        System.out.printf("Total Price after %d days: %.2f \n", rental.getDays(), rental.getTotalPrice());
        System.out.printf("Total Price after %d days: %.2f \n", rental2.getDays(), rental2.getTotalPrice());

        System.out.println(rental.getStartDate().minusDays(1).format(formatter));
        System.out.println(rental.getEndDate().format(formatter));

        System.out.println(rental2.getEndDate().format(formatter));
        System.out.println(rental2.getStartDate().minusDays(1).format(formatter));

        Period per = rental.getStartDate().until(rental2.getStartDate());
        long totalDays = rental.getStartDate().until(rental2.getStartDate(), ChronoUnit.DAYS);
        System.out.printf("Der er %d aar, %d maaneder og %d dage mellem de 2 udlejninger\n", per.getYears(), per.getMonths(), per.getDays());
        System.out.println(totalDays);


    }

}
