package Opgave_4;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by lukas on 05-04-2016.
 */
public class SwimerTestDrive {

    public static void main(String[] args) {
        Double[] times2 = new Double[]{25.0, 31.0, 56.0, 22.0, 56.0, 66.0};
        ArrayList<Double> times = new ArrayList<>(Arrays.asList(times2));

        Swimmer swimmer = new Swimmer("peter", "FFS_A/S", times, LocalDate.of(1989,7, 22));
        System.out.println(swimmer.bedsteTid());
        System.out.println(swimmer.gennemsnitAfTid());
        System.out.println(swimmer.snitUdenDårligste());
    }

}
