package Opgave_4;

/**
 * Created by lukas on 05-04-2016.
 */

import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Modellerer en svømmer
 */
public class Swimmer {
    private String name;
    private String club;
    private LocalDate birthday;
    private ArrayList<Double> times;

    /**
     * Initialiserer en ny svømmer med navn, fødselsdag, klub
     * og tider.
     */
    public Swimmer(String name, String club, ArrayList<Double> times, LocalDate birthday) {
        this.name = name;
        this.club = club;
        this.birthday = birthday;
        this.times = times;
    }

    /**
     * Returnerer svømmerens navn.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Returnerer svømmerens årgang.
     */
    public int getBirthYear() {
        return birthday.getYear();
    }

    /**
     * Returnerer svømmerens klub.
     */
    public String getClub() {
        return this.club;
    }

    /**
     * Registrerer svømmerens klub
     * @param club
     */
    public void setClub(String club) {
        this.club = club;
    }

    /**
     * Returnerer den hurtigste tid svømmeren har opnået
     */
    public double bedsteTid(){

        if (times.size() == 0) {
            return 0;
        } else {
            double min = times.get(0);
            for(double time : times) {
                if (time < min) {
                    min = time;
                }
            }
            return min;
        }
    }
    /**
     * Returnerer gennemsnittet af de tider svømmeren har
     * opnået
     */
    public double gennemsnitAfTid(){
        double sum = 0.0;
        for (double time : times) {
            sum += time;
        }

        if (sum == 0) {
            return 0;
        } else {
            return sum/times.size();
        }

    }
    /**
     * Returnerer gennemsnittet af de tider svømmeren har
     * opnået idet den langsomste tid ikke er medregnet
     */
    public double snitUdenDårligste(){
        // tjek lige logikken. der er muligvis en fejl. min til max
        double sum = 0;
        double max = Double.MIN_VALUE;

        if (times.size() == 0) {
            return 0;
        } else {
            for (double time : times) {
                sum += time;
                if (time > max) {
                    max = time;
                }
            }
            return (sum - max) / times.size();
        }

    }
}