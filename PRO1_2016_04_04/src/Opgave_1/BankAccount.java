package Opgave_1;

/**
 * Created by lukas on 04-04-2016.
 */
public class BankAccount {
    private double balance;
    private int id;
    static int ClassId = 0;

    /**
     * Initializes the object with an initial balance.
     * pre: initialBalance >= 0
     * @param initialBalance The initial balance
     * */
    public BankAccount(double initialBalance) {
        balance = initialBalance;
        BankAccount.ClassId++;
        this.id = BankAccount.ClassId;
    }

    /**
     * adds an amount to the account balance
     * pre: amount >= 0
     * @param amount the amount to deposit
     * */
    public void deposit(double amount) {
        balance += amount;
    }

    /**
     * withdraws an amount from the account balance
     * pre: amount >= 0
     * @param amount the amount to withdraw
     * */
    public void withdraw(double amount) {
        balance -= amount;
    }


    /**
     * gets the account balance
     * @return balance returns the account balance
     * */
    public double getBalance() {
        return balance;
    }


    /**
     * returns the objects unique id.
     * @return id returns the objects unique id
     * */
    public int getId() {
        return this.id;
    }
}
