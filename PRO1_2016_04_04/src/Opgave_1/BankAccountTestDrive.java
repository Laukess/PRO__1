package Opgave_1;

/**
 * Created by lukas on 04-04-2016.
 */
public class BankAccountTestDrive {

    public static void main(String[] args) {
        BankAccount acc = new BankAccount(0);
        acc.deposit(500);
        System.out.println("after depositing 500: " + acc.getBalance());
        acc.withdraw(300);
        System.out.println("after withdrawing 300 " + acc.getBalance());
        System.out.println("the current objects unique id " + acc.getId());
    }


}
