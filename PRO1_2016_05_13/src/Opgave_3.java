import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

/**
 * Created by Laukess on 17-05-2016.
 */
public class Opgave_3 {

    public static void main(String[] args) {

        Customer c1 = new Customer("Parker", "Peter", 44);
        Customer c2 = new Customer("Carmack", "John", 55);
        Customer c3 = new Customer("Balish", "Pyter", 34);
        Customer c4 = new Customer("Clegane", "Gregor", 66);
        Customer c5 = new Customer("Clegane", "Sandor", 65);
        Customer c6 = new Customer("Beratheon", "Jofry", 33);

        ArrayList<Customer> customers = new ArrayList<>();
        customers.add(c1);
        customers.add(c2);
        customers.add(c3);
        customers.add(c4);
        customers.add(c5);
        customers.add(c6);
        Collections.sort(customers);

        Customer[] badCustomers = new Customer[3];
        badCustomers[0] = c4;
        badCustomers[1] = c3;
        badCustomers[2] = c6;
        Arrays.sort(badCustomers);

        System.out.println(goodCustomers(customers, badCustomers));


    }

    /**
     * Returnerer en sorteret ArrayList der indeholder alle
     * customers fra list1 der ikke er i list2
     * Krav: list1 og list2 er sorterede og indeholder ikke tomme
     * pladser
     */
    public static ArrayList<Customer> goodCustomers(
            ArrayList<Customer> list1, Customer[] list2) {

        int i1 = 0;
        int i2 = 0;
        ArrayList<Customer> results = new ArrayList<>();

        while (i1 < list1.size() && i2 < list2.length) {

            if (list1.get(i1).compareTo(list2[i2]) < 0) {
                results.add(list1.get(i1));
                i1++;
            } else if (list2[i2].compareTo(list1.get(i1)) > 0){
                results.add(list2[i2]);
                i2++;
            } else if (list1.get(i1).compareTo(list2[i2]) == 0) {
                i1++;
                i2++;
            }

        }

        while (i1 < list1.size()) {
            results.add(list1.get(i1));
            i1++;
        }


        Collections.sort(results);

        return results;
    }

}