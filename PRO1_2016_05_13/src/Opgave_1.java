import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Laukess on 13-05-2016.
 */
public class Opgave_1 {

    public static void main(String[] args) {

        ArrayList<Customer> c1 = new ArrayList<>();
        c1.add(new Customer("Parker", "Peter", 12));
        c1.add(new Customer("Doe", "John", 22));
        c1.add(new Customer("Carmack", "John", 44));


        ArrayList<Customer> c2 = new ArrayList<>();
        c2.add(new Customer("Baker", "Jane", 12));
        c2.add(new Customer("Clark", "Momxay", 22));
        c2.add(new Customer("Smith", "Tori", 44));

        Collections.sort(c1);
        Collections.sort(c2);


//        fletAlleKunder(c1, c2);
        System.out.println(c1 + " --- " + c2);
        System.out.println(fletAlleKunder(c1, c2));

    }

    /**
     * Returnerer en sorteret ArrayList der indeholder alle
     * kunder fra både list1 og list2
     * Krav: list1 og list2 er sorterede
     */
    public static ArrayList fletAlleKunder(ArrayList<Customer> list1,
                                           ArrayList<Customer> list2) {
        ArrayList<Customer> allCustomers = new ArrayList<>();
        int i1 = 0;
        int i2 = 0;

        while (i1 < list1.size() && i2 < list2.size()) {
            if (list1.get(i1).compareTo(list2.get(i2)) < 0 ) {
                allCustomers.add(list1.get(i1));
                i1++;
            } else if (list1.get(i1).compareTo(list2.get(i2)) > 0 ) {
                allCustomers.add(list2.get(i2));
                i2++;
            } else {
                allCustomers.add(list1.get(i1));
                allCustomers.add(list2.get(i2));
                i1++;
                i2++;
            }
        }

        while (i1 < list1.size() && i2 == list2.size()) {
            allCustomers.add(list1.get(i1));
            i1++;
        }

        while (i2 < list2.size() && i1 == list1.size()) {
            allCustomers.add(list2.get(i2));
            i2++;
        }

        return allCustomers;
    }

}
