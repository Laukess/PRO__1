import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * Created by Laukess on 17-05-2016.
 */
public class Opgave_4 {

    public static void main(String[] args) {

        Customer c1 = new Customer("Parker", "Peter", 44);
        Customer c2 = new Customer("Carmack", "John", 55);
        Customer c3 = new Customer("Balish", "Pyter", 34);
        Customer c4 = new Customer("Clegane", "Gregor", 66);
        Customer c5 = new Customer("Clegane", "Sandor", 65);
        Customer c6 = new Customer("Beratheon", "Jofry", 33);

        ArrayList<Customer> customers = new ArrayList<>();
        customers.add(c1);
        customers.add(c2);
        customers.add(c3);
        customers.add(c4);

        customers.add(c6);

        Collections.sort(customers);
        System.out.println(customers);
        insertCustomer(customers, c5);
        System.out.println(customers);

    }

    /**
     * Indsætter customer i customers.
     * Precondition: Listen customers er sorteret
     * Postcondition: Listen customers er fortsat sorteret, men
     * nu med customer indsat.
     */
    public static void insertCustomer(ArrayList<Customer> customers,
                                      Customer customer) {

        for (int i = 0 ; i < customers.size() ; i++) {
            if (customer.compareTo(customers.get(i)) < 0) {

                customers.add(i, customer);
                return;


            }
        }

    }


}
