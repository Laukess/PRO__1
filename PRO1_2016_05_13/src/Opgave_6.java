/**
 * Created by Laukess on 22-05-2016.
 */
public class Opgave_6 {

    public static void main(String[] args) {
        GameOfLife life = new GameOfLife();
        String[] strings = new String[6];
        strings[0] = "       ";
        strings[1] = "  o    ";
        strings[2] = "   o   ";
        strings[3] = " ooo   ";
        strings[4] = "       ";
        strings[5] = "       ";

        life.initialize(strings);

        // Udregn de første 5 generationer
        for (int i = 0; i < 5; i++) {
            System.out.println("---" + i + "---");
            System.out.println(life.output());
            System.out.println("-------\n");

            life.next();
        }
    }

}
