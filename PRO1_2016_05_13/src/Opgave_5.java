import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

/**
 * Created by Laukess on 17-05-2016.
 */
public class Opgave_5 {

    public static void main(String[] args) {

        Customer c1 = new Customer("Parker", "Peter", 44);
        Customer c2 = new Customer("Carmack", "John", 55);
        Customer c3 = new Customer("Balish", "Pyter", 34);
        Customer c4 = new Customer("Clegane", "Gregor", 66);
        Customer c5 = new Customer("Clegane", "Sandor", 65);
        Customer c6 = new Customer("Beratheon", "Jofry", 33);

        Customer[] customers = new Customer[]{c1, c2, c3, c4, c6};

        Arrays.sort(customers);
        System.out.println(Arrays.toString(customers));
        insertCustomer(customers, c5);
        System.out.println(Arrays.toString(customers));

    }

    /**
     * Indsætter customer i customers.
     * Precondition: Listen customers er sorteret
     * Postcondition: Listen customers er fortsat sorteret, men
     * nu med customer indsat.
     */
    public static void insertCustomer(Customer[] customers,
                                      Customer customer) {


//        ToDo: Hvad sker der hvis det skal saettes ind tilsidst?
        int j = 0;
        Customer[] result = new Customer[customers.length+1];

        for (int i = 0 ; i < customers.length ; i++) {
            if (customer.compareTo(customers[i]) > 0) {
                result[j] = customers[i];
                j++;
            } else if (customer.compareTo(customers[i]) < 0) {
                result[j++] = customer;
                result[j] = customers[i];
            } else {
                result[j++] = customer;
                result[j] = customers[i];
            }
        }

        System.out.println("-------");

        for (Customer c : result) {
            System.out.print(c + " ");
        }

        System.out.println();

        for (Customer c : customers) {
            System.out.print(c + " ");
        }

        System.out.println();
        customers = result;
        for (Customer c : customers) {
            System.out.print(c + " ");
        }

        System.out.println();
        System.out.println("-------");
    }
    
}
