import com.sun.deploy.util.ArrayUtil;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Laukess on 17-05-2016.
 */
public class Opgave_2 {
    public static void main(String[] args) {

        int[] l1 = new int[]{2, 4, 6, 8, 10, 12, 14};
        int[] l2 = new int[]{1, 2, 4, 5, 6, 9, 12, 17};

        System.out.println( Arrays.toString(fællesTal(l1, l2)));

    }


    /**
     * Returnerer et sorteret array der indeholder alle
     * elementer list1 og list2 har til fælles
     * Krav: list1 og list2 er sorterede og indeholder ikke
     * tomme pladser
     */
    public static int[] fællesTal(int[] list1, int[] list2) {

        int i1 = 0;
        int i2 = 0;
        int last = Math.min(list1[0], list2[0]) - 1;

        ArrayList<Integer> buffer = new ArrayList<>();

        while (i1 < list1.length && i2 < list2.length) {

            if (list1[i1] == list2[i2]) {

                if (list1[i1] != last) {
                    buffer.add(list1[i1]);
                    last = list1[i1];
                }
                i1++;
                i2++;

            } else if (list1[i1] < list2[i2]) {
                i1++;
            } else if (list1[i1] > list2[i2]) {
                i2++;
            }

        }
//        Integer[] arr = result.toArray(new Integer[result.size()]);
        int[] result = new int[buffer.size()];
        for (int i = 0 ; i < buffer.size() ; i++) {
            result[i] = buffer.get(i);
        }

        return result;

    }

}
