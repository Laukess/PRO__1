/**
 * Created by Laukess on 22-05-2016.
 */
public class GameOfLife {

    char[][] board = new char[6][7];
    char[][] tempBoard = new char[6][7];

    public void initialize(String[] strings) {
        for (int row = 0 ; row < strings.length ; row++) {
            for (int col = 0 ; col < strings[row].length() ; col++) {
                board[row][col] = strings[row].charAt(col);
            }
        }
    }

    public String output() {
        String result = "";
        for (char[] row : board) {
            for (int i = 0 ; i < row.length ; i++) {
                result += row[i];
            }
            result += "\n";
        }
        return result;
    }

    public void next() {
        int neibourghs;
        for (int row = 0 ; row < board.length ; row++) {
            for (int col = 0 ; col < board[row].length ; col++) {

                neibourghs = getNumOfNeibourghs(board, row, col);
                if (board[row][col] == 'o') {
                    if (neibourghs < 2) {
                        tempBoard[row][col] = ' ';
                    } else if (neibourghs >= 2 && neibourghs <= 3) {
                        tempBoard[row][col] = 'o';
                    } else if (neibourghs > 3) {
                        tempBoard[row][col] = ' ';
                    }
                } else {
                    if (neibourghs == 3) {
                        tempBoard[row][col] = 'o';
                    } else {
                        tempBoard[row][col] = ' ';
                    }
                }
            }
        }

        board = tempBoard;
        tempBoard = new char[6][7];
    }

    public int getNumOfNeibourghs(char[][] board, int row, int col) {
        int top = getNumOfTopNeibourghs(board, row, col);
        int right = getNumOfRightNeibourghs(board, row, col);
        int bottom = getNumOfBottomNeibourghs(board, row, col);
        int left = getNumOfLeftNeibourghs(board, row, col);

//        System.out.println("["+ row + ", " + col +"] = " + (top + right + bottom + left));
        return top + right + bottom + left;
    }

    public int getNumOfTopNeibourghs(char[][] board, int row, int col) {
        int total = 0;

        if (row == 0) {
            return 0;
        }
        if (col == 0) {
            total += board[row-1][col] == 'o' ? 1 : 0;
            total += board[row-1][col+1] == 'o' ? 1 : 0;
        } else if (col == board[row].length - 1) {
            total += board[row-1][col] == 'o' ? 1 : 0;
            total += board[row-1][col-1] == 'o' ? 1 : 0;
        } else {
            total += board[row-1][col-1] == 'o' ? 1 : 0;
            total += board[row-1][col] == 'o' ? 1 : 0;
            total += board[row-1][col+1] == 'o' ? 1 : 0;
        }

        return total;
    };

    public int getNumOfRightNeibourghs(char[][] board, int row, int col) {
        int total = 0;

        if (col == board[row].length - 1) {
            return 0;
        }

        total += board[row][col+1] == 'o' ? 1 : 0;

//        if (row == 0) {
//            total += board[row][col+1] == 'o' ? 1 : 0;
//            total += board[row+1][col+1] == 'o' ? 1 : 0;
//        } else if (row == board.length - 1) {
//            total += board[row][col+1] == 'o' ? 1 : 0;
//            total += board[row-1][col+1] == 'o' ? 1 : 0;
//        } else {
//            total += board[row-1][col+1] == 'o' ? 1 : 0;
//            total += board[row][col+1] == 'o' ? 1 : 0;
//            total += board[row+1][col+1] == 'o' ? 1 : 0;
//        }

        return total;
    };

    public int getNumOfBottomNeibourghs(char[][] board, int row, int col) {
        int total = 0;

        if (row == board.length - 1) {
            return 0;
        }

        if (col == 0) {
            total += board[row+1][col] == 'o' ? 1 : 0;
            total += board[row+1][col+1] == 'o' ? 1 : 0;
        } else if (col == board[row].length - 1) {
            total += board[row+1][col] == 'o' ? 1 : 0;
            total += board[row+1][col-1] == 'o' ? 1 : 0;
        } else {
            total += board[row+1][col-1] == 'o' ? 1 : 0;
            total += board[row+1][col] == 'o' ? 1 : 0;
            total += board[row+1][col+1] == 'o' ? 1 : 0;
        }


        return total;
    };

    public int getNumOfLeftNeibourghs(char[][] board, int row, int col) {
        int total = 0;

        if (col == 0) {
            return 0;
        }

        total += board[row][col-1] == 'o' ? 1 : 0;

//        if (row == 0) {
//            total += board[row][col-1] == 'o' ? 1 : 0;
//            total += board[row+1][col-1] == 'o' ? 1 : 0;
//        } else if (row == board.length - 1) {
//            total += board[row][col-1] == 'o' ? 1 : 0;
//            total += board[row-1][col-1] == 'o' ? 1 : 0;
//        } else {
//            total += board[row+1][col-1] == 'o' ? 1 : 0;
//            total += board[row][col-1] == 'o' ? 1 : 0;
//            total += board[row-1][col-1] == 'o' ? 1 : 0;
//        }

        return total;
    };
}