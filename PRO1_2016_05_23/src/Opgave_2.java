import java.util.Arrays;

/**
 * Created by Laukess on 23-05-2016.
 */
public class Opgave_2 {

    public static void main(String[] args) {

        Customer[] customers = new Customer[]{
                new Customer("Doe", "John", 22),
                new Customer("Snow", "John", 44),
                new Customer("Balish", "Pyter", 33),
                new Customer("Hodor", "Hodor", 44),
                new Customer("Stark", "Ned", 33),
                new Customer("Bolten", "Ramsy", 55),
                null
        };
        Arrays.sort(customers, 0, customers.length-1);
//        Customer[] customers = new Customer[temp.length+1];
//        Arrays.sort(temp);
//        System.arraycopy(temp, 0, customers, 0, temp.length);

        System.out.println(Arrays.toString(customers));

        indsætKunde(customers, new Customer("Doe", "zoo", 22));
//        indsætKunde(customers, new Customer("Aaa", "Bbb", 22));
//        indsætKunde(customers, new Customer("Stark", "Sansa", 22));

        System.out.println(Arrays.toString(customers));

    }

    /**
     * Indsætter kunde i kunder. Arrayet kunder er sorteret
     * Krav: kunder er sorteret
     *
     */
    public static void indsætKunde(Customer[] customers, Customer customer){
//      Den skulle vel sendes med ?
        int customersLenght = 6;

        for (int i = customersLenght ; i > 0 ; i--) {

            if (customer.compareTo(customers[i-1]) > 0) {
                customers[i] = customer;
                return;
            }

            Customer temp = customers[i-1];
            customers[i-1] = customers[i];
            customers[i] = temp;

        }
        customers[0] = customer;
        customersLenght++;

    }


}
