import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * Created by Laukess on 23-05-2016.
 */
public class Opgave_1 {

    public static void main(String[] args) {
        ArrayList<Customer> customers = new ArrayList<>();
        customers.add(new Customer("Doe", "John", 22));
        customers.add(new Customer("Snow", "John", 44));
        customers.add(new Customer("Balish", "Pyter", 33));
        customers.add(new Customer("Hodor", "Hodor", 44));
        customers.add(new Customer("Stark", "Ned", 33));
        customers.add(new Customer("Bolten", "Ramsy", 55));

        Collections.sort(customers);

        System.out.println(customers);
        indsætKunde(customers, new Customer("Stark", "Sansa", 44));
        System.out.println(customers);
    }


    /**
     * Indsætter kunde i kunder. Listen kunder er sorteret
     * Krav: kunder er sorteret
     *
     */
    public static void indsætKunde(ArrayList<Customer> customers, Customer customer){

        for (int i = 0 ; i < customers.size() ; i++) {
            if (customer.compareTo(customers.get(i)) < 0) {
                customers.add(i, customer);
                return;
            }
        }
        customers.add(customers.size() - 1, customer);

    }


}
