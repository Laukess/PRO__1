import java.awt.*;
import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 * Created by Laukess on 22-05-2016.
 */
public class Opgave_10_2 {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        String ifl = "defaultInput";
        String ofl = "defaultOutput";

        try {
            System.out.println("Inputfile name: ");
            ifl = input.next();

            System.out.println("Outputfile name: ");
            ofl = input.next();
        } catch (Exception e) {
            System.out.println("error: " + e);
        }

        File inputFile = new File(ifl + ".txt");
        int lineNumber = 1;

        try {
            Scanner in = new Scanner(inputFile);
            PrintWriter out = new PrintWriter(ofl + ".txt");

            while (in.hasNextLine()) {
                out.println("/* " + lineNumber + " */ " + in.nextLine());
                lineNumber++;
            }
            in.close();
            out.close();


        } catch (Exception e) {
            System.out.println("error: " + e);
        }


    }



}
