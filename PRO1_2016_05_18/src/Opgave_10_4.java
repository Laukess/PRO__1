import java.io.File;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Laukess on 22-05-2016.
 */
public class Opgave_10_4 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String ifl = "input2";
        String ofl = "output2";
        ArrayList<ArrayList<Double>> lists = new ArrayList<>();

        try {
            System.out.println("Inputfile name: ");
            ifl = input.next();

            System.out.println("Outputfile name: ");
            ofl = input.next();
        } catch (Exception e) {
            System.out.println("error: " + e);
        }


        File inputFile = new File(ifl + ".txt");
        int lineNumber = 1;

        try {
            Scanner in = new Scanner(inputFile);
            PrintWriter out = new PrintWriter(ofl + ".txt");

            while (in.hasNextLine()) {

                if (lists.size() == 0) {
                    String[] fullLine = in.nextLine().split(" ");

                    for (String word : fullLine) {
                        if (!word.equals("")) {
                            lists.add(new ArrayList<Double>(){{add(Double.parseDouble(word));}});
                        }
                    }

                }

                String[] fullLine = in.nextLine().split(" ");
                int counter = 0;
                for (String word : fullLine) {
                    if (!word.equals("")) {
                        lists.get(counter++).add(Double.parseDouble(word));
                    }
                }

            }

            for (int i = 0 ; i < lists.size() ; i++) {
                double total = 0;
                for(double d : lists.get(i)) {
                    total += d;
                }
                out.print(total / lists.get(i).size() + "\t");
            }


            in.close();
            out.close();


        } catch (Exception e) {
            System.out.println("error: " + e);
        }

    }


}
