package yatzy;

/**
 * Created by lukas on 06-04-2016.
 */
public class YatzyTestDrive {

    public static void main(String[] args) {
        Yatzy y = new Yatzy();
        YatzyTestDrive ytd = new YatzyTestDrive();

        y.throwDice(new boolean[]{false, false, false, false, false});
        ytd.getValuesTestPrint(y.getValues());

        System.out.println("------------------------");

        y.throwDice(new boolean[]{true, false, false, false, false});
        ytd.getValuesTestPrint(y.getValues());

        System.out.println("------------------------");

        y.throwDice(new boolean[]{true, true, true, true, true});
        ytd.getValuesTestPrint(y.getValues());

        System.out.println("--------------------------------------");
        System.out.println(y.valueSpecificFace(3));

        System.out.println("--------------------------------------");
        System.out.println(y.valueOnePair());

        System.out.println("--------------------------------------");
        y.setValues(new int[]{6, 6, 6, 6, 6});
        ytd.getValuesTestPrint(y.getValues());
        System.out.println(y.valueTwoPair());


        System.out.println("--------------------------------------");
        y.setValues(new int[]{1, 2, 3, 4, 5});
        System.out.println(y.valueSmallStraight());

        System.out.println("--------------------------------------");
        y.setValues(new int[]{2, 3, 4, 5, 6});
        System.out.println(y.valueLargeStraight());

        y.setValues(new int[]{3, 3, 5, 5, 1});
        System.out.println(y.valueFullHouse());

    }
        public void getValuesTestPrint(int[] list) {
            for (int value : list) {
                System.out.println(value);
        }
    }
}