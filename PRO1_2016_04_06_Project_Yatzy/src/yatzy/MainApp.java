package yatzy;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

public class MainApp extends Application {
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) {
        stage.setTitle("Yatzy");
        GridPane pane = new GridPane();
        this.initContent(pane);
        Scene scene = new Scene(pane);
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    // -------------------------------------------------------------------------

    // The Yatzy game object
    private Yatzy yatzy = new Yatzy();
    // Shows the face values of the 5 dice.
    private TextField[] txfValues;
    // Shows the hold status of the 5 dice.
    private CheckBox[] chbHolds;
    private TextField[] txfResults;
    private boolean clickable = true;
    private boolean isFinalRoundAction = false;
    private TextField txfSumSame, txfBonus, txfSumOther, txfTotal;
    private Label lblRolled;
    private Button btnRoll;

    private Alert alert;

    private Label[] lblSingles;
    private Label lblOnePair, lblTwoPair, lblThreeSame, lblFourSame, lblFullHouse,
            lblSmallStraight, lblLargeStraight, lblChance, lblYatzy, lblSum, lblBonus, lblOtherSum, lblTotal;

    private void initContent(GridPane pane) {
        pane.setGridLinesVisible(false);
        pane.setPadding(new Insets(10));
        pane.setHgap(10);
        pane.setVgap(10);

        // ---------------------------------------------------------------------

        GridPane dicePane = new GridPane();
        pane.add(dicePane, 0, 0);
        dicePane.setGridLinesVisible(false);
        dicePane.setPadding(new Insets(10));
        dicePane.setHgap(10);
        dicePane.setVgap(10);
        dicePane.setStyle("-fx-border-color: black");

        // TODO: initialize txfValues, chbHolds, btnRoll and lblRolled
        txfValues = new TextField[]{new TextField(), new TextField(), new TextField(), new TextField(), new TextField()};
        chbHolds = new CheckBox[]{new CheckBox(), new CheckBox(), new CheckBox(), new CheckBox(), new CheckBox()};
        btnRoll = new Button();

        Font bigFont = new Font(50);

        for (int i = 0; i < chbHolds.length; i++) {
            dicePane.add(txfValues[i], i, 0);
            txfValues[i].setPrefWidth(100);
            txfValues[i].setPrefHeight(100);
            txfValues[i].setFont(bigFont);
            txfValues[i].setEditable(false);
            CheckBox chk = chbHolds[i];
            chk.setText("Hold");
            dicePane.add(chk, i, 1);
        }

        dicePane.add(btnRoll, 3, 2);
        btnRoll.setText("Roll");
        btnRoll.setMinSize(100, 50);

        HBox lblRolledBox = new HBox();

        lblRolled = new Label();
        lblRolled.setText("Rolled: " + yatzy.getThrowCount());

        lblRolledBox.getChildren().addAll(lblRolled);
        lblRolledBox.setAlignment(Pos.CENTER);
        dicePane.add(lblRolledBox, 4, 2);

        disableHoldCheckbox();

        // ---------------------------------------------------------------------

        GridPane scorePane = new GridPane();
        pane.add(scorePane, 0, 1);
        scorePane.setGridLinesVisible(false);
        scorePane.setPadding(new Insets(10));
        scorePane.setVgap(5);
        scorePane.setHgap(10);
        scorePane.setStyle("-fx-border-color: black");

        // TODO: Initialize labels for results, txfResults,
        lblSingles = new Label[]{new Label("1-s"), new Label("2-s"), new Label("3-s"), new Label("4-s"), new Label("5-s"), new Label("6-s")};
        lblOnePair = new Label("One pair");
        lblTwoPair = new Label("Two pairs");
        lblThreeSame = new Label("Three Same");
        lblFourSame = new Label("Four Same");
        lblFullHouse = new Label("Full House");
        lblSmallStraight = new Label("Small Straight");
        lblLargeStraight = new Label("Large Straight");
        lblChance = new Label("Chance");
        lblYatzy = new Label("Yatzy");


        for (int i = 0; i < lblSingles.length; i++) {
            scorePane.add(lblSingles[i], 0, i);
        }

        scorePane.add(lblOnePair, 0, 6);
        scorePane.add(lblTwoPair, 0, 7);
        scorePane.add(lblThreeSame, 0, 8);
        scorePane.add(lblFourSame, 0, 9);
        scorePane.add(lblFullHouse, 0, 10);
        scorePane.add(lblSmallStraight, 0, 11);
        scorePane.add(lblLargeStraight, 0, 12);
        scorePane.add(lblChance, 0, 13);
        scorePane.add(lblYatzy, 0, 14);

        txfResults = new TextField[15];
        for (int i = 0; i < txfResults.length; i++) {
            txfResults[i] = new TextField();
            scorePane.add(txfResults[i], 1, i);
            txfResults[i].setPrefWidth(60);
            txfResults[i].setEditable(false);
        }

        lblSum = new Label("Sum: ");
        scorePane.add(lblSum, 2, 5);

        txfSumSame = new TextField("0");
        txfSumSame.setPrefWidth(60);
        txfSumSame.setEditable(false);
        scorePane.add(txfSumSame, 3, 5);

        lblBonus = new Label("Bonus: ");
        scorePane.add(lblBonus, 4, 5);

        txfBonus = new TextField("0");
        txfBonus.setPrefWidth(60);
        txfBonus.setEditable(false);
        scorePane.add(txfBonus, 5, 5);

        lblOtherSum = new Label("Sum :");
        scorePane.add(lblOtherSum, 2, 14);

        txfSumOther = new TextField("0");
        txfSumOther.setPrefWidth(60);
        txfSumOther.setEditable(false);
        scorePane.add(txfSumOther, 3, 14);

        lblTotal = new Label("Total: ");
        scorePane.add(lblTotal, 4, 14);

        txfTotal = new TextField("0");
        txfTotal.setPrefWidth(60);
        txfTotal.setEditable(false);
        scorePane.add(txfTotal, 5, 14);

        for (int i = 0; i < txfResults.length; i++) {
            int index = i;
            txfResults[i].setOnMouseClicked(event -> saveValue(index));
        }

        alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Game finished");
        alert.setHeaderText(this.txfTotal.getText());
        alert.setContentText("Do you wish to play another game");

        btnRoll.setOnAction(event -> btnRollAction());

    }

    // -------------------------------------------------------------------------

    // TODO: Create a method for btnRoll's action.
    // Hint: Create small helper methods to be used in the action method.
    private void btnRollAction() {
        yatzy.throwDice(new boolean[]{
                chbHolds[0].isSelected(),
                chbHolds[1].isSelected(),
                chbHolds[2].isSelected(),
                chbHolds[3].isSelected(),
                chbHolds[4].isSelected()});
        int[] diceResults = yatzy.getValues();

        for (int i = 0; i < txfValues.length; i++) {
            txfValues[i].setText(diceResults[i] + "");
        }
        lblRolled.setText("Rolled: " + yatzy.getThrowCount());
        setResults();

        if (yatzy.getThrowCount() >= 3) {
            btnRoll.setDisable(true);
        }

        clickable = true;
        enableHoldCheckbox();
    }


    private void setResults() {
        for (int i = 0; i < yatzy.getPossibleResults().length; i++) {

            if (!txfResults[i].isDisable()) {
                txfResults[i].setText(yatzy.getPossibleResults()[i] + "");
            }

        }
    }

    // -------------------------------------------------------------------------

    // TODO: Create a method for mouse click on one of the text fields in txfResults.
    // Hint: Create small helper methods to be used in the mouse click method.


    private void saveValue(int index) {
        if (yatzy.getThrowCount() > 0 && clickable) {
            TextField tf = txfResults[index];
            tf.setDisable(true);

            for (TextField tField : txfResults) {
                if (!tField.isDisable()) {
                    tField.setText("0");
                }
            }
            btnRoll.setDisable(false);
            yatzy.resetThrowCount();
            clickable = false;

            int sum = 0;
            for (int i = 0; i < lblSingles.length; i++) {
                sum += Integer.parseInt(txfResults[i].getText());
            }
            txfSumSame.setText(sum + "");

            if (sum >= 63) {
                txfBonus.setText("50");
            }

            int sum2 = 0;
            for (int i = lblSingles.length; i < txfResults.length; i++) {
                sum2 += Integer.parseInt(txfResults[i].getText());
            }
            txfSumOther.setText(sum2 + "");

            txfTotal.setText(
                    Integer.parseInt(txfSumSame.getText())
                            + Integer.parseInt(txfBonus.getText()) +
                            Integer.parseInt(txfSumOther.getText()) + ""
            );


            btnRoll.requestFocus();
            disableHoldCheckbox();

            int numOfDisabledFields = 0;
            for (TextField field : txfResults) {
                if (field.isDisable()) {
                    numOfDisabledFields++;
                }
            }
            if (txfResults.length == numOfDisabledFields) {
                alert.setHeaderText("You scored: " + this.txfTotal.getText() + " points");
                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == ButtonType.OK) {
                    reset();
                } else {
                    Platform.exit();
                }
            }

        }
    }

    private void reset() {
        for (TextField field : txfResults) {
            field.setDisable(false);
        }
        yatzy.reset();
        setResults();

        for (TextField field : txfValues) {
            field.setText("");
        }

        txfSumSame.setText("0");
        txfBonus.setText("0");
        txfSumOther.setText("0");
        txfTotal.setText("0");

    }

    private void disableHoldCheckbox() {
        this.isFinalRoundAction = true;
        for (CheckBox box : chbHolds) {
            box.setSelected(false);
            box.setDisable(true);

        }
    }

    private void enableHoldCheckbox() {
        this.isFinalRoundAction = false;
        for (CheckBox box : chbHolds) {
            box.setDisable(false);
        }
    }

}
