package searching_exercises;

public class Player implements Comparable<Player> {
	private String name;
	private int height;
	private int weight;
	private int score;

	public Player(String name, int height, int weight, int score) {
		this.height = height;
		this.name = name;
		this.weight = weight;
		this.score = score;
	}

	public int compareTo(Player other) {
		if (this.getScore() < other.getScore()) {
			return -1;
		} else if(this.getScore() > other.getScore()) {
			return 1;
		} else {
			return 0;
		}
	}

	public String getName() {
		return name;
	}

	public int getHeight() {
		return height;
	}

	public int getWeight() {
		return weight;
	}

	public int getScore() {
		return score;
	}
	
	@Override
    public String toString(){
//		return "Name: " + name + " height: " + height + " weight: " + weight + " goals: " + score;
		return this.getScore()+ "";
	}

}
