package searching_exercises;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public class PlayerMethods {

	public Player findScoreLinear(ArrayList<Player> players, int score) {
		// TODO: exercise 4.1
        Player answer = null;
        int i = 0;

        while (answer == null && i < players.size()) {
            if (players.get(i).getScore() == score) {
                answer = players.get(i);
            }
            i++;
        }

		return answer;
	}

    // TODO: exercise 4.2
    public Player findScoreBinary(ArrayList<Player> players, int score) {
        int middle;
        int left = 0;
        int right = players.size() - 1;
        Player player = null;

        while (player == null && left <= right) {
            middle = (right + left) / 2;
            System.out.println("middle: " + middle);
            System.out.println("left: " + left);
            System.out.println("right: " + right);
            System.out.println(players.get(middle).getScore() + " - " + score);
            if (players.get(middle).getScore() == score) {
                player = players.get(middle);
            } else if (players.get(middle).getScore() > score) {
                right = middle - 1;
            } else {
                left = middle + 1;
            }
        }
        return player;

    }

    // TODO: exercise 4.3
    public String shortTopPlayer(ArrayList<Player> players) {
        for (Player player : players) {
            if (player.getScore() > 50 && player.getHeight() < 170) {
                return player.getName();
            }
        }
        return "";
    }

	
	public static void main(String[] args) {
		PlayerMethods metoder = new PlayerMethods();
		
		// Test of exercise 4.1
		ArrayList<Player> playerList = new ArrayList<Player>();
		playerList.add(new Player("Hans", 196, 99, 45));
		playerList.add(new Player("Bo", 203, 89, 60));
		playerList.add(new Player("Jens", 188, 109, 32));
		playerList.add(new Player("Finn", 194, 102, 12));
		playerList.add(new Player("Lars", 192, 86, 35));
		playerList.add(new Player("Mads", 200, 103, 37));
        playerList.add(new Player("John", 169, 101, 65));
		
		System.out.println("Player who has 35 goals: " + metoder.findScoreLinear(playerList, 35));
		System.out.println("Player who has 30 goals: " + metoder.findScoreLinear(playerList, 32));

		// TODO: test exercise 4.2
        System.out.println("Hvem har en score paa 60 ?" + metoder.findScoreLinear(playerList, 60));
        System.out.println("Hvem har en score paa 140 ?" + metoder.findScoreLinear(playerList, 140));

        Collections.sort(playerList);
        System.out.println(playerList);
        System.out.println(metoder.findScoreBinary(playerList, 12));

//      TODO: test exercise 4.3
        System.out.println("Hvem har over 50 maal, men er lavere end 170 cm ? " + metoder.shortTopPlayer(playerList));




	}

}
