package searching_exercises;

import java.util.ArrayList;

public class Searching {
	
	public static boolean findUneven(int[] tabel) {
		// TODO: exercise 1
        boolean found = false;
		int i = 0;
        while (!found && i < tabel.length) {
            if (tabel[i] % 2 != 0) {
                found = true;
            }
            i++;
        }
		return found;
	}

//  exercise 2
    public static int findFirstInRange(int[] table, int startRange, int endRange) {
        int answer = -1;
        int i = 0;
        while (answer == -1 && i < table.length) {
            if (table[i] >= startRange && table[i] <= endRange) {
                answer = table[i];
            }
            i++;
        }
        return answer;
    }

//  exercise 3
    public static boolean has2DuplicatesInARow(int[] table) {
        int i = 0;
        boolean found = false;

        while(!found && i < table.length - 1) {
            if (table[i] == table[i+1]) {
                found = true;
            }
            i++;
        }
        return found;


    }

    //exercise 5.1
    public static int heltalskvadratrod(int n) {
        int answer = 0;
        int r = 0;

        while (r <= n) {
            if (r * r <= n && n < (r+1) * (r+1)) {
//                answer = r;
                return r;
            }
            r++;
        }
        return answer;
    }

    public static int binFindHeltalskvaradtroden(int n) {
        boolean found = false;
        double left = 0;
        double right = n;
        double middle = -1;
        while (!found && left <= right) {
            middle = (left + right) / 2;
            if ((middle * middle) <= n && n < (middle + 1) * (middle + 1)) {
                found = true;
            } else if ((middle * middle) >= n && n < (middle + 1) * (middle + 1)) {
                right = middle - 1;
            } else {
                left = middle + 1;
            }
        }
        return (int) middle;
//        if (found) {
//            return (int) middle;
//        } else {
//            return -1;
//        }
    }


    /**
     * Returnér positionen på n efter det evt. er flyttet
     * én position til venstre. Hvis n ikke findes, returneres -1.
     *
     * exercise 6
     */
    public static int find(ArrayList<Integer> list, int n) {

        for (int i = 0 ; i < list.size() ; i++) {
            if (list.get(i) == n) {
                if (i == 0) {
                    return i;
                } else {
                    int temp = list.remove(i);
                    list.add(i-1, temp);
                    return i-1;
                }
            }
        }
        return -1;

    }

//  exercise 7
    public static boolean k_ens(String s, int k) {
        char last = s.charAt(0);
        int curInARow = 1;
        int maxInARow = curInARow;
        for (int i = 1 ; i < s.length() ; i++) {
            if (s.charAt(i) == last) {
                curInARow++;
                if (curInARow > maxInARow) {
                    maxInARow = curInARow;
                }
            } else {
                curInARow = 1;
            }
            last = s.charAt(i);
        }
        return maxInARow >= k;
    }



}
