package searching_exercises;

import java.util.ArrayList;
import java.util.Arrays;

public class TestSearching {
	
	public static void main(String[] args) {

	    // Test for exercise 1
        int[] array1 = {2, 4, 8, 2};
        System.out.println("Array: " + Arrays.toString(array1));
        System.out.println("Are there uneven numbers in the array? " + Searching.findUneven(array1));
        array1[2] = 15;
        System.out.println("Array: " + Arrays.toString(array1));
        System.out.println("Are there uneven numbers in the array? " + Searching.findUneven(array1));
        System.out.println();


        // Test for exercise 2
        int[] array2 = {7, 56, 34, 3, 7, 14, 13, 4};
        System.out.println("Array: " + Arrays.toString(array2));
        System.out.println("What is the first number in the array in the 10-15 range: " + Searching.findFirstInRange(array2, 10, 15));
        array2[5] = 15;
        System.out.println("Array: " + Arrays.toString(array2));
        System.out.println("What is the first number in the array in the 10-15 range: " + Searching.findFirstInRange(array2, 10, 15));
        System.out.println();


        // Test for exercise 3
        int[] array3 = {7, 9, 13, 7, 9, 13};
        System.out.println("Array: " + Arrays.toString(array3));
        System.out.println("Any duplicate numbers in a row: " + Searching.has2DuplicatesInARow(array3));
        array3[3] = 13;
        System.out.println("Array: " + Arrays.toString(array3));
        System.out.println("Any duplicate numbers in a row: " + Searching.has2DuplicatesInARow(array3));
        System.out.println();

        // Test for exercise 4
        PlayerMethods pm = new PlayerMethods();
        Player p1, p2, p3;
        ArrayList<Player> playerList = new ArrayList<>();
        p1 = new Player("Tom Hall", 200, 66, 120);
        playerList.add(p1);
        p2 = new Player("John Carmack", 188, 67, 130);
        playerList.add(p2);
        p3 = new Player("John Remero", 195, 44, 160);
        playerList.add(p3);

//      exercise 5.1
        System.out.println("0: " + Searching.heltalskvadratrod(0));
        System.out.println("1: " + Searching.heltalskvadratrod(1));
        System.out.println("3: " + Searching.heltalskvadratrod(3));
        System.out.println("4: " + Searching.heltalskvadratrod(4));
        System.out.println("7: " + Searching.heltalskvadratrod(7));
        System.out.println("8: " + Searching.heltalskvadratrod(8));
        System.out.println("9: " + Searching.heltalskvadratrod(9));
        System.out.println("10: " + Searching.heltalskvadratrod(10));
        System.out.println("111: " + Searching.heltalskvadratrod(111));

//      exercise 6.
        ArrayList<Integer> list = new ArrayList<>();
        list.add(6);
        list.add(4);
        list.add(8);
        list.add(13);
        list.add(2);

        System.out.println(list);
        System.out.println("pos: " + Searching.find(list, 13));
        System.out.println(list);

//      exercise 7.
        System.out.println("k_ens(\"Jubiiii\", 4)" + Searching.k_ens("Jubiiii", 4));
        System.out.println("k_ens(\"Jubiiii\", 5)" + Searching.k_ens("Jubiiii", 5));


        System.out.println(Searching.heltalskvadratrod(112120));
        System.out.println(Searching.binFindHeltalskvaradtroden(-111));
    }

}
