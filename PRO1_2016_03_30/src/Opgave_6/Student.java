package Opgave_6;

import sun.plugin2.gluegen.runtime.CPU;

/**
 * Class describing a student
 *
 */
public class Student {
	private String CRP;
	private String name;
	private boolean active;
	private int age;

	/**
	 * Create an inactive student.
	 */
	public Student(String name, int age, String CRP) {
		this.name = name;
		this.age = age;
		this.CRP = CRP;
		this.active = false;
	}

	/**
	 * Create a student where 'active' is given as a parameter
	 */
	public Student(String name, int age, boolean active, String CPR) {
		this.name = name;
		this.age = age;
		this.CRP = CPR;
		this.active = active;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isActive() {
		return active;
	}

	public String getCRP() {
		return CRP;
	}

	public void setCRP(String CRP) {
		this.CRP = CRP;
	}

	/**
	 * Increase the age of the student by 1 year.
	 */
	public void increaseAge() {
		age++;
	}

	@Override
	public String toString() {
		return "Name: \t" + name + "\nAge: \t" + age + "\nCPR: \t" + CRP + "\nActive: \t" + active;
	}
}
