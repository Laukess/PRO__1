package Opgave_6;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.util.ArrayList;

public class StudentApp extends Application {
    
	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		stage.setTitle("Student Administration");
		GridPane pane = new GridPane();
		this.initContent(pane);

		Scene scene = new Scene(pane);
		stage.setScene(scene);
		stage.show();
	}

	// -------------------------------------------------------------------------

	private TextField txfName;
	private TextField txfAge;
	private TextField txfCPR;
	private CheckBox chkActive;
	private TextArea txAInf;

	private Button btnAdd;
	private Button btnSave;
	private Button btnGet;
	private Button btnDelete;
	private Button btnIndex;

	private Controller controller = new Controller();

	private void initContent(GridPane pane) {
		// show or hide grid lines
		pane.setGridLinesVisible(false);
		// set padding of the pane
		pane.setPadding(new Insets(25));
		// set horizontal gap between components
		pane.setHgap(10);
		// set vertical gap between components
		pane.setVgap(10);

		txAInf = new TextArea();
		txAInf.setEditable(false);
		txAInf.setPrefWidth(230);
		txAInf.setPrefHeight(80);
		pane.add(txAInf, 0, 0, 5, 1);

		Label lblName = new Label("Navn");
		pane.add(lblName, 0, 1);

		Label lblage = new Label("Alder");
		pane.add(lblage, 0, 2);

		Label lblCPR = new Label("CPR Nr");
		pane.add(lblCPR, 0, 3);

		Label lblActive = new Label("Aktiv");
		pane.add(lblActive, 0, 4);

		txfName = new TextField();
		pane.add(txfName, 1, 1, 4, 1);

		txfAge = new TextField();
		pane.add(txfAge, 1, 2, 4, 1);

		txfCPR = new TextField();
		pane.add(txfCPR, 1, 3, 4, 1);

		chkActive = new CheckBox();
		pane.add(chkActive, 1, 4);

		// add a buttons to the pane
		btnAdd = new Button("Create");
		pane.add(btnAdd, 0, 5);
		btnSave = new Button("Save");
		btnSave.setDisable(true);
		pane.add(btnSave, 1, 5);
		btnGet = new Button("Load");
		btnGet.setDisable(true);
		pane.add(btnGet, 2, 5);
		btnDelete = new Button("Delete");
		pane.add(btnDelete, 3, 5);
		btnDelete.setDisable(true);
		btnIndex = new Button("Index");
		pane.add(btnIndex, 4, 5);
		btnIndex.setDisable(true);

		// connect a method to the button
		btnAdd.setOnAction(event -> this.controller.addAction());
		btnSave.setOnAction(event -> this.controller.saveAction());
		btnGet.setOnAction(event -> this.controller.getAction());
		btnDelete.setOnAction(event -> this.controller.deleteAction());
		btnIndex.setOnAction(event -> this.controller.incrementAction());
	}

	/**
	 * This class controls access to the model in this application. In this
	 * case, the model is a single Student object.
	 */
	private class Controller {
		//private Student studerende = null;
        private ArrayList<Student> students = new ArrayList<>();

		private void addAction() {
            students.add(
                    new Student(
                            txfName.getText().trim(),
                            Integer.parseInt(txfAge.getText().trim()),
                            chkActive.isSelected(),
                            txfCPR.getText().trim()));
            clearFields();
            txAInf.setText(getDescription());
            btnGet.setDisable(false);
            btnDelete.setDisable(false);
            btnIndex.setDisable(false);
		}

		private void saveAction() {

            Student student = null;

            for (Student s : students) {
                if (s.getCRP().equals(txfCPR.getText().trim())) {
                    student = s;
                }
            }

            if (student == null) {
                student = new Student(
                        txfName.getText().trim(),
                        Integer.parseInt(txfAge.getText().trim()),
                        chkActive.isSelected(),
                        txfCPR.getText().trim()
                );
                students.add(student);
            } else {
                student.setName(txfName.getText().trim());
                student.setAge(Integer.parseInt(txfAge.getText().trim()));
                student.setActive(chkActive.isSelected());
                student.setCRP(txfCPR.getText().trim());
            }
            clearFields();
            txAInf.setText(getDescription(txfCPR.getText()));
            btnSave.setDisable(true);
            btnDelete.setDisable(true);
            btnGet.setDisable(false);
            btnIndex.setDisable(false);

		}

		private void getAction() {
            txAInf.setText("");
			if (!students.isEmpty()) {
                Student student = null;
                for (Student s : students) {
                    if (s.getCRP().equals(txfCPR.getText())) {
                        student = s;
                    }
                }

                if (student != null) {
                    txfName.setText(student.getName());
                    txfAge.setText(student.getAge()+"");
                    txfCPR.setText(student.getCRP());
                    chkActive.setSelected(student.isActive());
                }

				txAInf.setText(getDescription(txfCPR.getText()));
				btnSave.setDisable(false);
				btnDelete.setDisable(false);
				btnGet.setDisable(false);
                btnIndex.setDisable(false);
			}
		}

		private void deleteAction() {
			if (!students.isEmpty()) {

                Student student = null;
                for (int s = 0 ; s < students.size() ; s++) {
                    if (students.get(s).getCRP().equals(txfCPR.getText())) {
                        students.remove(s);
                    }
                }

				clearFields();
				txAInf.clear();
				btnDelete.setDisable(true);
				btnSave.setDisable(true);
				btnGet.setDisable(false);
				btnAdd.setDisable(false);
                btnIndex.setDisable(false);
            }
		}

		private void incrementAction() {
            String result = "";
            for (Student student : students) {
                result += student.getCRP() + ": " + student.getName() + "\n";
            }
            txAInf.setText(result);
		}

		private void clearFields() {
			txfName.clear();
            txfAge.clear();
            txfCPR.clear();
			chkActive.setSelected(false);
		}

		private String getDescription(String CPR) {
            String result = getDescription();
			for (Student student : students) {
                if (student.getCRP().equals(CPR)) {
                    result=student.toString();
                }
            }
			return result;
		}

        private String getDescription() {
            String result = "";
            for (Student student : students) {
                result = student.toString();
            }
            return result;
        }
	}
}
