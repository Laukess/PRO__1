/**
 * Created by lukas on 30-03-2016.
 */

import javafx.application.Application;
import javafx.geometry.*;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class Opgave_2 extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) {
        stage.setTitle("Exercise 2");
        GridPane pane = new GridPane();
        this.initContent(pane);

        Scene scene = new Scene(pane);
        stage.setScene(scene);
        stage.show();
    }

    private TextField leftInput;
    private TextField rightInput;
    private Button swapBtn;

    Controller controller = new Controller();

    private void initContent(GridPane pane) {
        pane.setGridLinesVisible(false);
        pane.setPadding(new Insets(10));
        pane.setHgap(10);
        pane.setVgap(10);

        leftInput = new TextField();
        rightInput = new TextField();
        swapBtn = new Button();
        // add a text field to the pane (at col=1, row=0, extending 2 columns and 1 row)
        pane.add(leftInput, 0, 0, 1, 1);
        //leftInput.setText("left");

        pane.add(rightInput, 0, 1, 1, 1);
        //rightInput.setText("right");

        pane.add(swapBtn, 0, 2, 1, 1);
        swapBtn.setText("Swap");

        swapBtn.setOnAction(event -> this.controller.swap());
    }

    class Controller {

        public void swap() {
            String temp = leftInput.getText();
            leftInput.setText(rightInput.getText());
            rightInput.setText(temp);
        }

    }
}

