/**
 * Created by lukas on 30-03-2016.
 */

import javafx.application.Application;
import javafx.geometry.*;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class Opgave_3 extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) {
        stage.setTitle("Exercise 3");
        GridPane pane = new GridPane();
        this.initContent(pane);

        Scene scene = new Scene(pane);
        stage.setScene(scene);
        stage.show();
    }

    private TextField counterInput;
    private Button upBtn;
    private Button downBtn;
    private HBox buttonGrp;

    Controller controller = new Controller();

    private void initContent(GridPane pane) {
        pane.setGridLinesVisible(false);
        pane.setPadding(new Insets(30));
        pane.setHgap(10);
        pane.setVgap(10);

        counterInput = new TextField();

        upBtn = new Button();
        upBtn.setText("Up");

        downBtn = new Button();
        downBtn.setText("Down");

        buttonGrp = new HBox();
        buttonGrp.setSpacing(20);
        buttonGrp.getChildren().addAll(downBtn, upBtn);
        pane.add(buttonGrp, 0, 1, 1, 1);

        pane.add(counterInput, 0, 0, 1, 1);
        counterInput.setText("15");

        upBtn.setOnAction(event -> this.controller.increment());
        downBtn.setOnAction(event -> this.controller.decrement());
    }

    class Controller {

        public void increment() {
            try {
                int value = Integer.parseInt(counterInput.getText());
                value++;
                counterInput.setText(value+"");
            } catch (Exception e) {
                counterInput.setText("15");
            }
        }

        public void decrement() {
            try {
                int value = Integer.parseInt(counterInput.getText());
                value--;
                counterInput.setText(value+"");
            } catch (Exception e) {
                counterInput.setText("15");
            }
        }

    }
}


