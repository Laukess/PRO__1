/**
 * Created by lukas on 30-03-2016.
 */

import javafx.application.Application;
import javafx.geometry.*;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class Opgave_1 extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) {
        stage.setTitle("Exercise 1");
        GridPane pane = new GridPane();
        this.initContent(pane);

        Scene scene = new Scene(pane);
        stage.setScene(scene);
        stage.show();
    }

    private TextField leftInput;
    private TextField rightInput;
    private TextField combinedInput;
    private Button sumbmit;

    Controller controller = new Controller();

    private void initContent(GridPane pane) {
        pane.setGridLinesVisible(false);
        pane.setPadding(new Insets(20));
        pane.setHgap(10);
        pane.setVgap(10);

        leftInput = new TextField();
        rightInput = new TextField();
        combinedInput = new TextField();
        sumbmit = new Button();
        // add a text field to the pane (at col=1, row=0, extending 2 columns and 1 row)
        pane.add(leftInput, 0, 0, 1, 1);
        //leftInput.setText("left");

        pane.add(rightInput, 2, 0, 1, 1);
        //rightInput.setText("right");

        pane.add(combinedInput, 0, 1, 3, 1);
        //combinedInput.setText("comb");

        pane.add(sumbmit, 0, 2, 1, 1);
        sumbmit.setText("Combine");

        sumbmit.setOnAction(event -> this.controller.combine(leftInput.getText(), rightInput.getText()));
    }

    class Controller {

        public void combine(String text1, String text2) {
            combinedInput.setText(text1.trim() + " " + text2.trim());
        }

    }


























}


