/**
 * Created by lukas on 30-03-2016.
 */

import javafx.application.Application;
import javafx.geometry.*;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import sun.font.TextLabel;

public class Opgave_5 extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) {
        stage.setTitle("Exercise 2");
        GridPane pane = new GridPane();
        this.initContent(pane);

        Scene scene = new Scene(pane);
        stage.setScene(scene);
        stage.show();
    }

    private TextField CInput;

    private TextField FInput;

    private Button calcBtn;

    Controller controller = new Controller();

    private void initContent(GridPane pane) {
        pane.setGridLinesVisible(false);
        pane.setPadding(new Insets(10));
        pane.setHgap(10);
        pane.setVgap(10);

        CInput = new TextField();
        Label CLabel = new Label("Celsius: ");

        FInput = new TextField();
        Label FLabel = new Label("Fahrenheit: ");

        calcBtn = new Button();
        pane.add(CLabel, 0, 0, 1, 1);
        pane.add(CInput, 1, 0, 1, 1);

        pane.add(FLabel, 0, 1, 1, 1);
        pane.add(FInput, 1, 1, 1, 1);

        pane.add(calcBtn, 0, 2, 1, 1);
        calcBtn.setText("Calc");

        calcBtn.setOnAction(event -> this.controller.calc());
    }

    class Controller {

        public void calc() {

            String c = CInput.getText();
            String f = FInput.getText();
            // if both are not empty AND c or f is not empty
            if( !(c.equals("") && f.equals("")) && (c.equals("") || f.equals("")) ) {
                if (c.equals("")) {
                    calcC();
                } else {
                    calcF();
                }
            }

        }

        public void calcC() {

            try {
                //int F = Integer.parseInt(FInput.getText());
                double F = Double.parseDouble(FInput.getText());
                CInput.setText((F - 32) / (9.0/5.0)   + "");
            } catch (Exception e) {
                CInput.setText(FInput.getText() + " is not a valid number");
            }

        }

        public void calcF() {

            try {
                //int C = Integer.parseInt(CInput.getText());
                double C = Double.parseDouble(CInput.getText());
                FInput.setText((9.0/5.0*C + 32) +"");
            } catch (Exception e) {
                FInput.setText(CInput.getText() + " is not a valid number");
            }

        }

    }
}
