package Opgave_1;

import java.util.ArrayList;

/**
 * Created by lukas on 11-04-2016.
 */
public class Car {

    private String license;
    private double pricePerDay;
    private int purchaseYear;

    private ArrayList<Rental> rentals = new ArrayList<>();

    public Car(String license, int year) {
        this.license = license;
        this.purchaseYear = year;
    }

    public void setDayPrice(double price) {
        this.pricePerDay = price;
    }

    public double getDayPrice() {
        return pricePerDay;
    }

    public String getLicense() {
        return this.license;
    }

    public int getPurchaseYear() {
        return this.purchaseYear;
    }

    public ArrayList<Rental> getRentals() {
        return rentals;
    }

    public void addRental(Rental rental) {
        this.rentals.add(rental);
    }

    public void removeRental(Rental rental) {
        this.rentals.remove(rental);
    }

    public int rentedLongest() {
        int max = Integer.MIN_VALUE;

        for (Rental rental : rentals) {
            if (rental.getDays() > max) {
                max = rental.getDays();
            }
        }

        return max == Integer.MIN_VALUE ? 0 : max;
    }

}
