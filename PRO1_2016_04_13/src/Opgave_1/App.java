package Opgave_1;

/**
 * Created by lukas on 13-04-2016.
 */
public class App {

    public static void main(String[] args) {

        Car car1 = new Car("1", 1989);
        car1.setDayPrice(200);
        Car car2 = new Car("2", 1999);
        car2.setDayPrice(300);
        Rental rental1 = new Rental(12, "2016-04-13", 14);
        Rental rental2 = new Rental(12, "2016-04-13", 16);
        Rental rental3 = new Rental(12, "2016-04-13", 15);

        car1.addRental(rental1);
        rental1.addCar(car1);

        car1.addRental(rental2);
        rental2.addCar(car1);

        car1.addRental(rental3);
        rental3.addCar(car1);

        System.out.println(car1.rentedLongest());

        rental1.addCar(car2);
        car2.addRental(rental1);
        System.out.println(rental1.getPrice());

    }

}
