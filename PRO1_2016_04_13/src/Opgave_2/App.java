package Opgave_2;

import java.util.ArrayList;

/**
 * Created by lukas on 13-04-2016.
 */
public class App {

    public static void main(String[] args) {
        Person p1 = new Person("Morten", 12);
        Person p2 = new Person("Peter", 13);
        Person p3 = new Person("Axel", 33);

        Gift gift1 = new Gift("This is a nice car", p2);
        gift1.setPrice(120000);

        Gift gift2 = new Gift("This is not a nice car, its a dead horse", p3);
        gift2.setPrice(13);

        Gift gift3 = new Gift("mule", p2);
        gift1.setPrice(100);

        p1.addReceivedGift(gift1);
        p1.addReceivedGift(gift2);
        p1.addReceivedGift(gift3);

        System.out.println(p1.CombinedValueOfAllGifts());

        System.out.println(p1.getPeopleWhoGaveMeGifts());

    }

}
