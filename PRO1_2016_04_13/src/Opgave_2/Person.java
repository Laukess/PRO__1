package Opgave_2;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by lukas on 13-04-2016.
 */
public class Person {
    private String name;
    private int age;

    private ArrayList<Gift> receivedGifts = new ArrayList<>();

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return this.name;
    }

    public ArrayList<Gift> getReceivedGifts() {
        return this.receivedGifts;
    }

    public void addReceivedGift(Gift gift) {
        this.receivedGifts.add(gift);
    }

    /**
     * removes a gift, and returns it if its found, or null (right?)
     * @param gift the gift to remove
     * @return gift or null
     */
    public Gift removeReceivedGift(Gift gift) {
        return this.receivedGifts.remove(receivedGifts.indexOf(gift));
    }

    public double CombinedValueOfAllGifts() {
        double value = 0;

        for (Gift gift : receivedGifts) {
            value += gift.getPrice();
        }
        return value;
    }

    public ArrayList<Person> getPeopleWhoGaveMeGifts() {
        ArrayList<Person> people = new ArrayList<>();

        for (Gift gift : this.getReceivedGifts()) {
            if (!people.contains(gift.getGiftGiver())) {
                people.add(gift.getGiftGiver());
            }
        }
        return people;
    }

}
