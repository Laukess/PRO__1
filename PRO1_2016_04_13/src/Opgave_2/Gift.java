package Opgave_2;

/**
 * Created by lukas on 13-04-2016.
 */
public class Gift {
    private String description;
    private double price;
    private Person giftGiver;

    public Gift(String description, Person person) {
        this.description = description;
        this.giftGiver  = person;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Person getGiftGiver() {
        return this.giftGiver;
    }
}
