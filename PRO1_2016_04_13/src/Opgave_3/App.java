package Opgave_3;

import java.util.ArrayList;

/**
 * Created by lukas on 13-04-2016.
 */
public class App {

    public static void main(String[] args) {
        TrainingPlan plan = new TrainingPlan('A', 1, 33);
        ArrayList<Double> laptimes = new ArrayList<>();
        laptimes.add(12.0);
        laptimes.add(33.0);
        plan.createSwimmer("morten", 1089, laptimes, "Alpha Beta Gamma");
        plan.createSwimmer("peter", 1089, laptimes, "Alpha Beta Gamma");

    }

}
