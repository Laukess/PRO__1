package Opgave_4;

import java.util.ArrayList;

/**
 * Created by lukas on 24-04-2016.
 */
public class NumericQuestion extends Question {

    private double answer;

    public NumericQuestion() {
        this.answer = 0.0;
        setText("");
    }

    public boolean checkAnswer(String response) {
        System.out.println(response);
        System.out.println(getAnswer());
        return Double.parseDouble(response) >= (answer - 0.01) && Double.parseDouble(response) <= (answer + 0.01) ;
    }

    public void setAnswer(double answer) {
        this.answer = answer;
    }
}
