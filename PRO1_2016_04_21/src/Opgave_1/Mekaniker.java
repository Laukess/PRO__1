package Opgave_1;

/**
 * Created by lukas on 21-04-2016.
 */
public class Mekaniker extends Person {
    private int aar;
    private double timeløn;

    public Mekaniker(String navn, String adresse, int aar, double timeløn) {
        super(navn, adresse);
        this.aar = aar;
        this.timeløn = timeløn;
    }

    public int getAar() {
        return aar;
    }

    public void setAar(int aar) {
        this.aar = aar;
    }

    public double getTimeløn() {
        return timeløn;
    }

    public void setTimeløn(double timeløn) {
        this.timeløn = timeløn;
    }
}
