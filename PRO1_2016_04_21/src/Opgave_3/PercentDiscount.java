package Opgave_3;

/**
 * Created by lukas on 21-04-2016.
 */
public class PercentDiscount extends Discount {

    public double getDiscountedPrice(double origPrice) {
        return getDiscount(origPrice) * origPrice;
    }

}
