package Opgave_3;

/**
 * Created by lukas on 21-04-2016.
 */
public class Discount {

    private double discount = 0.0;

    public double getDiscountedPrice(double origPrice) {
        return discount;
    }

    public double getDiscount(double origPrice) {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

}
