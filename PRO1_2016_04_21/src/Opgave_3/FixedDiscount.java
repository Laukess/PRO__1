package Opgave_3;

/**
 * Created by lukas on 21-04-2016.
 */
public class FixedDiscount extends Discount {

    public double getDiscountedPrice(double origPrice) {
        return origPrice > 1000 ? getDiscount(origPrice) : 0;
    }

}
