package Opgave_2;

/**
 * Created by lukas on 21-04-2016.
 */
public class Værkfører extends Mekaniker {

    private int promAar;
    private int tillaeg;

    public Værkfører(String navn, String adresse, int aar, double timeløn, int promAar, int tillaeg) {
        super(navn, adresse, aar, timeløn);
        this.promAar = promAar;
        this.tillaeg = tillaeg;
    }

    public int getPromAar() {
        return promAar;
    }

    public void setPromAar(int promAar) {
        this.promAar = promAar;
    }

    public int getTillaeg() {
        return tillaeg;
    }

    public double getTimeløn() {
        return super.getTimeløn() + tillaeg;
    }

    public void setTillaeg(int tillaeg) {
        this.tillaeg = tillaeg;
    }
}
