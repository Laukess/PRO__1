package Opgave_2;

import java.util.ArrayList;

/**
 * Created by lukas on 21-04-2016.
 */
public class Mekaniker extends Person {
    private int aar;
    private double timeløn;
    private double arbejdsuge;

    public Mekaniker(String navn, String adresse, int aar, double timeløn) {
        super(navn, adresse);
        this.aar = aar;
        this.timeløn = timeløn;
        this.arbejdsuge = 37;
    }

    public int getAar() {
        return aar;
    }

    public void setAar(int aar) {
        this.aar = aar;
    }

    public double getTimeløn() {
        return timeløn;
    }

    public void setTimeløn(double timeløn) {
        this.timeløn = timeløn;
    }

    public double getArbejdsuge() {
        return arbejdsuge;
    }

    public void setArbejdsuge(double arbejdsuge) {
        this.arbejdsuge = arbejdsuge;
    }

    public double getUgeLoen() {
        return getTimeløn() * this.arbejdsuge;
    }

    public static double beregnSamletLøn(ArrayList<Mekaniker> list) {
        double samletLoen = 0;
        for (Mekaniker m : list) {
            samletLoen += m.getUgeLoen();
        }
        return samletLoen;
    }

}
