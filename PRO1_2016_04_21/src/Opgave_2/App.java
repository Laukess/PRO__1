package Opgave_2;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by lukas on 21-04-2016.
 */
public class App {

    public static void main(String[] args) {
        Person p = new Person("John", "Hervej 22");
        Mekaniker m = new Mekaniker("Mika", "Vej 12", 1999, 220);
        Værkfører v = new Værkfører("Spike", "Vej 13", 1989, 220, 2007, 30);
        Synsmand s = new Synsmand("Peter", "jel", 1998, 220);

        s.setSyn(4);

        ArrayList<Mekaniker> mekanikere = new ArrayList<>();
        mekanikere.add(m);
        mekanikere.add(v);
        mekanikere.add(s);

        for (Mekaniker mekaniker : mekanikere) {
            System.out.println(mekaniker.getUgeLoen());
        }

        System.out.println("Den samlede Loen er: " + Mekaniker.beregnSamletLøn(mekanikere));

    }

}
