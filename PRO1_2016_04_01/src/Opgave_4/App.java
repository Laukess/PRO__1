package Opgave_4;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.ArrayList;

/**
 * Created by lukas on 01-04-2016.
 */
public class App extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) {
        stage.setTitle("Person List App");
        GridPane pane = new GridPane();
        this.initContent(pane);

        Scene scene = new Scene(pane);
        stage.setScene(scene);
        stage.show();
        txtFldName.requestFocus();
    }

    private TextField txtFldName;


    private Button btnAddName;
    private ListView<Person> lvwPeople;

    private Controller controller = new Controller();

    private void initContent(GridPane pane) {

        pane.setGridLinesVisible(false);
        pane.setPadding(new Insets(10));
        pane.setHgap(10);
        pane.setVgap(10);

        VBox lblNamesBox = new VBox();
        pane.add(lblNamesBox, 0 ,0);
        lblNamesBox.setAlignment(Pos.TOP_LEFT);

        Label lblNames = new Label("Names:");
        lblNamesBox.getChildren().add(lblNames);

        lvwPeople = new ListView<>();
        lvwPeople.getItems().setAll(controller.getNames());
        final int ROW_HEIGHT = 25;
        final int DEFAULT_ROWS = 4;
        lvwPeople.setPrefHeight(ROW_HEIGHT * DEFAULT_ROWS);
        pane.add(lvwPeople, 1, 0, 2, 1);


        Label lblName = new Label("Name:");
        txtFldName = new TextField();
        pane.add(lblName, 0 ,1);
        pane.add(txtFldName, 1, 1, 2, 1);

        btnAddName = new Button();
        btnAddName.setText("Add name");
        pane.add(btnAddName, 3, 1);


        btnAddName.setOnAction(event -> this.controller.addName());


        txtFldName.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER)  {
                    controller.addName();
                }
            }
        });
    }


    class Controller {

        ArrayList<Person> people = new ArrayList<>();

        public ArrayList<Person> getNames() {
            return people;
         }

        private void addName() {

            if (validate()) {

                try {

                    for (Person person : people) {
                        if (person.getName().equals(txtFldName.getText())) {
                            Alert alert = new Alert(Alert.AlertType.INFORMATION);
                            alert.setTitle("Add Name");
                            alert.setHeaderText("Name Already exist");
                            alert.show();
                            return;
                        }
                    }

                    Person newPerson = new Person(
                            txtFldName.getText()
                    );
                    people.add(newPerson);
                    lvwPeople.getItems().setAll(people);
                    txtFldName.setText("");
                    txtFldName.requestFocus();
                } catch (Exception e) {  }

            }
        }

        private boolean validate() {

            if (txtFldName.getText().equals("")) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Add person");
                alert.setHeaderText("Name empty");
                alert.setContentText("Fill in the name of the person");
                alert.show();
                return false;
            }
            return true;
        }





















    }

















}
