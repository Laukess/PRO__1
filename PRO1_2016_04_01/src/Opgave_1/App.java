package Opgave_1;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.text.TextFlow;
import javafx.stage.Stage;
import sun.font.TextLabel;

import java.util.ArrayList;

/**
 * Created by lukas on 01-04-2016.
 */
public class App extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) {
        stage.setTitle("Person List App");
        GridPane pane = new GridPane();
        this.initContent(pane);

        Scene scene = new Scene(pane);
        stage.setScene(scene);
        stage.show();
    }

    private TextField txtFldName;
    private TextField txtFldTilte;
    private CheckBox chkBoxSenior;
    private Button btnAddPerson;
    private ListView<Person> lvwPeople;

    private Controller controller = new Controller();

    private void initContent(GridPane pane) {

        pane.setGridLinesVisible(false);
        pane.setPadding(new Insets(10));
        pane.setHgap(10);
        pane.setVgap(10);

        Label lblName = new Label("Name:");
        txtFldName = new TextField();
        pane.add(lblName, 0 ,0);
        pane.add(txtFldName, 1, 0, 2, 1);

        Label lblTitle = new Label("Title:");
        txtFldTilte = new TextField();
        pane.add(lblTitle, 0, 1);
        pane.add(txtFldTilte, 1, 1, 2, 1);

        chkBoxSenior = new CheckBox();
        chkBoxSenior.setText("Senior");
        pane.add(chkBoxSenior, 0, 2);

        btnAddPerson = new Button();
        btnAddPerson.setText("Add Person");
        pane.add(btnAddPerson, 3, 2);

        Label lblPeople = new Label("People:");
        lvwPeople = new ListView<>();
        lvwPeople.getItems().setAll(controller.getPeople());
        final int ROW_HEIGHT = 25;
        final int DEFAULT_ROWS = 4;
        lvwPeople.setPrefHeight(ROW_HEIGHT * DEFAULT_ROWS);
        pane.add(lblPeople, 0, 3);
        pane.add(lvwPeople, 1, 3, 2, 1);

        btnAddPerson.setOnAction(event -> this.controller.addPerson());

    }


    class Controller {

        ArrayList<Person> people = new ArrayList<>();

        public ArrayList<Person> getPeople() {
            return people;
         }

        private void addPerson() {
            try {

                for (Person person : people) {
                    if (person.getName().equals(txtFldName.getText())) {
                        return;
                    }
                }

                //'peter' in person.name for person in people

                Person newPerson = new Person(
                        txtFldName.getText(),
                        txtFldTilte.getText(),
                        chkBoxSenior.isSelected()
                );
                people.add(newPerson);
                lvwPeople.getItems().setAll(people);


            } catch (Exception e) {

            }
        }























    }

















}
