package Opgave_3;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.util.ArrayList;

/**
 * Created by lukas on 01-04-2016.
 */
public class App extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) {
        stage.setTitle("Person List App");
        GridPane pane = new GridPane();
        this.initContent(pane);

        personWindow = new PersonInputWindow("Create Person", stage);
        Scene scene = new Scene(pane);
        stage.setScene(scene);
        stage.show();
    }

    private PersonInputWindow personWindow;
    private Button btnAddPerson;
    private ListView<Person> lvwPeople;

    private Controller controller = new Controller();

    private void initContent(GridPane pane) {

        pane.setGridLinesVisible(false);
        pane.setPadding(new Insets(10));
        pane.setHgap(10);
        pane.setVgap(10);

        btnAddPerson = new Button();
        btnAddPerson.setText("Add Person");
        pane.add(btnAddPerson, 2, 0);

        lvwPeople = new ListView<>();
        lvwPeople.getItems().setAll(controller.getPeople());
        final int ROW_HEIGHT = 25;
        final int DEFAULT_ROWS = 4;
        lvwPeople.setPrefHeight(ROW_HEIGHT * DEFAULT_ROWS);
        pane.add(lvwPeople, 0, 0, 2, 1);

        btnAddPerson.setOnAction(event -> this.controller.createPersonAction());

    }


    class Controller {

        ArrayList<Person> people = new ArrayList<>();

        public void createPersonAction() {
            personWindow.showAndWait();

            if (personWindow.getPerson() != null) {

                for (Person person : people) {
                    if (person.getName().equals(personWindow.getPerson().getName())) {

                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("Add Person");
                        alert.setHeaderText("Person Already Added!");
                        alert.show();

                        return;
                    }
                }

                people.add(personWindow.getPerson());
                lvwPeople.getItems().setAll(people);
            }
        }

        public ArrayList<Person> getPeople() {
            return people;
        }

    }

















}
