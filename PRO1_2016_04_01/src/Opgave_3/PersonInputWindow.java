package Opgave_3;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * Created by lukas on 01-04-2016.
 */
public class PersonInputWindow extends Stage {

    public PersonInputWindow(String title, Stage owner) {
        this.initOwner(owner);
        this.initStyle(StageStyle.UTILITY);
        this.initModality(Modality.APPLICATION_MODAL);
        this.setMinHeight(100);
        this.setMinWidth(200);
        this.setResizable(false);

        this.setTitle(title);
        GridPane pane = new GridPane();
        this.initContent(pane);

        Scene scene = new Scene(pane);
        this.setScene(scene);

    }

    private TextField txtFldName;
    private TextField txtFldTitle;
    private CheckBox chkBoxSenior;
    private Button btnCancel;
    private Button btnOK;

    private Person person = null;

    private void initContent(GridPane pane) {
        pane.setGridLinesVisible(false);
        pane.setPadding(new Insets(10));
        pane.setHgap(10);
        pane.setVgap(10);

        Label lblName = new Label("Name:");
        txtFldName = new TextField();
        pane.add(lblName, 0 ,0);
        pane.add(txtFldName, 1, 0, 2, 1);

        Label lblTitle = new Label("Title:");
        txtFldTitle = new TextField();
        pane.add(lblTitle, 0, 1);
        pane.add(txtFldTitle, 1, 1, 2, 1);

        chkBoxSenior = new CheckBox();
        chkBoxSenior.setText("Senior");
        pane.add(chkBoxSenior, 0, 2);

        btnCancel = new Button();
        btnCancel.setText("Cancel");
        pane.add(btnCancel, 1,3);

        btnOK = new Button();
        btnOK.setText("OK");
        pane.add(btnOK, 2, 3);

        btnCancel.setOnAction(event -> this.cancelAction());
        btnOK.setOnAction(event -> this.OKAction());
    }

    public Person getPerson() {
        return person;
    }

    private void clearInput() {
        txtFldName.clear();
        // So the field have focus next time it is shown.
        txtFldName.requestFocus();
        txtFldTitle.clear();
        chkBoxSenior.setSelected(false);
    }

    public void cancelAction() {
        clearInput();
        person = null;
        this.hide();
    }

    public void OKAction() {

        if (InputIsValid()) {
            person = new Person(
                    txtFldName.getText().trim(),
                    txtFldTitle.getText().trim(),
                    chkBoxSenior.isSelected()
            );
            clearInput();
            this.hide();
        }

    }

    private boolean InputIsValid() {
        if (txtFldName.getText().equals("")) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Add person");
            alert.setHeaderText("Name empty");
            alert.setContentText("Fill in the name of the person");
            alert.show();
            return false;
        } else if (txtFldTitle.getText().equals("")) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Add person");
            alert.setHeaderText("Title empty");
            alert.setContentText("Fill in the title of the person");
            alert.show();
            return false;
        }
        return true;
    }

}
