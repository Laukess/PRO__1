package Opgave_5;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.ArrayList;

/**
 * Created by lukas on 01-04-2016.
 */
public class App extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) {
        stage.setTitle("Person List App");
        GridPane pane = new GridPane();
        this.initContent(pane);

        Scene scene = new Scene(pane);
        stage.setScene(scene);
        stage.show();
        txtFldName.requestFocus();
    }

    private TextField txtFldName;
    private Button btnAddName;
    private RadioButton rBtnBoys;
    private RadioButton rbtnGirls;
    private ListView<Person> lvwPeople;
    private VBox lblNamesBox;
    private HBox genderRadioBox;
    private ToggleGroup group;

    private Controller controller = new Controller();

    private void initContent(GridPane pane) {

        pane.setGridLinesVisible(false);
        pane.setPadding(new Insets(10));
        pane.setHgap(10);
        pane.setVgap(10);


        genderRadioBox = new HBox();
        pane.add(genderRadioBox, 1, 0, 2, 1);
        genderRadioBox.setAlignment(Pos.BASELINE_CENTER);


        group = new ToggleGroup();
        rBtnBoys = new RadioButton();
        rBtnBoys.setText("Boys");
        rBtnBoys.setUserData(Person.Gender.BOY);
        rBtnBoys.setToggleGroup(group);

        rbtnGirls = new RadioButton();
        rbtnGirls.setText("Girls");
        rbtnGirls.setUserData(Person.Gender.GIRL);
        rbtnGirls.setToggleGroup(group);

        genderRadioBox.getChildren().add(rBtnBoys);
        genderRadioBox.getChildren().add(rbtnGirls);


        lblNamesBox = new VBox();
        pane.add(lblNamesBox, 0 ,1);
        lblNamesBox.setAlignment(Pos.TOP_LEFT);


        Label lblNames = new Label("Names:");
        lblNamesBox.getChildren().add(lblNames);


        lvwPeople = new ListView<>();
        controller.initializePeople();
        lvwPeople.getItems().setAll(controller.getNames());
        final int ROW_HEIGHT = 25;
        final int DEFAULT_ROWS = 4;
        lvwPeople.setPrefHeight(ROW_HEIGHT * DEFAULT_ROWS);
        pane.add(lvwPeople, 1, 1, 2, 1);


        Label lblName = new Label("Name:");
        txtFldName = new TextField();
        pane.add(lblName, 0 ,2);
        pane.add(txtFldName, 1, 2, 2, 1);

        btnAddName = new Button();
        btnAddName.setText("Add name");
        pane.add(btnAddName, 3, 2);


        btnAddName.setOnAction(event -> this.controller.addName());

        group.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                controller.displayNames();
            }
        });

        txtFldName.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER)  {
                    controller.addName();
                }
            }
        });
    }


    class Controller {

        ArrayList<Person> people = new ArrayList<>();

        public void initializePeople() {
            people.add(new Person("Sarah", Person.Gender.GIRL));
            people.add(new Person("Peter", Person.Gender.BOY));

            people.add(new Person("pia", Person.Gender.GIRL));
            people.add(new Person("john", Person.Gender.BOY));

            people.add(new Person("eve", Person.Gender.GIRL));
            people.add(new Person("mike", Person.Gender.BOY));
        }

        public void displayNames() {
            lvwPeople.getItems().setAll(getNames());
        }

        public ArrayList<Person> getNames() {
            ArrayList<Person> result = new ArrayList<>(people);

            if ( group.getSelectedToggle() != null) {

                result.clear();

                Person.Gender relevantGender = (Person.Gender) group.getSelectedToggle().getUserData();

                for (Person person : people) {
                    if (person.getGender() == relevantGender) {
                        result.add(person);
                    }
                }

            }

            return result;
         }

        private void addName() {

            if (validate()) {

                try {

                    for (Person person : people) {
                        if (person.getName().equals(txtFldName.getText())) {
                            Alert alert = new Alert(Alert.AlertType.INFORMATION);
                            alert.setTitle("Add Name");
                            alert.setHeaderText("Name Already exist");
                            alert.show();
                            return;
                        }
                    }

                    if ( group.getSelectedToggle() != null) {

                        Person.Gender gender = (Person.Gender) group.getSelectedToggle().getUserData();

                        Person newPerson = new Person(
                                txtFldName.getText(),
                                gender
                        );
                        people.add(newPerson);
                        txtFldName.setText("");
                        txtFldName.requestFocus();
                        displayNames();
                    }


                } catch (Exception e) {  }

            }
        }

        private boolean validate() {

            if (txtFldName.getText().equals("")) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Add person");
                alert.setHeaderText("Name empty");
                alert.setContentText("Fill in the name of the person");
                alert.show();
                return false;
            }

            // ToDo: refactor med [group.getSelectedToggle().getUserData()] ?
            for (int i = 0 ; i < group.getToggles().size() ; i++) {
                if (group.getToggles().get(i).isSelected()) {
                    return true;
                }
            }

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Add person");
            alert.setHeaderText("Gender Not Selected");
            alert.setContentText("Choose the gender of the person");
            alert.show();
            return false;
        }




















    }

















}
