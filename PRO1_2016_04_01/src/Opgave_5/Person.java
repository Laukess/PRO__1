package Opgave_5;

import java.util.Enumeration;

/**
 * Created by lukas on 01-04-2016.
 */
public class Person {

    private String name;
    private Gender gender;

    public Person(String name, Gender gender) {
        this.name = name;
        this.gender = gender;
    }

    public String getName() {
        return this.name;
    }

    public Gender getGender() { return this.gender; }

    @Override
    public String toString() {
        return getName();
    }

    public enum Gender {
        GIRL,
        BOY
    }

}
