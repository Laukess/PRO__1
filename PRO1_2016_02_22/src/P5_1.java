/**
 * Created by lukas on 22-02-2016.
 */
public class P5_1 {

    public static void main(String[] args) {
        // a
        int i = 2;
        int sum = 0;
        while (i <= 100) {
            sum += i;
            i = i+2;
        }
        System.out.println("a -> " + sum);

        // b
        i = 0;
        int total = 0;
        double total2 = 0;
        while (i <= 100) {
            total += Math.pow(i, 2);
            total2 += Math.sqrt(i);
            i++;
        }
        System.out.println("b -> " + total + " eller " + total2);

        // c
        total = 0;
        for (int k = 0; k <= 20 ; k++ ) {
            total += Math.pow(2, k);
        }
        System.out.println("c -> " + total);

        MyScanner ms = new MyScanner();
        double input1;
        do {
            input1 = ms.getDouble("Indtast det foerste nummer.");
        } while (input1 < 0);

        double input2;
        do {
            input2 = ms.getDouble("Indtast det naeste nummer.");
        } while (input2 < 0);

        total = 0;
        for (double j = Math.min(input1, input2) ; j <= Math.max(input2, input1) ; j++) {

            if (i % 2 != 0) {
                total += j;
            }

        }
        System.out.println("d -> " + total);


        String input3; //"1377286"
        do {
            input3 = ms.getString("Indtast en raekke numre.");
        } while (input3.equals(" "));

        total = 0;
        for ( i = 0 ; i < input3.length() ; i++ ) {
            int digit = input3.charAt(i) - '0';

            if (digit % 2 != 0) {
                total += digit;
            }

        }
        System.out.println("e -> " + total);
    }

}
