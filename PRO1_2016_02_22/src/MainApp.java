import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class MainApp extends Application
{
    public static void main(String[] args)
    {
        Application.launch(args);
    }

    @Override
    public void start(Stage stage)
    {
        GridPane root = this.initContent();
        Scene scene = new Scene(root);

        stage.setTitle("Loops");
        stage.setScene(scene);
        stage.show();
    }

    private GridPane initContent()
    {
        GridPane pane = new GridPane();
        Canvas canvas = new Canvas(500, 500);
        pane.add(canvas, 0, 0);
        this.drawShapes(canvas.getGraphicsContext2D());
        return pane;
    }

    // ------------------------------------------------------------------------

    private void drawShapes(GraphicsContext gc) {
        //exercise_1(gc);

        //exercise_2(gc);

        //exercise_3_1(gc);
        //exercise_3_2(gc);
        //exercise_3_3(gc);

        //exercise_4_1(gc);
        //exercise_4_2(gc);
        //exercise_4_3(gc);

        //exercise_5(gc);

        //exercise_6(gc);

        //exercise_7(gc);

        //exercise_8(gc);

        //exercise_9(gc);

        //exercise_10(gc);

        //exercise_11(gc);

        exercise_12(gc);
    }

    private void exercise_1(GraphicsContext gc) {

        int x1 = 100;
        int y1 = 75;

        int x2 = 100;
        int y2 = 125;

        int angle = 4;
        int length = 8;

        gc.strokeLine(x1, y1, x1+length, y1-angle);
        gc.strokeLine(x1, y1, x1+length, y1+angle);

        gc.strokeLine(x2, y2, x2+length, y2-angle);
        gc.strokeLine(x2, y2, x2+length, y2+angle);

        int x3 = 20;
        int y3 = 50;

        gc.strokeLine(x3, y3, x3+length, y3-angle);
        gc.strokeLine(x3, y3, x3+length, y3+angle);
    }

    private void exercise_2(GraphicsContext gc) {
        int x1 = 100;
        int y1 = 100;

        int length = 100;
        int spread = 20;

        for (int i = 1 ; i <= 9 ; i++) {
            gc.strokeLine(x1, y1, spread*i, y1-length);
        }
    }

    private void exercise_3_1(GraphicsContext gc) {
        // 20 40
        int x1 = 20;
        int y1 = 5;
        int gap = 40;

        while(x1 < 200) {
            gc.strokeLine(x1, y1, x1, y1+180);
            x1 += gap;
        }
    }

    private void exercise_3_2(GraphicsContext gc) {
        int x1 = 20;
        int y1 = 20;
        int length = 40;

        while (y1 < 200) {
            gc.strokeLine(x1, y1, x1+160, y1);
            y1 += 40;
        }

    }

    private void exercise_3_3(GraphicsContext gc) {
        int x1 = 10;
        int y1 = 200-20;
        int len = 190;

        while (y1 > 0) {
            gc.strokeLine(x1, y1, len, y1);
            x1 += 20;
            len -= 20;
            y1 -= 40;
        }
    }

    private void exercise_4_1(GraphicsContext gc) {

        int x = 100;
        int y = 100;
        int r = 20;

        while (r <= 100) {
            gc.strokeOval((x-r), (y-r), 2*r, 2*r);
            r += 20;
        }
    }

    private void exercise_4_2(GraphicsContext gc) {

        int x = 20;
        int y = 100;
        int r = 10;

        while (r <= 80) {
            gc.strokeOval((x - 10), (y - r), 2*r, 2*r);
            r += 10;
        }
    }

    private void exercise_4_3(GraphicsContext gc) {

        int x = 100;
        int y = 100;
        int r = 20;

        int height = 4;
        int width = 2;
        double moveLeftValue = 1;

        while (moveLeftValue <= 4) {
            gc.strokeOval((x-r*moveLeftValue), (y-r*2), width*r, height*r);
            moveLeftValue += 0.5;
            width += 1;
        }
    }

    private void exercise_5(GraphicsContext gc) {

        String s = "Datamatiker";
        int index = 1;
        int y = 10;

        while (index <= s.length()) {
            gc.fillText(s.substring(0, index), 20, y);
            y += 18;
            index++;
        }
    }

    private void exercise_6(GraphicsContext gc) {
        int y = 170;
        int startX = 10;
        int gap = 15;

        gc.strokeLine(5, y, 180, y);
        gc.strokeLine(180, y, 180-5, y+5);
        gc.strokeLine(180, y, 180-5, y-5);

        for (int i = 0 ; i <= 10 ; i++) {

            if (i % 5 == 0) {
                gc.strokeLine(startX + gap*i, y - 10, startX + gap*i, y + 10);
                gc.fillText(i + "", startX - 3 + gap*i, y + 22);
            } else {
                gc.strokeLine(startX + gap*i, y - 5, startX + gap*i, y + 5);
            }

        }
    }

    private void exercise_7(GraphicsContext gc) {
        String s = "Datamatiker";
        int y = 10;

        for (int index = 1 ; index <= s.length() ; index++) {
            gc.fillText(s.substring(0, index), 20, 18 * index);
        }
    }

    private void exercise_8(GraphicsContext gc) {

        int height = 80;
        int heightDecrease = 8;

        int x = 16;
        int xIncrease = 16;

        int y = 160;
        int yDecrease = -12;

        for (int i = 0 ; i < 10 ; i++) {
            gc.strokeLine((x + xIncrease * i), (y + yDecrease * i), (x + xIncrease * i), (y - height - (heightDecrease * i / 2)));
            System.out.println("x: " + (x + xIncrease * i) + " y: " + (y + yDecrease * i) + " height: " + (y - height - (heightDecrease * i / 2)));
        }
    }

    private void exercise_9(GraphicsContext gc) {
        double x = 180;
        double tY = x / 5;
        double bY = x / 2;

        for (int i = 1 ; i <= 12 ; i++ ) {
            gc.strokeLine(x, 100+bY, x, 100-tY);
            x = x * 0.75;
            bY = x / 2;
            tY = x / 5;
        }
    }

    private void exercise_10(GraphicsContext gc) {

        for (int i = 0 ; i < 5 ; i++) {
            this.drawCircle(gc, 50 + (25*i), 100, 40);
        }

    }

    private void exercise_11(GraphicsContext gc) {
        this.drawCross(gc, 100, 100, 10);
    }

    private void exercise_12(GraphicsContext gc) {
        int x = 20;
        int y = 450; //150
        int h = 200; //50

        this.drawTriangle(gc, x, y, h);

        //for (int i = 1 ; i < 4 ; i++) {
        //    this.drawTriangle(gc, x, y, h / ((int)Math.pow(3, i)));
        //}

        //this.drawTriangle(gc, x, y, h / 3);
        //this.drawTriangle(gc, (x + h) - (h / 3), (y - h) + (h / 3), h / 3);
        //this.drawTriangle(gc, (x + h * 2) - (h / 3) * 2, y , h / 3);
    }

    private void drawCircle(GraphicsContext gc, int x, int y, int r) {
        gc.strokeOval((x-r), (y-r), r*2, r*2);
    }

    private void drawCross(GraphicsContext gc, int x, int y, int length) {
        gc.strokeLine((x-length/2), (y), (x+length/2), y);
        gc.strokeLine((x), (y-length/2), (x), (y+length/2));
    }

    private void drawTriangle(GraphicsContext gc, int x, int y, int h) {
        gc.strokeLine(x, y, x+(h*2), y);
        gc.strokeLine(x+h, y-h, x, y);
        gc.strokeLine(x+h, y-h, x+(h*2), y);
    }


}
