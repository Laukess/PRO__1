/**
 * Created by lukas on 22-02-2016.
 */

public class Exercise_1 {
    public static void main(String[] args) {
        int i = 0;
        int j = 0;
        int n = 0;

        System.out.println("i\tj\tn");

        while (i<10) {
            i++;
            n = n + i + j;
            j++;

            System.out.println(i+"\t"+j +"\t" + n);
        }

        int counter = 0;
        for (int p = 10; p > 0 ; p--) {
            counter++;
        }
        System.out.println(counter);

    }

}
