package Opgave_5;
import Opgave_3.*;

import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.Scanner;

public class BarChart {
	private ArrayList<Integer> valueList = new ArrayList<>();
    private ArrayList<String> headlineList = new ArrayList<>();

    MyScanner ms = new MyScanner();
	
	public void readValues() {

        while (true) {
            int value;
            do {
                value = ms.getInt("value");
            } while (false);

            if (value < 0) {
                return;
            }

            String headline;
            do {
                headline = ms.getString("headline");
            } while (false);

            headlineList.add(headline);
            valueList.add(value);
        }

	}

	/**
	 * Finds and returns the max value in the list.
	 * @param list the list with values.
	 * @return the max value found.
	 */
	public int findMax(ArrayList<Integer> list) {
        int max = Integer.MIN_VALUE;
		for(int value : list) {
			if (value > max) {
                max = value;
            }
		}
		return max;
	}

	/**
	 * Prints out a BarChart of the values using the System.out.println method. 
	 * The max value will be printed with a width of 40 stars (*).
	 */
	public void printBarChart() {
        double max = findMax(this.valueList);
		int maxAsterixWidth = 40;

        for (int i = 0 ; i < valueList.size() ; i++) {
            double procentOfMax = valueList.get(i) / max;
            System.out.print(headlineList.get(i));
            for (int star = 0 ; star < maxAsterixWidth * procentOfMax ; star++) {
                System.out.print("*");
            }
            System.out.println();
        }

    }
}
