package Opgave_4;
import java.util.ArrayList;
import java.util.Scanner;

public class BarChart {
	private ArrayList<Integer> list = new ArrayList<Integer>();
	
	public ArrayList<Integer> readValues() {
		System.out.println("Indtast nogle positive tal.  "
				+ "Indtast et negativt tal for at afslutte: ");

		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		while (n >= 0) {
			list.add(n);
			n = in.nextInt();
		}		
		in.close();
		
		return list;
	}

	/**
	 * Finds and returns the max value in the list.
	 * @param list the list with values.
	 * @return the max value found.
	 */
	public int findMax(ArrayList<Integer> list) {
        int max = Integer.MIN_VALUE;
		for(int value : list) {
			if (value > max) {
                max = value;
            }
		}
		return max;
	}

	/**
	 * Prints out a BarChart of the values using the System.out.println method. 
	 * The max value will be printed with a width of 40 stars (*).
	 */
	public void printBarChart() {
		double max = findMax(this.list);
		int maxAsterixWidth = 40;

        for (int value : this.list) {
            double procentOfMax = value / max;
            for (int star = 0 ; star < maxAsterixWidth * procentOfMax ; star++) {
                System.out.print("*");
            }
            System.out.println();
        }
	}
}
