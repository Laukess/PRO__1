/**
 * Created by lukas on 16-03-2016.
 */
public class ArrayDoubleApp {

    public static void main(String[] args) {
        int[][] doubleArray = new int[][]{
                {3, 3, 1, 0, 1},
                {0, 0, 9, 7, 1},
                {5, 5, 9, 9, 1},
                {7, 1, 1, 1, 7},
                {1, 3, 5, 7, 9},
                {1, 1, 1, 1, 1}
        };

        ArrayDoubleMethods ex = new ArrayDoubleMethods();
        //print raekke 6
        System.out.println(ex.sumRow(doubleArray, 5));
        // print col 6, den findes ikke, saa 0 bliver returned.
        System.out.println(ex.sumCol(doubleArray, 5));
        // print arrayet
        ex.print(doubleArray);
        // print summen af hele arrayet.
        System.out.println(ex.sum(doubleArray));

        // finder summen ved hjaelp af sumRow funktionen, for at sikre
        // sig at sum funktionen returns the same value.
        int total = 0;
        for (int i = 0 ; i < doubleArray.length ; i++) {
            total += ex.sumRow(doubleArray, i);
        }
        System.out.println(total);



    }

}
