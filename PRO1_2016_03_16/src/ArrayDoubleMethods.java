import java.lang.reflect.Array;

/**
 * Created by lukas on 16-03-2016.
 */
public class ArrayDoubleMethods {

    public int sumRow(int[][] table, int row) {
        int sum = 0;

        if (row < table.length) {
            for (int col = 0 ; col < table[row].length ; col++) {
                sum += table[row][col];
            }
        }
        return sum;

    }

    public int sumCol(int[][] table, int col) {
        int total = 0;

        if (col < table.length) {
            for (int[] row : table) {

                if(col < row.length) {
                    total += row[col];
                }

            }
        }
        return total;
    }

    public void print(int[][] table) {

        for(int row = 0 ; row < table.length ; row++) {
            for (int col = 0 ; col < table[row].length ; col++) {
                System.out.print(table[row][col] + " ");
            }
            System.out.println();
        }

    }

    public int sum(int[][] table) {
        int total = 0;
        for (int row = 0 ; row < table.length ; row++) {
            for (int col = 0 ; col < table[row].length ; col++) {
                total += table[row][col];
            }
        }
        return total;

    }

}
