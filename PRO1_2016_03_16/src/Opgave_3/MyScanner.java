package Opgave_3;

import java.util.Scanner;

/**
 * Created by lukas on 25-02-2016.
 */
public class MyScanner {

    Scanner s = new Scanner(System.in);

    public int getInt(String input) {
        int output;
        System.out.print(input+" >>> ");
        while (!s.hasNextInt()) {
            System.out.print(input+" >>> ");
        }
        return output = s.nextInt();
    }

    public Double getDouble(String input) {
        double output;
        System.out.print(input+" >>> ");
        while (!s.hasNextDouble()) {
            System.out.print(input+" >>> ");
        }
        return output = s.nextDouble();
    }

    public String getString(String input) {
        String output;
        System.out.print(input+" >>> ");
        while (!s.hasNext()) {
            System.out.print(input+" >>> ");
        }
        return output = s.next();
    }

}
