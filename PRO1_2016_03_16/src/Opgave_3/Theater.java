package Opgave_3;

import java.lang.reflect.Array;

/**
 * Created by lukas on 16-03-2016.
 */
public class Theater {
    private int[][] seats;
    MyScanner ms = new MyScanner();

    public Theater(int[][] seats) {
        this.seats = seats;
    }

    public Theater() {
        this(new int[][]{
                {0, 10, 10, 10, 10, 10, 10, 10, 10, 10},
                {10, 10, 10, 10, 10, 10, 10, 10, 10, 10},
                {10, 10, 10, 10, 10, 10, 10, 10, 10, 10},
                {10, 10, 20, 20, 20, 20, 20, 20, 10, 10},
                {10, 10, 20, 20, 20, 20, 20, 20, 10, 10},
                {10, 10, 20, 20, 20, 20, 20, 20, 10, 10},
                {20, 20, 30, 30, 40, 40, 30, 30, 20, 20},
                {20, 30, 30, 40, 50, 50, 40, 30, 30, 20},
                {30, 40, 50, 50, 50, 50, 50, 50, 40, 30}
        });
    }

    public int[] findSeatAtPrice(int price) {
        for (int row = 0 ; row < this.seats.length ; row++) {
            for (int col = 0 ; col < this.seats[row].length ; col++) {
                if (this.seats[row][col] == price) {
                    return new int[]{row, col};
                }
            }
        }
        return new int[]{-1, -1};
    }

    public int getSeatPrice(int[] location) {
        return seats[location[0]][location[1]];
    }

    public boolean reserveSeat(int[] location) {
        if (seats[location[0]][location[1]] == 0) {
            return false;
        } else {
            seats[location[0]][location[1]] = 0;
            return true;
        }
    }

    public int getRows() {
        return seats.length;
    }

    public int getColumns() {
        return seats[0].length;
    }

    public int[] promptUserForSeatDetails() {
        int row;
        do {
            row = ms.getInt("Hvilken raekke vil du side i ?");
        } while (row < 0 || row > getRows());

        int seatNum;
        do {
            seatNum = ms.getInt("Hvilket seat vil du side i ?");
        } while (seatNum < 0 || seatNum > getColumns());

        return new int[]{row, seatNum};
    }

}
