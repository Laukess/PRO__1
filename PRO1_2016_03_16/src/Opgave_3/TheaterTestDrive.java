package Opgave_3;

/**
 * Created by lukas on 16-03-2016.
 */
public class TheaterTestDrive {

    public static void main(String[] args) {
        Theater theater = new Theater();
        MyScanner ms = new MyScanner();

        String input;
        do {
            input = ms.getString("Oensker du en plads til en bestem pris, eller en bestem plads ? [pris/plads]");
        } while (!input.equalsIgnoreCase("pris") && !input.equalsIgnoreCase("plads"));


        if (input.equalsIgnoreCase("pris")) {

            int price;
            do {
                price = ms.getInt("Hvor meget skal det koste?");
            } while (price < 0);

            int[] location = theater.findSeatAtPrice(price);

            if (location[0] == -1) {
                System.out.println("Der er ikke fundet nogle seats til den pris");
            } else {
                if (theater.reserveSeat(location)) {
                    System.out.println("Du har faat en plads paa raekke " + location[0] + " saede " + location[1]);
                } else {
                    System.out.println("Der er sket en fejl, du har proevet at reservere et seat der allerede er reserveret");
                }
            }

        } else if (input.equalsIgnoreCase("plads")) {

            boolean isDone = false;

            while (!isDone) {
                int[] location = theater.promptUserForSeatDetails();
                if (theater.getSeatPrice(location) > 0){
                    System.out.println("Din plads er ledig og koster " + theater.getSeatPrice(location) + "kr.");
                    if(!theater.reserveSeat(location)) {
                        System.out.println("Der er sket en fejl, du har proevet at reservere et seat der allerede er reserveret");
                    } else {
                        System.out.println("Tillykke, du har nu rXeserveret pladsen paa raekke " + location[0] + " seat " + location[1]);
                        isDone = true;
                    }
                } else {
                    System.out.println("Denne plads er allede reserveret");
                }
            }

        }

    }

}
