package Opgave_5;

/**
 * Created by Laukess on 04-05-2016.
 */
public abstract class Vare {

    private double price;
    private String name;
    private String description;


    public double getMoms(double price) {
        return price * 0.25;
    }


    public double salgsPris() {
        return this.price + getMoms(this.price);
    }

}
