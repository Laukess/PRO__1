package Opgave_4;

/**
 * Created by lukas on 25-04-2016.
 */
public class Ellipse extends Shape{
    // in radius
    private int height;
    private int width;

    public Ellipse(int x, int y, int height, int width) {
        super(x, y);
        this.height = height;
        this.width = width;
    }

    public double getArea() {
        return Math.PI * this.height * this.width;
    }
}

