package Opgave_4;

/**
 * Created by lukas on 25-04-2016.
 */
public class Square extends Shape{

    private int size;

    public Square(int x, int y, int size) {
        super(x, y);
        this.size = size;
    }

    public double getArea() {
        return Math.pow(size, 2);
    }


}
