package Opgave_4;

/**
 * Created by lukas on 25-04-2016.
 */
public class Circle extends Shape {

    private double radius;

    public Circle(int x, int y, double radius) {
        super(x, y);
        setSize(radius);
    }

    public void setSize(double radius) {
        this.radius = radius;
    }

    public double getArea() {
        return Math.PI * Math.pow(radius, 2);
    }
}
