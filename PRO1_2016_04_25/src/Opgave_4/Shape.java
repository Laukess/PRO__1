package Opgave_4;

/**
 * Created by lukas on 25-04-2016.
 */
public abstract class Shape {
    private int x;
    private int y;
    private double area;

    public Shape(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public abstract double getArea();

    public void setArea(double area) {
        this.area = area;
    }

    public void translate(int x, int y) {
        this.x += x;
        this.y += y;
    }


//    public abstract void setSize();

}
