package Opgave_3;

/**
 * Created by lukas on 21-04-2016.
 */
public class Værkfører extends Ansat {

    private int promAar;
    private int tillaeg;

    public Værkfører(String navn, String adresse, int aar, double timeløn, int promAar, int tillaeg) {
        super(navn, adresse);
        this.promAar = promAar;
        this.tillaeg = tillaeg;
    }

    public int getPromAar() {
        return promAar;
    }

    public void setPromAar(int promAar) {
        this.promAar = promAar;
    }

    public int getTillaeg() {
        return tillaeg;
    }

    @Override
    public double getUgeLoen() {
        return (getTimeloen() + tillaeg) * getArbejdstimer();
    }

    public void setTillaeg(int tillaeg) {
        this.tillaeg = tillaeg;
    }
}
