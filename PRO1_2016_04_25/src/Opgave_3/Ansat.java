package Opgave_3;

import java.util.ArrayList;

/**
 * Created by lukas on 25-04-2016.
 */
public abstract class Ansat extends Person {

    private double timeloen;
    private int arjedstimer;

    public Ansat(String navn, String adresse, double timeloen, int arjedstimer) {
        super(navn, adresse);
        this.timeloen = timeloen;
        this.arjedstimer = arjedstimer;
    }

    public Ansat(String navn, String adresse) {
        this(navn, adresse, 0, 0);
    }

    public abstract double getUgeLoen();

    public static double beregnSamletLøn(ArrayList<Ansat> list) {
        double samletLoen = 0;
        for (Ansat a : list) {
            samletLoen += a.getUgeLoen();
        }
        return samletLoen;
    }

    public double getTimeloen() {
        return timeloen;
    }

    public void setTimeloen(double timeloen) {
        this.timeloen = timeloen;
    }

    public int getArbejdstimer() {
        return arjedstimer;
    }

    public void setArjedstimer(int arjedstimer) {
        this.arjedstimer = arjedstimer;
    }
}
