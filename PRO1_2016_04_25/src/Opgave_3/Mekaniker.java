package Opgave_3;

/**
 * Created by lukas on 21-04-2016.
 */
public class Mekaniker extends Ansat {
    private int aar;

    public Mekaniker(String navn, String adresse, int aar, double timeløn, int arbejdstimer) {
        super(navn, adresse);
        this.aar = aar;
        setTimeloen(timeløn);
        setArjedstimer(arbejdstimer);
    }

    public int getAar() {
        return aar;
    }

    public void setAar(int aar) {
        this.aar = aar;
    }

    public double getUgeLoen() {
        return getTimeloen() * getArbejdstimer();
    }

}
