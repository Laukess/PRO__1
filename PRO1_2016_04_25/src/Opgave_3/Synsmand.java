package Opgave_3;

/**
 * Created by lukas on 21-04-2016.
 */
public class Synsmand extends Mekaniker {

    private int syn;

    public Synsmand(String navn, String adresse, int aar, double timeløn, int arbejdstimer) {
        super(navn, adresse, aar, timeløn, arbejdstimer);
        this.syn = 0;
    }

    public double getUgeLoen() {
        return super.getUgeLoen() + (syn * 250);
    }

    public int getSyn() {
        return syn;
    }

    public void setSyn(int syn) {
        this.syn = syn;
    }
}
