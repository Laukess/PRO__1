package Opgave_3;

/**
 * Created by lukas on 25-04-2016.
 */
public class Arbejdsdreng extends Ansat {

    public Arbejdsdreng(String navn, String adresse, double timeloen, int arbejdstimer) {
        super(navn, adresse, timeloen, arbejdstimer);
    }

    @Override
    public double getUgeLoen() {
        return getTimeloen() * getArbejdstimer();
    }
}
