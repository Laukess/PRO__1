package Opgave_3;

import java.util.ArrayList;

/**
 * Created by lukas on 21-04-2016.
 */
public class App {

    public static void main(String[] args) {
        Person p = new Person("John", "Hervej 22");
        Mekaniker m = new Mekaniker("Mika", "Vej 12", 1999, 220, 37);
        Værkfører v = new Værkfører("Spike", "Vej 13", 1989, 220, 2007, 30);
        Synsmand s = new Synsmand("Peter", "jel", 1998, 220, 37);

        s.setSyn(4);

        ArrayList<Ansat> ansatte = new ArrayList<>();
        ansatte.add(m);
        ansatte.add(v);
        ansatte.add(s);

        for (Ansat ansat : ansatte) {
            System.out.println(ansat.getUgeLoen());
        }

        System.out.println("Den samlede Loen er: " + Mekaniker.beregnSamletLøn(ansatte));

    }

}
