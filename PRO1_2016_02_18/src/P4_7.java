import java.util.Scanner;

/**
 * Created by lukas on 18-02-2016.
 */
public class P4_7 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("tal 1 >> ");
        int one = in.nextInt();

        System.out.print("tal 2 >> ");
        int two = in.nextInt();

        System.out.print("tal 3 >> ");
        int three = in.nextInt();

        if (one <= two && one <= three && two <= three) {
            System.out.println("low -> high");
        } else if ( one >= two && one >= three && two >= three ) {
            System.out.println("high -> low");
        } else {
            System.out.println("NOPE!!!!!!");
        }

    }

}
