/**
 * Created by lukas on 18-02-2016.
 */
public class Opgave_1_TestDrive {
    public static void main(String[] args) {
        Opgave_1 child = new Opgave_1(0, true);
        System.out.println("Gender: " + child.gender() + ", age " +child.getAge() + ": " + child.institution() + ", Team: " + child.team());
        for (int i = 1 ; i < 20 ; i++) {
            child.setAge(i);
            System.out.println("Gender: " + child.gender() + ", age " +child.getAge() + ": " + child.institution() + ", Team: " + child.team());
        }

        System.out.println("Gender: " + child.gender());
        child.setBoy(false);
        System.out.println("Gender: " + child.gender());

        child.setBoy(false);
        child.setAge(0);
        System.out.println("Gender: " + child.gender() + ", age " +child.getAge() + ": " + child.institution() + ", Team: " + child.team());
        for (int i = 1 ; i < 20 ; i++) {
            child.setAge(i);
            System.out.println("Gender: " + child.gender() + ", age " +child.getAge() + ": " + child.institution() + ", Team: " + child.team());
        }
    }
}
