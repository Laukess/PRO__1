import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by lukas on 18-02-2016.
 */
public class P4_8 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        ArrayList ls = new ArrayList();

        System.out.print("tal 1 >> ");
        int one = in.nextInt();

        System.out.print("tal 2 >> ");
        int two = in.nextInt();

        System.out.print("tal 3 >> ");
        int three = in.nextInt();

        System.out.print("tal 4 >> ");
        int four = in.nextInt();

        ls.add(one);
        ls.add(two);
        ls.add(three);
        ls.add(four);

        int firstValue = (int) ls.remove(0);
        if (ls.indexOf(firstValue) > -1) {
            System.out.println("one pair");
            ls.remove(ls.indexOf(firstValue));
            if (ls.get(0) == ls.get(1)) {
                System.out.println("two pairs");
            }
        }

        if (one == two && three == four) {
            System.out.println("yay");
        } else if (one == three && two == four) {
            System.out.println("yay");
        } else if (one == four && two == three) {
            System.out.println("yay");
        }

    }

}

