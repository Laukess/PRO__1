/**
 * Created by lukas on 18-02-2016.
 */
public class Opgave_4_TestDrive {

    public static void main(String[] args) {
        Opgave_4 printer = new Opgave_4();

        System.out.println("*** printer uden papir ***");
        printer.makeCopy();

        System.out.println("*** Indsaetter 5 ark, og printer 6 kopier ***");
        printer.insertPaper(5);

        for (int i = 1 ; i <= 6 ; i++) {
            printer.makeCopy();
        }

        System.out.println("*** Indsaetter 2 ark, og proever at printe 3 kopier ***");
        printer.insertPaper(2);
        printer.makeCopy(3);
        System.out.println("*** Indsaetter 2 ark mere, og proever igen at printe 3 kopier ***");
        printer.insertPaper(2);
        printer.makeCopy(3);

        System.out.println("*** Jam printeren og print en kopi ***");
        printer.makePaperJam();
        printer.makeCopy();
        System.out.println("*** Fix jam og print en kopi ***");
        printer.fixPaperJam();
        printer.makeCopy();

        printer.insertPaper(10);
        System.out.println("*** Der er " + printer.getPaper() + " ark i printeren ***");
        printer.makeCopy(8);
        System.out.println("*** Efter 8 print er der " + printer.getPaper() + " ark tilbage ***" );


    }
}
