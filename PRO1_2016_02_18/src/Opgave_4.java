/**
 * Created by lukas on 18-02-2016.
 */
public class Opgave_4 {
    private int paper;
    private boolean paperStuck = false;

    public Opgave_4() {

    }

    public void insertPaper(int paper) {

        if(this.paper + paper > 500) {
            System.out.println("Fejl. Ikke plads til mere papir!");
        } else {
            this.paper += paper;
        }

    }

    public int getPaper() {
        return this.paper;
    }

    public void makeCopy() {

        if (paper > 0) {
            if (!this.paperStuck) {
                this.paper--;
                System.out.println("printer...");
            } else {
                System.out.println("JAM!!!!!!");
            }
        } else {
            System.out.println("Mangler papir....");
        }

    }


    public void makeCopy(int copies) {

        if (this.paper >= copies) {
            if (!this.paperStuck){
                this.paper -= copies;
                System.out.println("printer...");
            } else {
                System.out.println("JAM!!!!!!");
            }
        } else {
            System.out.println("Mangler papir....");
        }

    }

    public void makePaperJam() {
        this.paperStuck = true;
    }

    public void fixPaperJam() {
        this.paperStuck = false;
    }

}
