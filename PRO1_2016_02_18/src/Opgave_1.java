/**
 * Created by lukas on 18-02-2016.
 */
public class Opgave_1 {
    /**
     * Write a description of class Child here.
     */
    private int age;
    private boolean boy;

    public Opgave_1(int age, boolean boy) {
        this.age = age;
        this.boy = boy;
    }

    // opgave 2
    public String gender() {
        return this.boy ? "Boy" : "Girl";
    }

    // opgave 3
    public String team() {
        // hvis man er 17+ ???
        String team = "Error";
        if (gender().equals("Girl")) {
            if (getAge() < 8) {
                team = "Tumbling girls";
            } else if (getAge() >= 8 && !institution().equals("Out of school")) {
                team = "Springy girls";
            } else {
                team = "N/A";
            }
        } else if (gender().equals("Boy")) {
            if ( getAge() < 8 ) {
                team = "Young cubs";
            } else if (getAge() >= 8 && !institution().equals("Out of school")) {
                team = "Cool boys";
            } else {
                team = "N/A";
            }
        }
        return team;
    }

    public int getAge() {
        return this.age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isBoy() {
        return this.boy;
    }

    public void setBoy(boolean boy) {
        this.boy = boy;
    }

    public String institution() {
        String institution = "Error";
        if (this.age == 0) {
            institution = "Home";
        } else if (this.age > 0 && this.age < 3) {
            institution = "Nursery";
        } else if (this.age >= 3 && this.age <= 5) {
            institution = "Kindergarten";
        } else if (this.age >= 6 && this.age <= 16) {
            institution = "School";
        } else if (this.age > 16) {
            institution = "Out of school";
        }
        return institution;
    }

}