import java.util.Scanner;

/**
 * Created by lukas on 18-02-2016.
 */
public class My_Scanner {

    public static void main(String[] args) {

        Scanner s = new Scanner(System.in);
        int answer;

        System.out.print("nummer->");
        while (!s.hasNextInt()) {
            System.out.print("nummer->");
            s.next();
        }
        answer = s.nextInt();
        System.out.println(answer);
    }
}
