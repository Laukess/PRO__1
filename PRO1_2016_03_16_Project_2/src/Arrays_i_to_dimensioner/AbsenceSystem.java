package Arrays_i_to_dimensioner;

public class AbsenceSystem {
    /**
     * Print the absence table on the screen
     */

    String[] months = new String[]{"Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dec"};

    public void printAbsence(int[][] absence) {

        System.out.print("             ");
        for (int col = 0 ; col < absence[0].length ; col++) {
            System.out.printf("%-6.6s", months[col]);
        }
        System.out.println();

        for (int row = 0 ; row < absence.length ; row++) {
            System.out.printf("Student %d:    ", row+1);
            for (int col = 0 ; col < absence[row].length ; col++) {
                System.out.printf("%-6.6s", absence[row][col]);
            }
            System.out.println();
        }
    }

    /**
     * Returns the total number of absent days for the given student during the last 12 months.
     */
    public int totalAbsence(int[][] absence, int studentNumber) {
        int totalAbsence = 0;
        // gaar ud fra studentNumber er 1 hoejere end den studerendes index i absence array'et
        try {
            for (int col = 0 ; col < absence[studentNumber-1].length ; col++) {
                totalAbsence += absence[studentNumber-1][col];
            }
        } catch (Exception e) {
            // hvis eleven ikke er fundet, return'er metoden -1.
            totalAbsence = -1;
        }
        return totalAbsence;
    }

    /**
     * Returns the average monthly number of absent days for the given student.
     */
    public double averageMonth(int[][] absence, int studentNumber) {
        int totalAbsence = totalAbsence(absence, studentNumber);

        if (totalAbsence != -1) {
            if (totalAbsence == 0) {
                return 0;
            } else {
                return totalAbsence / (double) months.length;
            }
        } else {
            // this der ikke er angivet et rigtigt studentNumber, return'er metoden -1
            return -1;
        }
    }

    /**
     * Returns the number of students without any absence during the last 12 months.
     * 
     */
    public int studentWithoutAbsenceCount(int[][] absence) {
        int studentsWithNoAbsence = 0;

        for (int studentIndex = 0 ; studentIndex < absence.length ; studentIndex++) {
            if(totalAbsence(absence, studentIndex+1) == 0) {
                studentsWithNoAbsence++;
            }
        }
        return studentsWithNoAbsence;
    }

    /**
     * Returns the student with the most absence during the last 12 months.
     * If more than one student has the most absence, return any one of them.
     */
    public int mostAbsentStudent(int[][] absence) {

        int topAbsenceIndex = 0;

        for (int studentIndex = 1 ; studentIndex < absence.length ; studentIndex++) {
            int currentStudentAbsence = totalAbsence(absence, studentIndex+1);
            int mostAbsence = totalAbsence(absence, topAbsenceIndex+1);
            if (currentStudentAbsence > mostAbsence) {
                topAbsenceIndex = studentIndex;
            }
        }
        if (absence.length > 0) {
            return topAbsenceIndex + 1;
        } else {
            return -1;
        }

    }

    /**
     * Resets the absence to 0 for the given student during the last 12 months.
     */
    public void reset(int[][] absence, int studentNumber) {

        for (int month = 0 ; month < absence[studentNumber-1].length ; month++) {
            absence[studentNumber-1][month] = 0;
        }

    }
}
