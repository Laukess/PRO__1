package Arrays_i_to_dimensioner;

public class TestAbsenceSystem {

    public static void main(String[] args) {
        int[][] absence15t = {
                { 2, 0, 0, 0, 3, 1, 0, 2,  0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0,  0, 0, 0, 0 },
                { 2, 0, 0, 0, 3, 1, 0, 2,  0, 0, 0, 0 },
                { 1, 2, 1, 2, 1, 2, 0, 2,  0, 0, 4, 0 },
                { 5, 0, 0, 0, 0, 0, 0, 10, 0, 0, 0, 0 } };

        AbsenceSystem absenceSystem = new AbsenceSystem();
        absenceSystem.printAbsence(absence15t);
        System.out.println("totalAbsence metode med outOfBounds studentNumber -> " +
                absenceSystem.totalAbsence(absence15t, 6));
        System.out.println("totalAbsence metode med studentNumber 5 -> " +
                absenceSystem.totalAbsence(absence15t, 5));
        System.out.println("Gennemsnitlige frevaerdsdage hver maaned over et aar for outOfBounds studentNumber -> " +
                absenceSystem.averageMonth(absence15t, 6));
        System.out.println("Gennemsnitlige frevaerdsdage hver maaned over et aar for studentNumber 5 -> " +
                absenceSystem.averageMonth(absence15t, 5));
        System.out.println("Antal studerende uden fravaerd -> " +
                absenceSystem.studentWithoutAbsenceCount(absence15t));
        System.out.println("StudentNumber paa den med mest fravaerd -> " +
                absenceSystem.mostAbsentStudent(absence15t));

        System.out.println("\n\ntable foer studentNumber 1 for resat hans/hendes fravaerd: \n");
        absenceSystem.printAbsence(absence15t);
        absenceSystem.reset(absence15t, 1);
        System.out.println("\n\ntable efter studentNumber 1 fik resat hans/hendes fravaerd: \n");
        absenceSystem.printAbsence(absence15t);
    }
}
