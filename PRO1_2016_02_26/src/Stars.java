	public class Stars {
	final int MAX_ROWS = 10;

	public void starPicture() {
		for (int row = 1; row <= MAX_ROWS; row++) {
			for (int star = 1; star <= row; star++) {
				System.out.print("*");
			}
			System.out.println();
		}
	}

	public void starPictureA() {
		for (int i = 1 ; i <= MAX_ROWS ; i++) {

			for (int j = 1 ; j <= i ; j++) {
				System.out.print("*");
			}

			System.out.print("\n");
		}
	}

	public void starPictureB() {
		for (int i = 1 ; i <= MAX_ROWS ; i++) {

			for (int j = MAX_ROWS ; j >= i ; j--) {
				System.out.print(" ");
			}

			for (int k = 1 ; k <= i ; k++) {
				System.out.print("*");
			}

			System.out.print("\n");
		}
	}

	public void starPictureC() {
		for (int i = 1 ; i <= MAX_ROWS ; i++) {

            for (int k = i-1 ; k > 0 ; k--) {
                System.out.print(" ");
            }

            for (int j = 10 ; j >= i ; j--) {
                System.out.print("*");
            }
            System.out.println();
        }
	}

	public void starPictureD() {

        // line
		for (int i = 1 ; i <= 5 ; i++) {

            // number of spaces
            int spaces = 5-i;
            for (int j = spaces ; j > 0 ; j--) {
                System.out.print(" ");
            }

            // number of *
            for (int k = 9 - (spaces * 2) ; k > 0 ; k--) {
                System.out.print("*");
            }
            System.out.println();
        }

        for (int i = 5 ; i >= 1 ; i--) {

            // number of spaces
            int spaces = 5-i;
            for (int j = spaces ; j > 0 ; j--) {
                System.out.print(" ");
            }

            // number of *
            for (int k = 9 - (spaces * 2) ; k > 0 ; k--) {
                System.out.print("*");
            }

            System.out.println();
        }

	}
}
