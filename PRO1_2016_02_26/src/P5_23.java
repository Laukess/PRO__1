/**
 * Created by lukas on 28-02-2016.
 */
public class P5_23 {

    public static void main(String[] args) {

        MyScanner ms = new MyScanner();
        int input;
        do {
            input = ms.getInt("indtast et tal.");
        } while (input < 0);

        for (int i = 1 ; i <= input ; i++) {

            if (P5_23.isPrime(i)) {
                System.out.println(i);
            }
        }
    }

    public static boolean isPrime(int number) {

        // loop over alle tal fra 2 til halvdelen af nummeret.
        for (int i = 2 ; i <= Math.ceil(number / 2.0) ; i++) {
            //System.out.println("kan " + number + " divideres med " + i + "?");
            if (number % i == 0) {
                //System.out.println("ja");
                return false;
            }
        }
        return true;
    }

}
